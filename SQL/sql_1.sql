-- DDL - Data Definition Language
-- Creazione tabella
--CREATE TABLE Rubrica (
--	nome VARCHAR(255),
--	cognome VARCHAR(255),
--	cod_fis VARCHAR(16),
--	telefono VARCHAR(255)
--);

-- Eliminazione tabella
-- DROP TABLE Rubrica;

-- Aggiunta di una colonna
-- ALTER TABLE Rubrica ADD Email VARCHAR(255);

-- Alterazione di una colonna
-- ALTER TABLE Rubrica ALTER COLUMN email VARCHAR(500);

-- Eliminazione di una colonna
-- ALTER TABLE Rubrica DROP COLUMN Email;

-- DML - Data Manipulation Language
--INSERT INTO Rubrica (nome, cognome, cod_fis, telefono) VALUES ('Giovanni', 'Pace', 'PCAGNN', '+39123456');
--INSERT INTO Rubrica (nome, cognome, cod_fis, telefono) VALUES
--('Mario', 'Rossi', 'MRRRSS', '456123'), 
--('Valeria', 'Verdi', 'VLRVRD', '456124');

--DELETE FROM Rubrica WHERE nome = 'Mario';
--DELETE FROM Rubrica;				-- ATTENZIONE!!!

UPDATE Rubrica SET nome = 'Mirko', cognome = 'Rossiccio' WHERE cod_fis = 'MRRRSS';

-- QL - Query Language
SELECT * FROM Rubrica;
--SELECT nome AS NomePersona, telefono AS 'Numero Telefonico' FROM Rubrica;		-- Proietto e rinomino nomi di colonna

--SELECT * FROM Rubrica WHERE nome = 'Giovanni' AND cognome = 'Pacelli';

 --SELECT * FROM Rubrica WHERE nome = 'Giovanni' OR nome = 'Mario';


-- CRUD
-- Creation
-- Reading
-- Update
-- Delete