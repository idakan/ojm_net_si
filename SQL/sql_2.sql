DROP TABLE IF EXISTS RubricaDue;

CREATE TABLE RubricaDue (
	nome VARCHAR(255) NOT NULL,
	cognome VARCHAR(255) NOT NULL,
	cod_fis VARCHAR(16) UNIQUE NOT NULL,
	telefono VARCHAR(255) DEFAULT 'N.D.'
);

--INSERT INTO RubricaDue (nome, cognome, cod_fis, telefono) VALUES
--('Giovanni', 'Pace', 'PCAGNN', '+39123456'),
--('Mario', 'Rossi', 'MRRRSS', '456123'), 
--('Mario', 'Rossi', 'MRRRSS', '456123'), 
--('Valeria', 'Verdi', 'VLRVRD', '456124');

INSERT INTO RubricaDue (nome, cognome, cod_fis, telefono) VALUES ('Giovanni', 'Pace', 'PCAGNN', '+39123456');
INSERT INTO RubricaDue (nome, cognome, cod_fis) VALUES ('Mario', 'Rossi', 'MRRRSS'); 
INSERT INTO RubricaDue (nome, cognome, cod_fis, telefono) VALUES ('Valeria', 'Verdi', 'VLRVRD', '456124');
INSERT INTO RubricaDue (cod_fis, cognome, nome) VALUES ('MRKMRT', 'Mirto', 'Mirko');

DELETE FROM RubricaDue WHERE cod_fis = 'MRRRSS';

SELECT * FROM RubricaDue;