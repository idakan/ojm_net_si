CREATE TABLE Persona(
	personaID INTEGER IDENTITY(1,1),
	nome VARCHAR(250),
	cognome VARCHAR(250),
	PRIMARY KEY(personaID)
);

CREATE TABLE CodiceFiscale(
	codiceID INTEGER PRIMARY KEY IDENTITY(1,1),
	numero VARCHAR(16) UNIQUE,
	data_scad DATE,
	personaRIF INTEGER UNIQUE NOT NULL,
	FOREIGN KEY (personaRIF) REFERENCES Persona(personaID) ON DELETE CASCADE
);

INSERT INTO Persona (nome, cognome) VALUES
('Giovanni','Pace'),
('Mario','Rossi'),
('Valeria','Verdi');

SELECT * FROM Persona;

INSERT INTO CodiceFiscale (numero, data_scad, personaRIF) VALUES ('12456','2020-02-02', 1);
INSERT INTO CodiceFiscale (numero, data_scad, personaRIF) VALUES ('123457','2020-02-02', 2);
INSERT INTO CodiceFiscale (numero, data_scad, personaRIF) VALUES ('123458','2020-02-02', 3);

SELECT nome, cognome, numero, data_scad AS 'Data Scadenza' FROM Persona
	LEFT JOIN CodiceFiscale ON Persona.personaID = CodiceFiscale.personaRIF;