DROP TABLE IF EXISTS RubricaTre;

CREATE TABLE RubricaTre (
	id INT PRIMARY KEY IDENTITY(1, 1),
	nome VARCHAR(255) NOT NULL,
	cognome VARCHAR(255) NOT NULL,
	cod_fis VARCHAR(16) UNIQUE NOT NULL,
	telefono VARCHAR(255) DEFAULT 'N.D.',
	mail VARCHAR(255)
);

INSERT INTO RubricaTre (nome, cognome, cod_fis, telefono) VALUES ('Giovanni', 'Pace', 'PCAGNN', '+39123456');
INSERT INTO RubricaTre (nome, cognome, cod_fis) VALUES ('Mario', 'Rossi', 'MRRRSS'); 
INSERT INTO RubricaTre (nome, cognome, cod_fis, telefono) VALUES ('Valeria', 'Verdi', 'VLRVRD', '456124');
INSERT INTO RubricaTre (cod_fis, cognome, nome, mail) VALUES ('MRKMRT', 'Mirto', 'Mirko', 'mirto@mirko.com');

--DELETE FROM RubricaTre WHERE id = 3;

INSERT INTO RubricaTre (cod_fis, cognome, nome) VALUES ('MRAMRT', 'Mirta', 'Maria');

-- QL
--SELECT * FROM RubricaTre WHERE nome IN ('Giovanni', 'Maria', 'Mario');
--SELECT * FROM RubricaTre WHERE telefono <> 'N.D.';
--SELECT * FROM RubricaTre WHERE mail IS NOT NULL;

-- LIKE
--SELECT * FROM RubricaTre WHERE nome LIKE 'Mar%';
--SELECT * FROM RubricaTre WHERE nome LIKE '%ia';
--SELECT *  FROM RubricaTre WHERE nome LIKE '%io%';




-- CREARE una tabella di note (appunti) che siano caratterizzati da:
-- Titolo
-- Descrizione

-- Creare quattro inserimento MOCK
-- Creare una query che mi restituisca tutte le note che contengano una stringa predefinita

CREATE TABLE Note(
	id INTEGER IDENTITY(1,1),
	autore VARCHAR(250),
	titolo VARCHAR(250) NOT NULL,
	descrizione TEXT
);

INSERT INTO Note (autore, titolo, descrizione) VALUES
('Giovanni', 'Primo titolo di esempio', 'prova prova prova'),
('Mario', 'Secondo tema di esempio', 'prova prova prova'),
('Giovanni', 'Terzo tema di esempio', 'prova prova prova'),
('Mario', 'Quarto tema di esempio', 'prova prova prova');

SELECT * 
	FROM Note 
	WHERE titolo LIKE '%tema%' AND autore = 'Giovanni';