CREATE TABLE Studente(
	studenteID INTEGER PRIMARY KEY IDENTITY(1,1),
	nominativo VARCHAR(500) NOT NULL,
	matricola VARCHAR(25) UNIQUE NOT NULL
);

CREATE TABLE Esame(
	esameID INTEGER PRIMARY KEY IDENTITY(1,1),
	nome_esame VARCHAR(250) NOT NULL,
	crediti INTEGER NOT NULL,
	data_esame DATETIME NOT NULL
);

CREATE TABLE Studente_Esame(
	studenteRIF INTEGER NOT NULL,
	esameRIF INTEGER NOT NULL,
	data_iscrizione DATETIME DEFAULT CURRENT_TIMESTAMP,
	FOREIGN KEY (studenteRIF) REFERENCES Studente(studenteID) ON DELETE CASCADE,
	FOREIGN KEY (esameRIF) REFERENCES Esame(esameID) ON DELETE CASCADE,
	UNIQUE(studenteRIF, esameRIF)									-- PRIMARY KEY(studenteRIF, esameRIF)
);

INSERT INTO Studente (nominativo, matricola) VALUES
('Giovanni Pace', '123456'),
('Mario Rossi', '123457'),
('Valeria Verdi', '123458'),
('Maria Nardi', '123459');

INSERT INTO Esame (nome_esame, crediti, data_esame) VALUES
('Analsi', 6, '2022-03-19T09:00:00'),
('Fisica', 6, '2022-03-20T09:00:00'),
('Informatica', 9, '2022-03-21T09:00:00'),
('Sistemi', 6, '2022-03-19T09:00:00'),
('Elettronica', 6, '2022-03-19T09:00:00');

INSERT INTO Studente_Esame (studenteRIF, esameRIF) VALUES
(1,	2),
(1,	4),
(2,	2),
(2,	3),
(3,	2);

SELECT * FROM Studente;
SELECT * FROM Esame;
SELECT * FROM Studente_Esame;

----SELECT nominativo, matricola, nome_esame, crediti, data_esame FROM Studente
----	JOIN Studente_Esame ON Studente.studenteID = Studente_Esame.studenteRIF
----	JOIN Esame ON Studente_Esame.esameRIF = Esame.esameID;

----SELECT * FROM Esame
----	JOIN Studente_Esame ON Esame.esameID = Studente_Esame.esameRIF
----	JOIN Studente ON Studente_Esame.studenteRIF = Studente.studenteID;

SELECT * FROM Studente
	FULL JOIN Studente_Esame ON Studente.studenteID = Studente_Esame.studenteRIF
	FULL JOIN Esame ON Studente_Esame.esameRIF = Esame.esameID
	ORDER BY nome_esame, nominativo DESC;

SELECT * FROM Studente;