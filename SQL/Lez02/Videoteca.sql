-- RELAZIONE FILM/REGISTA 1:N

CREATE TABLE Regista(
	registaID INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(255) NOT NULL,
	cognome VARCHAR(255) NOT NULL,
	luogo_nascita VARCHAR(255),
	data_nascita DATE,
);
CREATE TABLE Attore(
	attoreID INT PRIMARY KEY IDENTITY(1,1),
	nome VARCHAR(255) NOT NULL,
	cognome VARCHAR(255) NOT NULL,
	luogo_nascita VARCHAR(255),
	data_nascita DATE,
);
CREATE TABLE Film(
	filmID INT PRIMARY KEY IDENTITY(1,1),
	titolo VARCHAR(255) UNIQUE NOT NULL,
	data_prod DATE NOT NULL,
	nazionalita VARCHAR(255) NOT NULL,
	lingua VARCHAR(255) NOT NULL,
	registaRIF INT NOT NULL,
	FOREIGN KEY (registaRIF) REFERENCES Regista(registaID) ON DELETE CASCADE,
);
-- RELAZIONE ATTORE/FILM N:M
CREATE TABLE Attore_Film(
	attoreRIF INT NOT NULL,
	filmRIF INT NOT NULL,
	FOREIGN KEY (attoreRIF) REFERENCES Attore(attoreID) ON DELETE CASCADE,
	FOREIGN KEY (filmRIF) REFERENCES Film(filmID) ON DELETE CASCADE,
	UNIQUE(filmRIF, attoreRIF),
);
-- RELAZIONE FILM/SUPPORTO 1:1

CREATE TABLE Supporto(
	supportoID INT PRIMARY KEY IDENTITY(1,1),
	posizione VARCHAR(100) DEFAULT 'N.D.',
	tipologia VARCHAR(255) NOT NULL,
	filmRIF INT UNIQUE NOT NULL,
	FOREIGN KEY (filmRIF) REFERENCES Film(filmID) ON DELETE CASCADE,
);

-- DML

INSERT INTO Attore(cognome,nome,luogo_nascita,data_nascita) VALUES
	('Mangano','Luca','Roma','1998-07-27'),
	('Giovanni','Pace','Aquila','1986-09-13'),
	('Mario','Rossi','Viterbo','1967-04-14');

INSERT INTO Regista(cognome,nome,luogo_nascita,data_nascita) VALUES
	('Mangano','Luca','Roma','1998-07-27'),
	('Gianfranco','Spada','Palermo','1986-09-13'),
	('Valeria','Verdi','Firenze','1967-04-14');

INSERT INTO	Film(titolo,data_prod,nazionalita,lingua,registaRIF) VALUES
	('Titanic','1998-01-16','USA','Inglese',1),
	('La Grande Bellezza','2013-05-21','Italiana','Italiano',2),
	('The Judge','2014-10-10','USA','Inglese',3);

INSERT INTO	Supporto(posizione,tipologia,filmRIF) VALUES
	('34F','DVD',1),
	('37D','BLUE-RAY',3),
	('37F','NASTRO',2);
INSERT INTO Attore_Film VALUES
	(1,1),
	(1,2),
	(2,2),
	(2,3),
	(3,1),
	(3,2),
	(3,3);

SELECT * FROM Regista;
SELECT * FROM Film;
SELECT * FROM Attore;
SELECT * FROM Attore_Film;
SELECT * FROM Supporto;

