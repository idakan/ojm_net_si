DROP TABLE IF EXISTS RubricaDue;

CREATE TABLE RubricaDue (
	nome VARCHAR(255),
	cognome VARCHAR(255),
	cod_fis VARCHAR(16) UNIQUE NOT NULL,
	telefono VARCHAR(255) DEFAULT 'N.D.'
);
INSERT INTO RubricaDue (nome, cognome, cod_fis, telefono) VALUES
('Giovanni','Pace','PCAGNN','+39123456'),
('Mario', 'Rossi', 'MRRRSS', '456123'),
('Valeria', 'Verdi', 'VLRVRD', '456124');

SELECT * FROM RubricaDue;

DELETE FROM RubricaDue WHERE cod_fis = 'MRRRSS';

SELECT * FROM RubricaDue;