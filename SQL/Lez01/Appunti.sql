-- CREARE una tabella di note (appunti) che siano caratterizzati da:
-- Titolo
-- Descrizione

-- Creare quattro inserimenti MOCK
-- Creare una query che mi restituisca tutte le note che contengano una stringa predefinita

CREATE TABLE Appunti(
	id INT PRIMARY KEY IDENTITY (1,1),
	titolo VARCHAR(255) NOT NULL,
	descrizione VARCHAR(255)
);

INSERT INTO Appunti(titolo, descrizione) VALUES ('Appunto primo','Un appunto il cui contenuto � riassumibile');
INSERT INTO Appunti(titolo, descrizione) VALUES ('Appunto secondo','Un appunto il cui contenuto non � riassumibile');
INSERT INTO Appunti(titolo, descrizione) VALUES ('TUTT COS','Un appunto wow');
INSERT INTO Appunti(titolo, descrizione) VALUES ('Aaaaaaa','AAaaaaaaaaaaAAAAaaaaa');

SELECT * FROM Appunti WHERE descrizione LIKE '%ri%';
