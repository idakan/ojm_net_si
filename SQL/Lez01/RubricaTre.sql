DROP TABLE IF EXISTS RubricaTre;

CREATE TABLE RubricaTre (
	id INT PRIMARY KEY IDENTITY(1, 1),
	nome VARCHAR(255),
	cognome VARCHAR(255),
	cod_fis VARCHAR(16) UNIQUE NOT NULL,
	telefono VARCHAR(255) DEFAULT 'N.D.',
	email VARCHAR(255)
);
--INSERT INTO RubricaTre (id,nome, cognome, cod_fis, telefono) VALUES
--(1, 'Giovanni','Pace','PCAGNN','+39123456'),
--(2, 'Mario', 'Rossi', 'MRRRSS', '456123'),
--(3, 'Valeria', 'Verdi', 'VLRVRD', '456124');

INSERT INTO RubricaTre (nome, cognome, cod_fis, telefono) VALUES ('Giovanni','Pace','PCAGNN','+39123456');
INSERT INTO RubricaTre (nome, cognome, cod_fis, email) VALUES ('Mario', 'Rossi', 'MRRRSS','mario@rossi.com');
INSERT INTO RubricaTre (nome, cognome, cod_fis) VALUES ('Valeria', 'Verdi', 'VLRVRD');
INSERT INTO RubricaTre (cod_fis, nome, cognome, telefono) VALUES ('MNGLCU', 'Luca', 'Mangano', '3396683536');

INSERT INTO RubricaTre (cod_fis, cognome, nome) VALUES ('MRAMRT', 'Mirta', 'Maria');

-- QL

--SELECT * FROM RubricaTre WHERE nome IN ('Giovanni', 'Maria', 'Mario');
--SELECT * FROM RubricaTre WHERE telefono <> 'N.D.';
--SELECT * FROM RubricaTre WHERE email IS NOT NULL;

-- LIKE

--SELECT * FROM RubricaTre WHERE nome LIKE 'Mar%';
--SELECT * FROM RubricaTre WHERE nome LIKE '%ia';
--SELECT * FROM RubricaTre WHERE nome LIKE '%io%';

-- CREARE una tabella di note (appunti) che siano caratterizzati da:
-- Titolo
-- Descrizione

-- Creare quattro inserimenti MOCK
-- Creare una query che mi restituisca tutte le note che contengano una stringa predefinita