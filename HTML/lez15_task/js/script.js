function salva(){
    let varNome = document.getElementById("inNome").value;
    let varCognome = document.getElementById("inCognome").value;
    let varSesso = document.getElementById("inSesso").value;
    let varMatricola = document.getElementById("inMatricola").value;
    let varIscrizione = document.getElementById("inIscrizione").value;

    if(varMatricola.length == 0){
        alert("Matricola obbligatoria");
        document.getElementById("inMatricola").focus();
        return;
    }

    var persona = {
        nome: varNome,
        cognome: varCognome,
        sesso: varSesso,
        matricola: varMatricola,
        iscrizione: varIscrizione,
    }

    if(isPresente(persona.matricola))
        alert("Matricola già presente!");
    else{
        elenco.push(persona);

        localStorage.setItem("registro", JSON.stringify(elenco))
        stampaElenco()
    }   
}

function isPresente(varMatrDaVerificare){
    for(let item of elenco){
        if(item.matricola == varMatrDaVerificare)
            return true;
    }

    return false;
}

function elimina(varMatr){
    
    for(let [index, item] of elenco.entries()){
        if(item.matricola == varMatr){
            elenco.splice(index, 1)

            localStorage.setItem("registro", JSON.stringify(elenco))

            stampaElenco();
        }
    }
}

function stampaElenco(){

    let contenuto = "";

    for(let item of elenco){

        let colore = "";

        let annoIscritto = (new Date(item.iscrizione)).getFullYear();
        let annoAttuale = (new Date()).getFullYear();
        let anniTrascorsi = annoAttuale - annoIscritto;

        if(anniTrascorsi >= 0 && anniTrascorsi <= 3)
            colore = "text-primary";
        else if(anniTrascorsi > 3 && anniTrascorsi <= 5)
            colore = "text-success";
        else
            colore = "text-danger";


        let riga = `
            <tr class="${colore}">
                <td>${item.nome}</td>
                <td>${item.cognome}</td>
                <td>${item.sesso}</td>
                <td>${item.matricola}</td>
                <td>${item.iscrizione}</td>
                <td>
                    <button type="button" class="btn btn-danger btn-block" onclick="elimina('${item.matricola}')">Elimina</button>
                </td>
            </tr>
        `;

        contenuto += riga;
    }

    document.getElementById("contenuto-tabella").innerHTML = contenuto;

}

if(localStorage.getItem("registro") == null)
    localStorage.setItem("registro", JSON.stringify([]))

let elenco = JSON.parse(localStorage.getItem("registro"));

stampaElenco()


///Creare un elenco della spesa.
///Permettere l'inserimento di un nuovo elemento o la sua eliminazione tramite pulsante.
///HARD: Evitare l'inserimento di doppioni