function stampaTabella(arrContenuto){
    let contenuto = "";
    for(let [index, item] of arrContenuto.entries())
    {
        contenuto += `
                    <tr>
                        <td>${item.nome}</td>
                        <td>${item.cognome}</td>
                        <td>${item.telefono}</td>
                        <td>
                            <button type="button" class="btn btn-primary" onclick="dettaglioContatto(${item.id})">
                                <i class="fa-solid fa-magnifying-glass-plus"></i>
                            </button>
                            <button type="button" class="btn btn-danger" onclick="dettaglioContatto(${item.id})">
                                <i class="fa-solid fa-trash"></i>
                            </button>
                        </td>
                    </tr>
        `;
    }
    $("#contenuto-tabella").html(contenuto);
}

function dettaglioContatto(varId){
    $.ajax(
        {
            url: "https://localhost:44378/api/contatti/" + varId,
            method: "GET",
            success: function(response){
                console.log(response);

                $("#telefono").text(response.telefono);
                $("#nominativo").text(response.nome + " " +response.cognome);
            },
            error: function(errore){
                console.log(errore);
                alert("Errore");
            },
            complete: function(){
                console.log("Ho finito");
            },
        }
    )
}

function eliminaContatto(varId){

    $.ajax(
        {
            url: "https://localhost:44378/api/contatti/" + varId,
            method: "DELETE",
            success: function(response){
                alert("OK");
            },
            error: function(errore){
                console.log(errore);
                alert("Errore");
            }
        }
    )
}
let arrContenuto = []

$(document).ready(
    function(){
        
        $.ajax(
            {
                url: "https://localhost:44378/api/contatti",
                method: "GET",
                success: function(response){
                    stampaTabella(response);
                },
                error: function(errore){
                    console.log(errore.status);
                    alert("Errore")
                }
            }
        );
    }
);