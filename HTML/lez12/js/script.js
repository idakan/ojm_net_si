function inserisci(){
    let varNome = document.getElementById("inNome").value;
    let varCognome = document.getElementById("inCognome").value;

    let persona = {
        nome: varNome,
        cognome: varCognome
    };

    let elemento = `<li>${persona.nome}, ${persona.cognome}</li>`;

    document.getElementById("elenco").innerHTML += elemento;

    document.getElementById("inNome").value = "";
    document.getElementById("inCognome").value = "";

    document.getElementById("inNome").focus();
}

function svuota(){
    document.getElementById("elenco").innerHTML = "";
    document.getElementById("inNome").focus();
}