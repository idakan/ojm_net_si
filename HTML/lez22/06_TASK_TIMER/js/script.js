let counterSeconds = 0;
let counterMinutes = 0;
let counterHours = 0;
let clockTimer;

function start(){
    clockTimer = setInterval( () => {
        $("#spanSeconds").text(convertTime(counterSeconds));
        counterSeconds++;
        if(counterSeconds == 60){
            counterMinutes++;
            counterSeconds = 0;
            $("#spanMinutes").text(convertTime(counterMinutes));
        }
        if(counterMinutes == 60){
            counterHours++;
            counterMinutes = 0;
            $("#spanHours").text(convertTime(counterHours));
        }
    },1000);
}

function stop(){
    clearInterval(clockTimer);
}

function reset(){
    counterSeconds = 0
    counterMinutes = 0
    counterHours = 0
    $("#spanSeconds").text("00");
    $("#spanMinutes").text("00");
    $("#spanHours").text("00");
}

function convertTime(varNumber){
    return ("0"+varNumber).slice(-2);
}

