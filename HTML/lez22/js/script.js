function play(){
    let contaSecondi = 0;
    let contaMinuti = 0;
    let contaOre = 0;

    intervalloSecondi = setInterval(() => {
        if(!isPausa){

            $("#spanSecondi").text(convertiOrario(contaSecondi));
            contaSecondi ++;

            if(contaSecondi == 60){
                contaSecondi = 0;
                contaMinuti ++;
                $("#spanMinuti").text(convertiOrario(contaMinuti));

                if(contaMinuti == 60){
                    
                    contaMinuti = 0;
                    contaOre ++;
                    
                    $("#spanOre").text(convertiOrario(contaOre));
                }
            }     
        }
        
    }, 100); 
        
}

function stop(){
    if(intervalloSecondi){
        clearInterval(intervalloSecondi)
        
        $("#spanSecondi").text(0);
        $("#spanMinuti").text(0);
        $("#spanOre").text(0);
    }
}

function pausa(){
    isPausa = !isPausa;
}

function convertiOrario(varValore){
    let zeri = "0";

    let orario = zeri + varValore;
    return orario.slice(-2);
}

let intervalloSecondi;
let isPausa = false;

$(document).ready(
    function(){

        $("#btnPlay").on("click", function(){
            play();
        })

        $("#btnPausa").click(function(){
            pausa();
        })

        $("#btnStop").click(function(){
            stop();
        })

    }
)