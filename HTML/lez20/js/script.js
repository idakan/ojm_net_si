function salva(){
    let var_nome = document.getElementById("input_nome").value;
    let var_quan = document.getElementById("input_quan").value;

    let oggetto = {
        id: calcolaId(),
        nome: var_nome,
        quan: parseInt(var_quan),
    }

    if(!isPresente(oggetto))
        elenco.push(oggetto)

    stampaTabella();
}

function isPresente(objOggetto){
    for(let [index, item] of elenco.entries()){
        if(objOggetto.nome == item.nome){
            item.quan += objOggetto.quan;
            return true;
        }
    }

    return false;
}

function calcolaId(){
    if(elenco.length != 0)
        return elenco[elenco.length - 1].id + 1;

    return 1;
}

function cancella(varId){
    for(let [index, item] of elenco.entries()){
        if(item.id == varId){
            elenco.splice(index, 1);
        }
    }

    stampaTabella();
}

function apriModale(varId){
    for(let [index, item] of elenco.entries()){
        if(item.id == varId){
            document.getElementById("update_id").value = item.id;
            document.getElementById("update_nome").value = item.nome;
            document.getElementById("update_quan").value = item.quan;
        }
    }

    $("#modaleModifica").modal('show');
}

function aggiorna(){
    let var_id = document.getElementById("update_id").value;
    let var_nome = document.getElementById("update_nome").value;
    let var_quan = document.getElementById("update_quan").value;

    let oggetto = {
        id: var_id,
        nome: var_nome,
        quan: var_quan
    }

    for(let [index, item] of elenco.entries()){
        if(item.id == oggetto.id){
            item.nome = oggetto.nome;
            item.quan = oggetto.quan;

            alert("STAPPOOOOOOOO!");
        }
    }

    $("#modaleModifica").modal('hide');

    stampaTabella();
}

function stampaTabella(){
    let contenuto = "";

    for(let [index, item] of elenco.entries()){
        contenuto += `
            <tr>
                <td>${item.id}</td>
                <td>${item.nome}</td>
                <td>${item.quan}</td>
                <td>
                    <button type="button" class="btn btn-outline-danger" onclick="cancella(${item.id})">
                        <i class="fa-solid fa-trash"></i>
                    </button>

                    <button type="button" class="btn btn-outline-warning" onclick="apriModale(${item.id})">
                        <i class="fa-solid fa-pencil"></i>
                    </button>
                </td>
            </tr>
        `;   
    }

    document.getElementById("contenuto-tabella").innerHTML = contenuto;
}

let elenco = [
    {
        id: 1,
        nome: "Latte",
        quan: 12
    },
    {
        id: 2,
        nome: "Biscotti",
        quan: 2
    },
    {
        id: 3,
        nome: "Caffè",
        quan: 5
    },
];
stampaTabella();

/*
    Creare un'applicazione multipagina che sia in grado di gestire un elenco di libri.
    Ogni libro sarà caratterizzato da:
    - Titolo
    - Autore
    - Codice ISBN
    - Prezzo
    - Quantità

    Predisporre una pagina dedicata al solo inserimento
    Predisporre una pagina dedicata all'elencazione dei libri già inseriti, modifica ed eliminazione.

    HARD: Sulla tabella predisporre un campo di ricerca con un pulsante che come CTA filtri i risultati all'interno della tabella (per Titolo)

    HARD HARD: Alla fine della tabella immettere una riga con il TOTALE dei prezzi illustrati
    La tabella (alla ricerca) aggiorna il totale in tempo reale.
*/