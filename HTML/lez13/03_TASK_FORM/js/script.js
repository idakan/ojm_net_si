/*
CREARE UN FORM comprensivo di: Nome, cognome, matricola studente ed anno di iscrizione
    I dati verranno inseriti all'interno di una tabella le cui righe dovranno sottostare alle seguenti direttive:

    - Se sono iscritto da più di tre anni, colorare tutta la riga di blu (solo il testo)
    - Se sono iscritto da più di tre anni ma meno di cinque, colorare tutta la riga di verde (solo il testo)
    - Se sono iscritto da più di cinque anni, colorare tutta la riga di rosso (solo il testo)
*/
function iscrizione(){
    let varNome = document.getElementById("inNome").value;
    let varCognome = document.getElementById("inCognome").value;
    let varMatricola = document.getElementById("inMatricola").value;
    let varData = document.getElementById("inIscrizione").value;

    let persona = {
        nome: varNome,
        cognome: varCognome,
        matricola: varMatricola,
        data: varData
    }

    let color = "";
    let alreadyExists = exist(persona.matricola);

    if(!alreadyExists)
    {
       elenco.push(persona);
       print();
    }
    else
    {
        alert("Matricola già presente!");
    }
}
function exist(varMatricola)
{
    for(let i = 0; i < elenco.length; i++)
    {
        if(elenco[i].matricola == varMatricola)
            return true;
    }
    return false;
}

function print()
{
    document.getElementById("bodyElement").innerHTML = "";
    for(let i=0; i<elenco.length; i++){

        let elemento = `
            <tr class="${colorRow(elenco[i].data)}">
                <td>${elenco[i].nome}</td>
                <td>${elenco[i].cognome}</td>
                <td>${elenco[i].matricola}</td>
                <td>${elenco[i].data}</td>
            </tr>
        `;

        document.getElementById("bodyElement").innerHTML += elemento;
    }
}

function colorRow(date)
{
    let todayYear = (new Date()).getFullYear();
    let yearPersona = (new Date(date)).getFullYear();
    if(todayYear - yearPersona < 3)
    {
        color = "text-primary"
    }
    if((todayYear - yearPersona >= 3) && (todayYear - yearPersona <= 5))
    {
        color = "text-success";
    }
    if(todayYear - yearPersona >= 5)
    {
        color = "text-danger";
    }
    return color;
}

let elenco = [];


    //    document.getElementById("bodyElement").innerHTML += element;
    //    document.getElementById("inNome").value = "";
    //    document.getElementById("inCognome").value = "";
    //    document.getElementById("inMatricola").value = "";
    //    document.getElementById("inIscrizione").value = "";
    //    document.getElementById("bodyElement").focus();
    //    var element = `
    //    <tr class="${colore}>
    //        <td>${persona.nome}</td>
    //        <td>${persona.cognome}</td>
    //        <td>${persona.matricola}</td>
    //        <td>${persona.data}</td>
    //    </tr>
    //    `;