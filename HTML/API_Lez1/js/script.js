function stampaTabella(){
    $.ajax(
        {
            url: "https://localhost:44378/api/contatti",
            method: "GET",
            success: function(response){

                let contenuto = "";
                for(let [index, item] of response.entries()){
                    contenuto += `  <tr>
                                        <td>${item.nome}</td>
                                        <td>${item.cognome}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary" onclick="dettaglioContatto(${item.id})">
                                                <i class="fa-solid fa-magnifying-glass-plus"></i>
                                            </button>
                                            
                                            <button type="button" class="btn btn-danger" onclick="eliminaContatto(${item.id})">
                                                <i class="fa-solid fa-trash"></i>
                                            </button>
                                        </td>
                                    </tr>`;
                }

                $("#contenuto-tabella").html(contenuto);

            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

function eliminaContatto(varId){
    
    $.ajax(
        {
            url: "https://localhost:44378/api/contatti/" + varId,
            method: "DELETE",
            success: function(risposta){
                switch(risposta.result){
                    case "success":

                        $(".nascosto-default").addClass("d-none");

                        $("#telefono").empty();
                        $("#nominativo").empty();

                        stampaTabella();                //Aggiorno il contenuto della tabella solo se va tutto a buon fine!
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            },
            complete: function(){
                console.log("Ho finito");
            }
        }
    );

}

function dettaglioContatto(varId){

    $.ajax(
        {
            url: "https://localhost:44378/api/contatti/" + varId,
            method: "GET",
            success: function(response){
                $(".nascosto-default").removeClass("d-none");

                $("#telefono").text(response.telefono);
                $("#nominativo").text(response.nome + " " +response.cognome);

                $("#btnModifica").data("identificativo", varId);
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );

}

function salvaContatto(){
    let input_nome = $("#input_nome").val();
    let input_cognome = $("#input_cognome").val();
    let input_telefono = $("#input_telefono").val();

    let contatto = {
        nome: input_nome,
        cognome: input_cognome,
        telefono: input_telefono
    }

    $.ajax(
        {
            url: "https://localhost:44378/api/contatti/insert",
            method: "POST",
            data: JSON.stringify(contatto),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "success":
                        stampaTabella();
        
                        $("#input_nome").val("");
                        $("#input_cognome").val("");
                        $("#input_telefono").val("");
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

function riempiPerModifica(varId){
    $.ajax(
        {
            url: "https://localhost:44378/api/contatti/" + varId,
            method: "GET",
            success: function(response){
                $("#input_nome").val(response.nome);
                $("#input_cognome").val(response.cognome);
                $("#input_telefono").val(response.telefono);

                $(".btn-salvataggio").data("iden", response.id);
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

function invioModificheContatto(varId){

    let input_nome = $("#input_nome").val();
    let input_cognome = $("#input_cognome").val();
    let input_telefono = $("#input_telefono").val();

    let contatto = {
        nome: input_nome,
        cognome: input_cognome,
        telefono: input_telefono
    }

    $.ajax(
        {
            url: "https://localhost:44378/api/contatti/" + varId,
            method: "PUT",
            data: JSON.stringify(contatto),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "success":
                        stampaTabella();
        
                        $("#input_nome").val("");
                        $("#input_cognome").val("");
                        $("#input_telefono").val("");

                        
                        $(".nascosto-default").addClass("d-none");

                        $("#telefono").empty();
                        $("#nominativo").empty();

                        $(".btn-salvataggio").data("iden", "");
                        break;
                    case "error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}

$(document).ready(
    function(){

        stampaTabella();

        $(".btn-salvataggio").click(function(){
            if($(this).data("iden") && $(this).data("iden") != ""){
                invioModificheContatto($(this).data("iden"));
            }
            else{
                salvaContatto();
            }
        })

        $("#btnModifica").click(function(){
            riempiPerModifica( $(this).data("identificativo") );
        })

    }
)