let elenco = []

function invia()
{
    let varElement = document.getElementById("inElement").value;
    let varQuantity = document.getElementById("inQuantity").value;

    let element = {
        nome: varElement,
        quantity: varQuantity,
    }
    addElement(element);
    print();
}

function addElement(element)
{
    pushToElenco(element); //Aggiungo l'elemento all'elenco
    pushToStorage(element); //Aggiungo l'elemento allo storage
}
function pushToStorage(element)
{
    let isPresent = localStorage.getItem(element.nome);
    
    if(isPresent != null) //Trovato articolo
    {
        localStorage.setItem(element.nome, Number(isPresent)+Number(element.quantity));
    }
    else //Non ho trovato l'articolo
    {
        localStorage.setItem(element.nome, element.quantity);
    }
}

function pushToElenco(element)
{
    for(let i = 0; i < elenco.length; i++)
    {
        if(elenco[i].nome == element.nome) //Se l'articolo è presente nell'elenco lo incremento
        {
            elenco[i].quantity = Number(elenco[i].quantity) + Number(element.quantity);
            return;
        }
    }
    elenco.push(element); //Articolo non trovato nell'elenco, lo inizializzo.

}

function print()
{
    let content = "";

    for(let item of elenco)
    {
        content += `
        <tr>
            <td>${item.nome}</td>
            <td>${item.quantity}</td>
            <td><button type="button" onclick="deleteItem('${item.nome}')" class="btn btn-primary btn-danger">X</button></td>
        </tr>
        `;
    }

    document.getElementById("tableBody").innerHTML = content;
}

function deleteItem(nome)
{
    for(let [index, item] of elenco.entries())
    {
        if(item.nome == nome)
        {
            elenco.splice(index, 1);
        }
    }
    localStorage.removeItem(nome);
    print();
}