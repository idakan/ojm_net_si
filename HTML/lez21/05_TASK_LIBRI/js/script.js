if(localStorage.getItem("Biblioteca") == null)
    localStorage.setItem("Biblioteca", JSON.stringify([]));

let elenco = JSON.parse(localStorage.getItem("Biblioteca"));
let elencoTemp = [];


if(document.getElementById("tableBody") != null)
    printTable();

function send(){
    let varTitolo = document.getElementById("inTitolo").value;
    let varAutore = document.getElementById("inAutore").value;
    let varIsbn = document.getElementById("inIsbn").value;
    let varPrezzo = document.getElementById("inPrezzo").value;
    let varQuantita = document.getElementById("inQuantita").value;

    let book = {
        id: getId(),
        titolo: varTitolo,
        autore: varAutore,
        isbn: varIsbn,
        prezzo: varPrezzo,
        quantita: varQuantita,
    }

    pushElement(book);
}

function getId(){ //Calcola l'id
    if(elenco.length == 0)
        return 1;
    return elenco[elenco.length - 1].id + 1;
}

function printTable(){ //Stampa il contenuto di "elenco[]" sotto forma di tabella
    let content = "";
    for(let item of elenco)
    {
        
        content += `
        <tr>
            <td>${item.titolo}</td>
            <td>${item.autore}</td>
            <td>${item.isbn}</td>
            <td>${item.prezzo}€</td>
            <td>${item.quantita}</td>
            <td><button type="button" class="btn btn-danger" onclick="deleteElement('${item.id}')"><i class="fa-solid fa-trash-can"></i></button></td>
            <td><button type="button" class="btn btn-primary" onclick="openModal('${item.id}')"><i class="fa-solid fa-pencil"></i></button></td>
        </tr>
        `;
    }
    document.getElementById("tableBody").innerHTML = content;
    getTotale(elenco);
}



function pushElement(book){ //Inserisco l'elemento nell'elenco e nel localStorage
    pushToElenco(book);
    localStorage.setItem("Biblioteca", JSON.stringify(elenco));
}

function pushToElenco(book) //Inserisco l'elemento sull'elenco
{
    //TRUE : Elemento trovato e aumentato
    //FALSE : Elemento non trovato e inizializzato
    for(let item of elenco)
    {
        if(item.isbn == book.isbn)
        {
            item.quantita = parseFloat(item.quantita) + parseFloat(book.quantita);
            console.log(item.quantita);
            return true;
        }
    }
    elenco.push(book);
    return false;
}

function openModal(id){ //Apro la modal, mostrandola a schermo
    for(let [index, item] of elenco.entries()){
        if(item.id == id){
            document.getElementById("updateId").value = item.id;
            document.getElementById("updateTitolo").value = item.titolo;
            document.getElementById("updateAutore").value = item.autore;
            document.getElementById("updateISBN").value = item.isbn;
            document.getElementById("updatePrezzo").value = item.prezzo;
            document.getElementById("updateQuantita").value = item.quantita;
        }
    }

    $("#modalUpdate").modal('show');
}

function update(){
    let varId = document.getElementById("updateId").value;
    let varTitolo = document.getElementById("updateTitolo").value;
    let varAutore = document.getElementById("updateAutore").value;
    let varIsbn = document.getElementById("updateISBN").value;
    let varPrezzo = document.getElementById("updatePrezzo").value;
    let varQuantita = document.getElementById("updateQuantita").value;

    let book = {
        id: varId,
        titolo: varTitolo,
        autore: varAutore,
        isbn: varIsbn,
        prezzo: varPrezzo,
        quantita: varQuantita,
    }

    for(let [index, item] of elenco.entries()){
        if(item.id == book.id){
            item.titolo = book.titolo;
            item.autore = book.autore;
            item.isbn = book.isbn;
            item.prezzo = book.prezzo;
            item.quantita = book.quantita;
        }
    }
    
    $("#modalUpdate").modal('hide');

    printTable();
}

function deleteElement(id){
    for(let [index, item] of elenco.entries())
    {
        if(item.id == id)
        {
            elenco.splice(index, 1);
        }
    }
    localStorage.setItem("Biblioteca", JSON.stringify(elenco));
    printTable();
}

function searchByTitolo(){
    elencoTemp = [];
    let title = document.getElementById("searchTitle").value;
    if(title == "")
    {
        printTable();
        return;
    }
    for(let item of elenco)
    {
        if(item.titolo == title)
        {
            elencoTemp.push(item);
        }
    }
    
    printSearch();
}

function printSearch(){
    let content = "";
    for(let item of elencoTemp)
    {
        
        content += `
        <tr>
            <td>${item.titolo}</td>
            <td>${item.autore}</td>
            <td>${item.isbn}</td>
            <td>${item.prezzo}€</td>
            <td>${item.quantita}</td>
            <td><button type="button" class="btn btn-danger" onclick="deleteElement('${item.id}')"><i class="fa-solid fa-trash-can"></i></button></td>
            <td><button type="button" class="btn btn-primary" onclick="openModal('${item.id}')"><i class="fa-solid fa-pencil"></i></button></td>
        </tr>
        `;
    }
    document.getElementById("tableBody").innerHTML = content;
    getTotale(elencoTemp);
}

function getTotale(currElenco){
    let totale = 0;
    for(let item of currElenco)
    {
        totale += item.prezzo*item.quantita;
    }
    let content = `
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td>${totale}€</td>
        <td></td>
        <td></td>
    </tr>
    `;
    document.getElementById("tableFoot").innerHTML = content;
}

function clearFilter(){
    document.getElementById("searchTitle").value = "";
    document.getElementById("searchTitle").focus();
    elencoTemp.splice(0,elencoTemp.length);
    printTable();
}