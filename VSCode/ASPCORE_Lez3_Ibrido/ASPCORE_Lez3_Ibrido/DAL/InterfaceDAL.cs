﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez3_Ibrido.DAL
{
    interface InterfaceDAL<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int varId);
        bool Insert(T t);
    }
}
