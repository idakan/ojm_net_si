#pragma checksum "C:\Users\UTENTE\source\repos\ASPCORE_Lez3_Ibrido\ASPCORE_Lez3_Ibrido\Views\Home\Dettaglio.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "27a78f2ebc5153cd75534b881c026e791f448002"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Dettaglio), @"mvc.1.0.view", @"/Views/Home/Dettaglio.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"27a78f2ebc5153cd75534b881c026e791f448002", @"/Views/Home/Dettaglio.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b853c2693020103458b52d8246f923ca3fd89014", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Dettaglio : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<ASPCORE_Lez3_Ibrido.Models.Studente>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n<h1>Studente: ");
#nullable restore
#line 3 "C:\Users\UTENTE\source\repos\ASPCORE_Lez3_Ibrido\ASPCORE_Lez3_Ibrido\Views\Home\Dettaglio.cshtml"
         Write(Model.Matricola);

#line default
#line hidden
#nullable disable
            WriteLiteral("</h1>\r\n\r\n<ul>\r\n    <li>Nome: ");
#nullable restore
#line 6 "C:\Users\UTENTE\source\repos\ASPCORE_Lez3_Ibrido\ASPCORE_Lez3_Ibrido\Views\Home\Dettaglio.cshtml"
         Write(Model.Nome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</li>\r\n    <li>Nome: ");
#nullable restore
#line 7 "C:\Users\UTENTE\source\repos\ASPCORE_Lez3_Ibrido\ASPCORE_Lez3_Ibrido\Views\Home\Dettaglio.cshtml"
         Write(Model.Cognome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</li>\r\n</ul>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<ASPCORE_Lez3_Ibrido.Models.Studente> Html { get; private set; }
    }
}
#pragma warning restore 1591
