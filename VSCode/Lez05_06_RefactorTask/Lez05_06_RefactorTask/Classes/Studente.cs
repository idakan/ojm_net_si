﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_06_RefactorTask.Classes
{
    public class Studente
    {
        public string Nominativo { get; set; }
        public string Citta { get; set; }

        private int cfu;

        public int Cfu
        {
            get { return cfu; }
            set
            {
                if (value < 0)
                    cfu = 0;
                else
                    cfu = value;
            }
        }


        private int matricola;

        public int Matricola
        {
            get { return matricola; }
            set { matricola = value; }
        }

        /// <summary>
        /// Costruttore di default, nessuna implementazione particolare.
        /// </summary>
        public Studente()
        {

        }

        /// <summary>
        /// Costruttore esplicito per la creazione automatica di uno studente con tutti i campi
        /// inseriti in automatico.
        /// </summary>
        /// <param name="varNominativo">Nominativo, senza limite di lunghezza</param>
        /// <param name="varMatricola">Matricola, senza limite di lunghezza</param>
        /// <param name="varCitta">Città, senza limite di lunghezza</param>
        /// <param name="varCfu">Cfu, da comprendere tra 0 e 180</param>
        public Studente(string varNominativo, int varMatricola, string varCitta, int varCfu)
        {
            Nominativo = varNominativo;
            Matricola = varMatricola;
            Citta = varCitta;
            Cfu = varCfu;
        }

        public override string ToString()
        {
            return $"Matricola: {Matricola} - Città: {Citta} - CFU: {Cfu} - ANNO: {verificaAnno()}";
        }

        public string verificaAnno()
        {
            string annoIscrizione = "N.D.";
            if (Cfu >= 0 && Cfu <= 60)
                annoIscrizione = "Primo anno";
            else if (Cfu > 60 && Cfu <= 120)
                annoIscrizione = "Secondo anno";
            else if (Cfu > 120 && Cfu <= 180)
                annoIscrizione = "Terzo anno";

            return annoIscrizione;
        }
    }
}
