﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_06_RefactorTask.Classes
{
    public class GestoreStudente
    {
        List<Studente> elenco = new List<Studente>();

        public GestoreStudente()
        {
            elenco.Add(new Studente("Giovanni Pace", 12345, "Avezzano", 150));
            elenco.Add(new Studente("Mario Rossi", 12346, "Roma", 130));
            elenco.Add(new Studente("Valeria Verdi", 12347, "Milano", 60));
        }

        public bool inserisciStudente(
            string varNominativo, 
            string varMatricola, 
            string varCitta,
            string varCfu)
        {
            int matrConvertita, cfuConvertito;

            try
            {
                matrConvertita = Convert.ToInt32(varMatricola);
                cfuConvertito = Convert.ToInt32(varCfu);
            } catch(Exception e)
            {
                return false;
            }

            Studente temp = new Studente(varNominativo, matrConvertita, varCitta, cfuConvertito);
            elenco.Add(temp);

            return true;
        }

        public void listaStudenti()
        {
            foreach(Studente item in elenco)
                Console.WriteLine(item);
        }

        public Studente cercaStudente(string varMatricola)
        {
            int matricolaConvertita;
            try
            {
                matricolaConvertita = Convert.ToInt32(varMatricola);

                foreach(Studente item in elenco)
                {
                    if (item.Matricola.Equals(matricolaConvertita))
                    {
                        return item;
                    }
                }

            } catch(Exception e)
            {
                Console.WriteLine("Qualcosa è andato storto nei parametri inseriti!");
            }

            return null;
        }

        public bool eliminaStudente(string varMatricola)
        {
            Studente temp = cercaStudente(varMatricola);

            return elenco.Remove(temp);
        }

        public bool modificaStudente(
            string varMatricola,
            string varNominativo,
            string varCitta,
            string varCfu)
        {
            Studente temp = cercaStudente(varMatricola);

            int cfuConvertiti;
            try
            {
                cfuConvertiti = Convert.ToInt32(varCfu);
            }
            catch(Exception e)
            {
                return false;                           //Caso in cui non riesco a convertire
            }

            if(temp != null)
            {
                temp.Nominativo = varNominativo;
                temp.Citta = varCitta;
                temp.Cfu = cfuConvertiti;

                return true;
            }
            else
            {
                return false;
            }
        }

        public string verificaRisultati(string varMatricola)
        {
            Studente temp = cercaStudente(varMatricola);

            if(temp != null)
            {
                return temp.verificaAnno();
            }
            else
            {
                return "Errore, matricola non trovata!";
            }
        }
    }
}
