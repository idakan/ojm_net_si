﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_04_TaskCasa.Classes
{
    public class Oggetto
    {
        public string Nome { get; set; }
        public string Descrizione { get; set; }

        public float Valore { get; set; }

        public override string ToString()
        {
            return $"{Nome} - {Descrizione} - Valore: {Valore}€";
        }
    }
}
