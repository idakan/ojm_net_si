﻿using System;

namespace Lez02_01_VerificaInput
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * Prendere in input il Nome, Cognome e l'età di una persona e stampare:
             * Giovanni Pace - Maggiorenne/Minorenne
             */

            //Console.WriteLine("Inserisci il nome:");
            //string nome = Console.ReadLine();

            //Console.WriteLine("Inserisci il cognome:");
            //string cognome = Console.ReadLine();

            //Console.WriteLine("Inserisci l'età:");
            //string eta = Console.ReadLine();

            //int etaConvertita = Convert.ToInt32(eta);

            //string fraseMagMin;

            //if(etaConvertita >= 18)
            //{
            //    fraseMagMin = "Maggiorenne";
            //}
            //else
            //{
            //    fraseMagMin = "Minorenne";
            //}

            //string frase = $"{nome} {cognome} - {fraseMagMin}";

            //Console.WriteLine(frase);

            //-------------------------------

            Console.WriteLine("Inserisci il nome:");
            string nome = Console.ReadLine();

            Console.WriteLine("Inserisci il cognome:");
            string cognome = Console.ReadLine();

            Console.WriteLine("Inserisci l'età:");
            string eta = Console.ReadLine();

            int etaConvertita = Convert.ToInt32(eta);

            string fraseMagMin = "Minorenne";

            if (etaConvertita >= 18)
                fraseMagMin = "Maggiorenne";

            string frase = $"{nome} {cognome} - {fraseMagMin}";

            Console.WriteLine(frase);


        }
    }
}
