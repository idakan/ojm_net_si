﻿using Lez04_06_ClasseTipo.Classes;
using System;

namespace Lez04_06_ClasseTipo
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona giovanni = new Persona();
            giovanni.Nome = "Giovanni";
            giovanni.Cognome = "Pace";
            giovanni.CodFis = "PCAGNN";

            Indirizzo indSped = new Indirizzo();
            indSped.Via = "Via le mani dal naso";
            indSped.Civico = "12";
            indSped.Cap = "12345";
            indSped.Citta = "Milano";
            indSped.Provincia = "MI";

            giovanni.IndirizzoSpedizione = indSped;

            giovanni.IndirizzoFatturazione = new Indirizzo()
            {
                Via = "Via degli olmi",
                Civico = "155A",
                Cap = "96325",
                Citta = "Bologna",
                Provincia = "BO"
            };

            Console.WriteLine(giovanni);
        }
    }
}
