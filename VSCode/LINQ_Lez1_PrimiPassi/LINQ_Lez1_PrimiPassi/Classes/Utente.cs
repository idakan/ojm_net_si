﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ_Lez1_PrimiPassi.Classes
{
    class Utente
    {
        public int Id { get; set; }
        public string Nominativo { get; set; }
        public int Eta { get; set; }
        public string Sesso { get; set; }

        public List<string> Interessi { get; set;}

        public Utente()
        {
            Interessi = new List<string>();
            Interessi.Add("Storia");
            Interessi.Add("Informatica");
            Interessi.Add("Fisica");
        }

        public override string ToString()
        {
            return $"{Id} {Nominativo} {Eta} {Sesso}";
        }
    }
}
