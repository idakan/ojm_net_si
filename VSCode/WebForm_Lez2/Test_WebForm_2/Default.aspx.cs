﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Test_WebForm_2.Models;

namespace Test_WebForm_2
{
    public partial class _Default : Page
    {
        public string Errore { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            Errore = (string)Session["errore"];
            Session["errore"] = "";
        }

        protected void btnInvia_Click(object sender, EventArgs e)
        {
            #region esempio di invocazione sessione
            //string varNome = nome.Text;
            //string varCognome = cognome.Text;

            //Session["sesNome"] = varNome;
            //Session["sesCognome"] = varCognome;
            #endregion

            string username = tbUser.Text;
            string password = tbPass.Text;

            //SELECT * FROM Utenti WHERE username = ... AND password = MD5(...)
            if(username.Equals("giovanni") && password.Equals("pace"))
            {
                Utente user = new Utente() {
                    Username = username
                };

                Session["utente"] = user;
                Response.Redirect("Iscrizione.aspx");
            }

            Response.Redirect("Errore.aspx");
        }
    }
}