﻿//------------------------------------------------------------------------------
// <generato automaticamente>
//     Codice generato da uno strumento.
//
//     Le modifiche a questo file possono causare un comportamento non corretto e verranno perse se
//     il codice viene rigenerato. 
// </generato automaticamente>
//------------------------------------------------------------------------------

namespace Test_WebForm_2
{


    public partial class About
    {

        /// <summary>
        /// Controllo lblUsername.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblUsername;

        /// <summary>
        /// Controllo lblDataLogin.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Label lblDataLogin;

        /// <summary>
        /// Controllo btnLogout.
        /// </summary>
        /// <remarks>
        /// Campo generato automaticamente.
        /// Per la modifica, spostare la dichiarazione di campo dal file di progettazione al file code-behind.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnLogout;
    }
}
