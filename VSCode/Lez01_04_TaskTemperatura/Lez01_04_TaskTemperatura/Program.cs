﻿using System;

namespace Lez01_04_TaskTemperatura
{
    class Program
    {
        static void Main(string[] args)
        {

            //--------------- TEST INPUT
            //Console.WriteLine("Inserisci la frase");

            //string variabile = Console.ReadLine();
            //Console.WriteLine($"La stringa inserita è: {variabile}");



            /**
             * Creare un sistema di controllo ingressi al ristorante:
             * In input avremo la temperatura (salvata in una variabile).
             * 
             * Se la temperatura è maggiore o uguale a 37,5° allora vietare l'ingresso al ristorante,
             * se è inferiore permettere l'ingresso!
             * 
             * ATTENZIONE, non ci vuole un medico per dire che sotto i 35 sei morto e sopra i 42 direi
             * che non è un valore accettabile!
             */

            //float temperatura = 3.5f;

            Console.WriteLine("Inserire temperatura");
            double temperatura = Convert.ToDouble(Console.ReadLine());

            //if (temperatura <= 35 || temperatura >= 42)
            //{
            //    Console.WriteLine("Errore ;(");
            //}
            //else
            //{
            //    if (temperatura <= 37.5f)
            //    {
            //        Console.WriteLine("Puoi entrare");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Non puoi entrare! ;(");
            //    }
            //}

            //Equivalente

            if (temperatura > 35 && temperatura < 42)
            {
                if (temperatura <= 37.5f)
                {
                    Console.WriteLine("Puoi entrare");
                }
                else
                {
                    Console.WriteLine("Non puoi entrare! ;(");
                }
            }
            else
            {
                Console.WriteLine("Errore ;(");
            }

        }
    }
}
