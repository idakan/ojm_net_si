﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_02_TaskEreditarieta.Classes
{
    public class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public Persona()
        {

        }

        public Persona(string varNome, string varCognome)
        {
            Nome = varNome;
            Cognome = varCognome;
        }

        public override string ToString()
        {
            return $"Persona: {Nome}, {Cognome}";
        }
    }
}
