﻿using System;

namespace Lez01_04_ControlliSemplici
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
             * if(condizione){
             *          corpo del codice nel caso la condizione sia verificata e positiva
             *}
             * else{
             *          corpo del codice nel caso la condizione non sia verificata o negativa
             *}
             */

            //int eta = 18;

            //if (eta >= 18)
            //{
            //    Console.WriteLine("Sei maggiorenne");
            //}
            //else
            //{
            //    Console.WriteLine("Sei minorenne");
            //}

            //Console.WriteLine(16 == 16);

            /* Possibili operatori:
             * >
             * <
             * >=
             * <=
             * ==
             * !=
             */

            //if (true)
            //{
            //    Console.WriteLine("Sono nel ramo IF");
            //}
            //else
            //{
            //    Console.WriteLine("Sono nel ramo ELSE");
            //}

            //Validazione NAIV

            //int eta = 18;

            //if(eta < 120)
            //{
            //    if (eta < 0)
            //    {
            //        Console.WriteLine("Troppo piccolo, sei un embrione... nemmeno");
            //    }
            //    else
            //    {
            //        if (eta >= 18)
            //        {
            //            Console.WriteLine("Sei maggiorenne");
            //        }
            //        else
            //        {
            //            Console.WriteLine("Sei minorenne");
            //        }
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Eeeeeeeeeh... troppo elevata");
            //}

            //------------------------------- Operatori Logici

            //int eta = 19;

            ////AND
            //// true     &&    true          = true
            //// 1        *        1          = 1

            //// false    &&    false         = false
            //// 0        *        0          = 0

            //if(eta > 0 && eta < 120)
            //{
            //    Console.WriteLine("OK");
            //}
            //else
            //{
            //    Console.WriteLine("Errore");
            //}

            ////OR
            //// true     &&    false         = true
            //// 1        *        0          = 1

            //// false    &&    true          = true
            //// 0        *        1          = 1
            //int eta = 190; 

            //if (eta > 0 || eta < 120)
            //{
            //    Console.WriteLine("OK");
            //}
            //else
            //{
            //    Console.WriteLine("Errore");
            //}

            //int eta = -20;

            //if(eta > 0 && eta < 120)
            //{
            //    if(eta >= 18)
            //    {
            //        Console.WriteLine("Sei Maggiorenne");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Sei Minorenne");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Valore non valido");
            //}

            //---------------------- Negazione

            //bool stato = true;

            //stato = !stato;

            //if(stato)
            //{
            //    Console.WriteLine("Sono nel ramo IF");
            //}
            //else
            //{
            //    Console.WriteLine("Sono nel ramo ELSE");
            //}

            Console.WriteLine(!true);
                
        }
    }
}
