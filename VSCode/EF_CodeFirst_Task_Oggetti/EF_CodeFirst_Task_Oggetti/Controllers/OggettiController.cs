﻿using EF_CodeFirst_Task_Oggetti.Data;
using EF_CodeFirst_Task_Oggetti.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Task_Oggetti.Controllers
{
    [ApiController]
    [Route("api/oggetti")]
    public class OggettiController : Controller
    {
        private readonly IRepo<Oggetto> _repository;

        public OggettiController(IRepo<Oggetto> rep)
        {
            _repository = rep;
        }

        #region Elenco degli oggetti
        [HttpGet]                           //GET - api/oggetti
        public ActionResult<IEnumerable<Oggetto>> RestituisciContatti()
        {
            var elenco = _repository.GetAll();

            return Ok(elenco);
        }
        #endregion

        #region Inserimento dell'oggetto
        [HttpPost("inserisci")]             //POST - api/oggetti/inserisci
        public ActionResult<Risposta> InserisciContatto(Oggetto objOggetto)
        {
            Oggetto temp = _repository.FindByCode(objOggetto.Codice);

            if(temp == null)
            {
                if (_repository.Insert(objOggetto))
                    return Ok(new Risposta() { Status = "success", Descrizione = "" });
                else
                    return Ok(new Risposta() { Status = "error", Descrizione = "Operazione non riuscita!" });
            }
            else
            {
                temp.Quantita += objOggetto.Quantita;

                if (_repository.Update(temp))
                    return Ok(new Risposta() { Status = "success", Descrizione = "" });
                else
                    return Ok(new Risposta() { Status = "error", Descrizione = "Operazione non riuscita!" });
            }
            

        }
        #endregion

        #region Dettaglio dell'oggetto
        [HttpGet("dettaglio/{varCodice}")]      //GET - api/oggetti/dettaglio/BLABLA1234156
        public ActionResult<Oggetto> CercaPerCodice(string varCodice)
        {
            return Ok(_repository.FindByCode(varCodice));
        }
        #endregion

        #region Eliminazione dell'oggetto
        [HttpDelete("elimina/{varCodice}")]
        public ActionResult<Risposta> EliminaPerCodice(string varCodice)
        {
            Oggetto temp = _repository.FindByCode(varCodice);

            if(temp != null)
            {
                if (_repository.Delete(temp.Id))
                    return Ok(new Risposta() { Status = "success", Descrizione = "" });
                else
                    return Ok(new Risposta() { Status = "error", Descrizione = "Errore di eliminazione!" });
            }
            else
            {
                return Ok(new Risposta() { Status = "error", Descrizione = "Elemento non trovato!"});
            }
        }
        #endregion



    }
}
