﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Task_Oggetti.Models
{
    public class Risposta
    {
        public string Status { get; set; }
        public string Descrizione { get; set; }
    }
}
