﻿using Lez05_02_ContenitoriComplessi.Classes;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Lez05_02_ContenitoriComplessi
{
    class Program
    {
        static void Main(string[] args)
        {
            ////List<object> elenco = new List<object>();     //Comportamento simile ad ArrayList per il tipi di dato interno alla memoria

            ////elenco.Add(5);
            ////elenco.Add("Giovanni");

            ////----------------------------------------------

            //List<Contatto> rubrica = new List<Contatto>();

            //Contatto giovanni = new Contatto("Giovanni", "Pace", "giovanni@pace.com", "123456");
            //Contatto mario = new Contatto("Mario", "Rossi", "mario@rossi.com", "123457");
            //Contatto valeria = new Contatto()
            //{
            //    Nome = "Valeria",
            //    Cognome = "Verdi",
            //    Email = "valeria@verdi.com",
            //    Telefono = "789456"
            //};

            //rubrica.Add(giovanni);
            //rubrica.Add(mario);
            //rubrica.Add(valeria);

            ////for(int i=0; i<rubrica.Count; i++)
            ////{
            ////    Console.WriteLine(rubrica[i]);
            ////}


            //try
            //{
            //    Console.WriteLine(rubrica[15]);
            //}
            //catch (ArgumentOutOfRangeException e)
            //{
            //    Console.WriteLine("Fuori dal range!");
            //}

            ////---------------------------------------------------

            ////Console.WriteLine("Inserisci un numero");
            ////string numero = Console.ReadLine();

            /////*
            //// * try{
            //// *      Corpo del try
            //// * } catch (... tipo di eccezione ...){
            //// *      Corpo del catch
            //// * }
            //// */

            ////try
            ////{
            ////    int numeroConvertito = Convert.ToInt32(numero);

            ////    Console.WriteLine(numeroConvertito * 10);
            ////} catch (Exception e)
            ////{
            ////    Console.WriteLine(e.Message);
            ////}

            ////while (true)
            ////{
            ////    try
            ////    {
            ////        Console.WriteLine("Inserisci un numero");
            ////        string numero = Console.ReadLine();

            ////        int numeroConvertito = Convert.ToInt32(numero);
            ////        Console.WriteLine(numeroConvertito * 10);
            ////    }
            ////    catch (Exception e)
            ////    {
            ////        Console.WriteLine(e.Message);
            ////    }
            ////}
            ///

            //---------------------------------------------

            List<Contatto> rubrica = new List<Contatto>();

            bool inserimentoAbilitato = true;

            while (inserimentoAbilitato)
            {
                Console.WriteLine("Inserisci il nome o digita Q per uscire");
                string nome = Console.ReadLine();

                if (nome.Equals("Q"))
                    break;

                Console.WriteLine("Inserisci il cognome");
                string cognome = Console.ReadLine();

                Console.WriteLine("Inserisci il mail");
                string mail = Console.ReadLine();

                Console.WriteLine("Inserisci il telefono");
                string telefono = Console.ReadLine();

                Contatto temp = new Contatto(nome, cognome, mail, telefono);
                rubrica.Add(temp);

                //rubrica.Add(new Contatto(nome, cognome, mail, telefono));

                //rubrica.Add(new Contatto()
                //{
                //    Nome = nome,
                //    Cognome = cognome,
                //    Email = mail,
                //    Telefono = telefono
                //}) ;
            }

            foreach (Contatto item in rubrica)
            {
                Console.WriteLine(item);
            }
        }
    }
}
