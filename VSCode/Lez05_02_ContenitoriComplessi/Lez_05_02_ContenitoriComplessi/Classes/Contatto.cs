﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez_05_02_ContenitoriComplessi.Classes
{
    public class Contatto
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }

        public Contatto() { }
        public Contatto(string varNome, string varCognome, string varEmail, string varTelefono)
        {
            Nome = varNome;
            Cognome = varCognome;
            Email = varEmail;
            Telefono = varTelefono;
        }
        public override string ToString()
        {
            return $"{Nome} {Cognome}\n" +
                $"Telefono: {Telefono}\n" +
                $"Email   : {Email}\n";
        }
    }
}
