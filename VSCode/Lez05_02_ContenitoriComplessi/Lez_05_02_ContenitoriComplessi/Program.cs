﻿using Lez_05_02_ContenitoriComplessi.Classes;
using System;
using System.Collections.Generic;

namespace Lez_05_02_ContenitoriComplessi
{
    class Program
    {
        static void Main(string[] args)
        {
            //List<object> elenco = new List<object>();                 //Comportamento simile ad ArrayList per il suo utilizzo

            //elenco.Add(5);
            //elenco.Add("Giovanni");

            //---------------------------------------------------------------------

            List<Contatto> rubrica = new List<Contatto>();

            Contatto giovanni = new Contatto("Giovanni", "Pace", "giovanni@pace.com", "123456");
            Contatto mario = new Contatto("Mario", "Rossi", "mario@rossi.com", "123457");
            Contatto valeria = new Contatto() {
                Nome = "Valeria",
                Cognome = "Verdi",
                Email = "valeria@verdi.com",
                Telefono = "789456"
            };

            bool inserimentoAbilitato = true;

            while (true)
            {
                Console.WriteLine("Inserisci il nome o digita Q per uscire");
                string nome = Console.ReadLine();

                if (nome.Equals("Q"))
                    break;

                Console.WriteLine("Inserisci il cognome");
                string cognome = Console.ReadLine();

                Console.WriteLine("Inserisci l'email");
                string email = Console.ReadLine();

                Console.WriteLine("Inserisci il telefono");
                string telefono = Console.ReadLine();

                Contatto temp = new Contatto(nome, cognome, email, telefono);
                rubrica.Add(temp);

                //rubrica.Add(new Contatto(nome, cognome, email, telefono));

                //rubrica.Add(new Contatto()
                //{
                //    Nome = nome,
                //    Cognome = cognome,
                //    Email = email,
                //    Telefono = telefono
                //});
            }
            
            foreach(Contatto item in rubrica)
            {
                Console.WriteLine(item);
            }

            //rubrica.Add(giovanni);
            //rubrica.Add(mario);
            //rubrica.Add(valeria);

            //for (int i = 0; i < rubrica.Count; i++)
            //{
            //    Console.WriteLine(rubrica[i]);
            //}
            
            
            //try
            //{
            //    Console.WriteLine(rubrica[15]);
            //} catch (ArgumentOutOfRangeException e)
            //{
            //    Console.WriteLine("Fuori dal range!");
            //}
            //catch (ArgumentNullException a)
            //{
            //    Console.WriteLine(a.Message);
            //}


            //-----------------------------------------------------------

            ///*
            // * try{
            // *      Corpo del try
            // * } catch ( ... tipo di eccezione ... ){
            // *      Corpo del catch
            // * }
            // */

            //try
            //{
            //    int numeroConvertito = Convert.ToInt32(numero);

            //    Console.WriteLine(numeroConvertito * 10);
            //} catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}

            //while (true)
            //{
            //    try
            //    {
            //        Console.WriteLine("Inserisci un numero");
            //        string numero = Console.ReadLine();

            //        int numeroConvertito = Convert.ToInt32(numero);
            //        Console.WriteLine(numeroConvertito * 10);
            //    } catch (Exception e)
            //    {
            //        Console.WriteLine(e.Message);
            //    }
            //}
            
        }
    }
}
