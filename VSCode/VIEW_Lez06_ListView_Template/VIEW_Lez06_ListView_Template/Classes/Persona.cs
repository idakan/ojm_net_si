﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VIEW_Lez06_ListView_Template.Classes
{
    public class Persona
    {
        public string Nome { get; set; }

        public string Cognome { get; set; }

        public string Email { get; set; }

        public override string ToString()
        {
            return $"{Nome} - {Cognome} - {Email}";
        }
    }
}
