﻿using ASPCORE_Lez1_HelloWorld.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez1_HelloWorld.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            Messaggio mess = new Messaggio()
            {
                Corpo = "Ciao sono Giovanni Pace e ti saluto!"
            };

            ViewBag.Sottotitolo = "Ciao sono un sottotitolo";
            return View(mess);
        }

        [HttpPost]
        public IActionResult Index(Messaggio objMess)
        {
            return View(objMess);
        }

        //[HttpPost]
        //public IActionResult Prova()
        //{
        //    return Ok(new { Status = "success" });
        //}


        public IActionResult Contattaci()
        {
            return View();
        }
    }
}
