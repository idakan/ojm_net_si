﻿using DB_Rubrica.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DB_Rubrica.DAL
{
    class RubricaDAL
    {
        private string connString;

        public RubricaDAL(IConfiguration config)
        {
            if(connString == null)
                connString = config.GetConnectionString("ServerLocale");
        }

        public List<Rubrica> GetListaRubrica()
        {
            List<Rubrica> elencoTemp = new List<Rubrica>();

            using (SqlConnection connection = new SqlConnection(connString))
            {
                string query = "SELECT nome, cognome, telefono FROM Contatto";
                SqlCommand command = new SqlCommand(query, connection);

                connection.Open();

                SqlDataReader reader = command.ExecuteReader();
                while(reader.Read())
                {
                    Rubrica temp = new Rubrica()
                    {
                        ContattoId = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };
                    elencoTemp.Add(temp);
                }
            }

            return elencoTemp;
        }
        public bool InsertContatto(Rubrica objRubrica)
        {
            using (SqlConnection connection = new SqlConnection(connString))
            {
                SqlCommand command = new SqlCommand();
                command.Connection = connection;

                command.CommandText = "INSERT INTO Contatto(nome,cognome,telefono) VALUES (@varNome, @varCognome, @varTelefono)";
                command.Parameters.AddWithValue("@varNome", objRubrica.Nome);
                command.Parameters.AddWithValue("@varCognome", objRubrica.Cognome);
                command.Parameters.AddWithValue("@varTelefono", objRubrica.Telefono);

                connection.Open();

                if (command.ExecuteNonQuery() > 0)
                    return true;
            }
                return false;
        }
        public bool UpdateContatto(Rubrica objRubrica)
        {
            using(SqlConnection connection = new SqlConnection(connString))
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "UPDATE Contatto SET contattoID = @varId, nome = @varNome, cognome = @varCognome, telefono = @varTelefono";
                command.Parameters.AddWithValue("@varId", objRubrica.ContattoId);
                command.Parameters.AddWithValue("@varNome", objRubrica.Nome);
                command.Parameters.AddWithValue("@varCognome", objRubrica.Cognome);
                command.Parameters.AddWithValue("@varTelefono", objRubrica.Telefono);
                command.Connection = connection;

                connection.Open();
                if (command.ExecuteNonQuery() > 0)
                    return true;
            }
            return false;
        }
        public bool DeleteContatto(int varId)
        {
            using(SqlConnection connection = new SqlConnection(connString))
            {
                SqlCommand command = new SqlCommand();
                command.CommandText = "DELETE FROM Contatto WHERE contattoId = @varId";
                command.Parameters.AddWithValue("@varId", varId);
                command.Connection = connection;

                connection.Open();
                if (command.ExecuteNonQuery() > 0)
                    return true;

            }

            return false;
        }
    }
}
