﻿using DB_Rubrica.DAL;
using DB_Rubrica.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DB_Rubrica.Controllers
{
    class RubricaController
    {
        private static IConfiguration configuration;

        public RubricaController()
        {
            if (configuration == null)
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

                configuration = builder.Build();
            }
        }

        public void StampaRubrica()
        {
            RubricaDAL rubricadal = new RubricaDAL(configuration);
            List<Rubrica> elencoContatti = rubricadal.GetListaRubrica();

            foreach(Rubrica item in elencoContatti)
            {
                Console.WriteLine(item);
            }
        }
        public void InserisciContatto(string varNome, string varCognome, string varTelefono)
        {
            Rubrica temp = new Rubrica()
            {
                Nome = varNome,
                Cognome = varCognome,
                Telefono = varTelefono
            };

            RubricaDAL rubricadal = new RubricaDAL(configuration);

            if (rubricadal.InsertContatto(temp))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }
        public void AggiornaContattoPerId(int varId, string varNome, string varCognome, string varTelefono)
        {
            Rubrica temp = new Rubrica()
            {
                ContattoId = varId,
                Nome = varNome,
                Cognome = varCognome,
                Telefono = varTelefono
            };

            RubricaDAL rubricadal = new RubricaDAL(configuration);

            if (rubricadal.UpdateContatto(temp))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }
        public void CancellaContattoPerId(int varId)
        {
            RubricaDAL rubricadal = new RubricaDAL(configuration);

            if(rubricadal.DeleteContatto(varId))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }
    }
}
