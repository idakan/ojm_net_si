﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_Rubrica.Models
{
    class Rubrica
    {
        public int ContattoId { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Telefono { get; set; }
        public override string ToString()
        {
            return $"{ContattoId} - {Nome} {Cognome} - {Telefono}";
        }
    }
}