﻿using System;

namespace Lez01_02_Variabili
{
    class Program
    {
        static void Main(string[] args)
        {
            //Tipi di dato
            ////int a = 5;              //Dichiarazione + Assegnazione

            //int a;                  //Dichiarazione
            //a = 5;                  //Assegnazione

            //float b = 6.2f;
            //double c = 6.2d;

            ////float d = 9.6d;           //Errore di dimensioni
            //double e = 9.6f;

            //Console.WriteLine(a);
            //Console.WriteLine(b);
            //Console.WriteLine(c);

            //Sovrascrittura
            //float numero = 8.95f;

            //numero = 2.4f;

            //Console.WriteLine(numero);

            //Operazioni
            //int valoreUno = 5;
            //int valoreDue = 8;

            //Console.WriteLine(valoreUno + valoreDue);
            //Console.WriteLine(valoreUno + valoreDue);
            //Console.WriteLine(valoreUno + valoreDue);
            //Console.WriteLine(valoreUno + valoreDue);

            //int somma = valoreUno + valoreDue;
            //Console.WriteLine(somma);
            //Console.WriteLine(somma);
            //Console.WriteLine(somma);
            //Console.WriteLine(somma);

            //Operazione complessa
            //Console.WriteLine(15 + 26.0f + 56.90d);

            //int valoreUno = 10;
            //double valoreDue = 3;

            //double risultato = valoreUno / valoreDue;

            //Console.WriteLine( (int)risultato );

            //-------------- AGGIORNAMENTO

            //int numero = 5;

            //numero = numero + 2;
            //numero = numero + 2;
            //numero = numero + 2;
            //numero = numero + 2;
            //numero = numero + 2;
            //numero = numero + 2;

            //Console.WriteLine(numero);


            int numero = 5;

            //numero += 2;

            numero++;

            Console.WriteLine(numero);

        }
    }
}
