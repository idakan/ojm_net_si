﻿using Lez04_04_Interfacce.Classes;
using System;

namespace Lez04_04_Interfacce
{
    class Program
    {
        static void Main(string[] args)
        {
            Gatto bu = new Gatto();
            bu.tipoMovimento();

            Pesce nemo = new Pesce();
            nemo.versoEmesso();

        }
    }
}
