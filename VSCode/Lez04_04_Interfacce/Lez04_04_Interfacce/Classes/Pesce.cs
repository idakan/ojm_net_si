﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_04_Interfacce.Classes
{
    class Pesce : IAnimale
    {
        public bool HaLeSquame { get; set; }

        public void tipoMovimento()
        {
            Console.WriteLine("Nuota");
        }

        public void versoEmesso()
        {
            Console.WriteLine("° ° °");
        }
    }
}
