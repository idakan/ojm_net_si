﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_04_Interfacce.Classes
{
    public interface IAnimale
    {
        void versoEmesso();
        void tipoMovimento();
    }
}
