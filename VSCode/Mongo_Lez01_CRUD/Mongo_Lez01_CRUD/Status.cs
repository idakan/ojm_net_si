﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Lez01_CRUD
{
    public class Status
    {
        public string Result { get; set; }
        public string Description { get; set; }
    }
}
