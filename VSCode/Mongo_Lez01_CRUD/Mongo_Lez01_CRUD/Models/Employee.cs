﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Lez01_CRUD.Models
{
    public class Employee
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }            //Object ID di mongo
        
        [BsonElement("matricola")]
        [Required]
        public int Matricola { get; set; }
        
        [BsonElement("nome")]
        [Required]
        [StringLength(50)]
        public string Nome { get; set; }        
        
        [BsonElement("cognome")]
        [Required]
        [StringLength(50)]
        public string Cognome { get; set; }

        [BsonElement("titolo")]
        [Required]
        [StringLength(50)]
        public string Titolo { get; set; }

        [BsonElement("dataNascita")]
        [Required]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime DataNascita { get; set; }
    }
}
