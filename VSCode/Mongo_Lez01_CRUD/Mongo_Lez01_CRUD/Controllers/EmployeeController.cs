﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Mongo_Lez01_CRUD.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_Lez01_CRUD.Controllers
{
    [ApiController]
    [Route("api/employees")]
    public class EmployeeController : Controller
    {
        private readonly IMongoCollection<Employee> employees;

        public EmployeeController(IConfiguration configuration)
        {
            string connString = configuration.GetValue<string>("MongoDbSettings:MongoLocale");
            string database   = configuration.GetValue<string>("MongoDbSettings:NomeDatabase");

            MongoClient client = new MongoClient(connString);
            IMongoDatabase mongoDatabase = client.GetDatabase(database);

            employees = mongoDatabase.GetCollection<Employee>("Employees");
        }

        [HttpPost("insert")]
        public ActionResult<Employee> InsertEmployee(Employee emp)
        {
            if(ModelState.IsValid)
            {
                Employee temp = employees.Find(o => o.Matricola == emp.Matricola).FirstOrDefault();

                if(temp == null)
                    employees.InsertOne(emp);
            }
            else
            {
            }

            return Ok(emp);
        }

        [HttpGet]
        public ActionResult<IEnumerable<Employee>> GetAll()
        {
            return Ok(employees.Find(FilterDefinition<Employee>.Empty));
        }

        [HttpGet("{docId}")]
        public ActionResult<Employee> GetById(string docId)
        {
            ObjectId tempId = new ObjectId(docId);
            var filter = Builders<Employee>.Filter.Eq(e => e.DocumentID, tempId);
            return Ok(employees.Find(filter).FirstOrDefault());
        }

        [HttpDelete("{docId}")]
        public ActionResult<bool> DeleteById(string docId)
        {
            ObjectId tempId = new ObjectId(docId);

            Employee temp = employees.Find(e => e.DocumentID == tempId).FirstOrDefault();
            if (temp != null)
            {
                var result = employees.DeleteOne<Employee>(e => e.DocumentID == tempId);
                if(result.IsAcknowledged && result.DeletedCount > 0)
                    return Ok(new { Status = "success" });
                else
                    return Ok(new { Status = "error" });
            }
            return Ok(new { Status = "error", Description = "No doc found" });
        }

        [HttpPut("{docId}")]
        public ActionResult<bool> UpdateById(string docId, Employee emp)
        {
            ObjectId tempId = new ObjectId(docId);

            Employee temp = employees.Find(e => e.DocumentID == tempId).FirstOrDefault();
            if (temp != null)
            {
                var filter = Builders<Employee>.Filter.Eq(e => e.DocumentID, tempId);
                var result = employees.ReplaceOne(filter, emp);

                if (result.IsAcknowledged && result.ModifiedCount > 0)
                    return Ok(new { Status = "success" });
            }
            return Ok(new { Status = "error", Description = "No doc found" });
        }
    }
}
