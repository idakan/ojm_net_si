﻿using Lez05_Parziali.Classes;
using System;

namespace Lez05_Parziali
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona giovanni = new Persona();
            giovanni.Nome = "Giovanni";
            giovanni.Cognome = "Pace";
            giovanni.CodFis = "PCAGNN";
            giovanni.Eta = 35;
            giovanni.Sesso = "M";

            Console.WriteLine(giovanni);
        }
    }
}
