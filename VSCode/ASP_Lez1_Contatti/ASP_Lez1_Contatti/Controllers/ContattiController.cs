﻿using ASP_Lez1_Contatti.Data;
using ASP_Lez1_Contatti.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Controllers
{
    [Route("api/contatti")]
    [ApiController]
    public class ContattiController : Controller
    {
        [HttpPost, HttpGet]
        public ActionResult<IEnumerable<Contatto>> GetAllContatti()
        {
            MockContatti mock = new MockContatti();

            var elenco = mock.GetContatti();
            return Ok(elenco);
        }

        [HttpPost("{id}")]
        public ActionResult<Contatto> GetContatto(int id)
        {
            MockContatti mock = new MockContatti();

            var contatto = mock.GetContattoById(id);
            return Ok(contatto);
        }
    }
}
