﻿using ASP_Lez1_Contatti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Data
{
    interface IContattiRepo
    {
        IEnumerable<Contatto> GetContatti();
        Contatto GetContattoById(int varId);
    }
}
