﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_ElencoCitta.Models
{
    public class Citta
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Provincia { get; set; }
        public override string ToString()
        {
            return $"{Id} {Nome} {Provincia}";
        }
    }
}
