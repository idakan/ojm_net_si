﻿using DB_ElencoCitta.DAL;
using DB_ElencoCitta.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace DB_ElencoCitta.Controllers
{
    public class CittaController
    {
        public static IConfiguration configuration;

        public CittaController()
        {
            //Leggo la configurazione ed la inserisco nella variabile "configurazione"
            //ConfigurationBuilder builder = new ConfigurationBuilder();
            //builder.SetBasePath(Directory.GetCurrentDirectory());
            //builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
            if (configuration == null)
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

                configuration = builder.Build();
            }
        }

        public void StampaCitta()
        {
            CittaDAL cittadal = new CittaDAL(configuration);
            List<Citta> elenco = cittadal.GetListaCitta();

            foreach(Citta item in elenco)
            {
                Console.WriteLine(item);
            }
        }
        public void InserisciCitta(string varNome, string varProvincia)
        {
            Citta temp = new Citta()
            {
                Nome = varNome,
                Provincia = varProvincia
            };

            CittaDAL cittadal = new CittaDAL(configuration);

            if (cittadal.InsertCitta(temp))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }
        
        public void EliminaCittaPerId(int varId)
        {
            CittaDAL cittadal = new CittaDAL(configuration);
            if (cittadal.DeleteCitta(varId))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }

        public void ModificaCittaPerId(int varId, string varNome, string varProvincia)
        {
            Citta temp = new Citta()
            {
                Id = varId,
                Nome = varNome,
                Provincia = varProvincia
            };

            CittaDAL cittadal = new CittaDAL(configuration);
            if (cittadal.UpdateCitta(temp))
                Console.WriteLine("Tutto ok!");
            else
                Console.WriteLine("Errore");
        }
    }
}
