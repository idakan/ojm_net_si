﻿using DB_ElencoCitta.Controllers;
using System;

namespace DB_ElencoCitta
{
    class Program
    {
        static void Main(string[] args)
        {
            CittaController gestore = new CittaController();
            gestore.StampaCitta();

            Console.WriteLine("-----------------------------");

            //gestore.inserisciCitta("Catanzaro", "CZ");

            //gestore.stampaCitta();

            //gestore.eliminaCittaPerId(5);
            //gestore.stampaCitta();

            gestore.ModificaCittaPerId(4, "Frosinone", "FR");
            gestore.StampaCitta();

            Console.ReadLine();
        }
    }
}
/*Ordine di configurazione:
 * Models
 * JSON
 * Controller
 * DAL
 */