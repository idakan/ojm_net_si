﻿using DB_ElencoCitta.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace DB_ElencoCitta.DAL
{
    class CittaDAL
    {
        private string connString;

        public CittaDAL(IConfiguration config)
        {
            if (connString == null)
                connString = config.GetConnectionString("ServerLocale");
        }

        public List<Citta> GetListaCitta()
        {
            List<Citta> elencoTemp = new List<Citta>();

            using (SqlConnection connection = new SqlConnection(connString))
            {
                string query = "SELECT cittaID, nome, prov FROM Citta";
                SqlCommand cmd = new SqlCommand(query, connection);

                connection.Open();

                SqlDataReader reader = cmd.ExecuteReader();
                while(reader.Read())
                {
                    Citta temp = new Citta()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Provincia = reader[2].ToString()
                    };
                    elencoTemp.Add(temp);
                }
            }

            return elencoTemp;
        }
        //LEGGI LE CITTA DAL DB

        public bool InsertCitta(Citta objCitta)
        {
            bool result = false;

            using (SqlConnection connection = new SqlConnection(connString))
            {
                //SqlCommand comm = new SqlCommand(query, connessione);  

                SqlCommand comm = new SqlCommand();
                comm.Connection = connection;
                
                comm.CommandText = "INSERT INTO Citta (nome, prov) VALUES (@nomeCitta, @provCitta)";
                comm.Parameters.AddWithValue("@nomeCitta", objCitta.Nome);
                comm.Parameters.AddWithValue("@provCitta", objCitta.Provincia);

                connection.Open();
                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    result = true;
            }

            return result;
        }
        
        public bool DeleteCitta(int varId)
        {
            bool risultato = false;

            using (SqlConnection connection = new SqlConnection(connString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connection;

                comm.CommandText = "DELETE FROM Citta WHERE cittaID = @idCitta";
                comm.Parameters.AddWithValue("@idCitta", varId);

                connection.Open();

                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    risultato = true;
            }
                return risultato;
        }
        public bool UpdateCitta(Citta objCitta)
        {
            bool risultato = false;

            using (SqlConnection connection = new SqlConnection(connString))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connection;

                comm.CommandText = "UPDATE Citta SET nome = @varNome, prov = @varProv WHERE cittaID = @varId";
                comm.Parameters.AddWithValue("@varNome", objCitta.Nome);
                comm.Parameters.AddWithValue("@varProv", objCitta.Provincia);
                comm.Parameters.AddWithValue("@varId", objCitta.Id);

                connection.Open();

                if (comm.ExecuteNonQuery() > 0)
                    risultato = true;
            }
            return risultato;
        }
    }
}
