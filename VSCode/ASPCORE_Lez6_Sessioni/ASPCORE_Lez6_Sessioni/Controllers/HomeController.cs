﻿using ASPCORE_Lez6_Sessioni.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez6_Sessioni.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult EffettuaLogin()
        {
            return View();
        }

        [HttpPost]
        public RedirectResult LoginCheck(Credenziali objCred)
        {
            if (objCred.Username.Equals("gioTastic") && objCred.Password.Equals("pace"))
            {
                HttpContext.Session.SetString("isLogged", "gioTastic");
                HttpContext.Session.SetString("Role", "ADMIN");

                return Redirect("/Home/Index");
            }
            else
            {
                return Redirect("/Home/EffettuaLogin");
            }

        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("isLogged") == null && 
                !HttpContext.Session.GetString("ROLE").Equals("ADMIN"))
                return Redirect("/Home/EffettuaLogin");

            return View();
        }
    }
}
