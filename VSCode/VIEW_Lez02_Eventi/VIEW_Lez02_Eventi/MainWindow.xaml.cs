﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace VIEW_Lez02_Eventi
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            pannelloPrincipale.MouseUp += new MouseButtonEventHandler(Click_Su_Stackpanel);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button pulsante = sender as Button;

            MessageBox.Show("Mi hai cliccato!");
            pulsante.Content = "HAI CLICCATO! ;)";
        }

        private void Click_Su_Stackpanel(object sender, MouseButtonEventArgs e)
        {
            StackPanel pannello = sender as StackPanel;

            MessageBox.Show("Mi hai cliccato lo Stackpanel! " + e.GetPosition(pannello).ToString());
        }
    }
}
