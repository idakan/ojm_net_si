﻿using Lez05_07_Poly.Classes;
using System;
using System.Collections.Generic;

namespace Lez05_07_Poly
{
    class Program
    {
        static void Main(string[] args)
        {
            Studente studUno = new Studente() { Nome = "Giovanni", Cognome = "Pace",  Matricola = "789456" };
            Studente studDue = new Studente() { Nome = "Mario", Cognome = "Rossi",  Matricola = "889456" };

            Docente docUno = new Docente() { Nome = "Valeria", Cognome = "Verdi", Dipartimento = "Fisica" };
            Docente docDue = new Docente() { Nome = "Marika", Cognome = "Mariko", Dipartimento = "Matmatica" };

            List<Persona> elenco = new List<Persona>();
            elenco.Add(studUno);
            elenco.Add(studDue);
            elenco.Add(docUno);
            elenco.Add(docDue);

            foreach(Persona item in elenco)
            {
                Console.WriteLine(item.GetType().Name);
                //Console.WriteLine(item.GetType().Name);
            }
        }
    }
}
