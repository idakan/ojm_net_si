﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_07_Poly.Classes
{
    public abstract class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
    }
}
