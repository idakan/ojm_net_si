﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_07_Poly.Classes
{
    public class Docente : Persona
    {
        public string Dipartimento { get; set; }
        public override string ToString()
        {
            return $"{Nome} {Cognome} - Dipartimento: {Dipartimento}";
        }
    }
}
