﻿using System;

namespace Lez01_01_ConsoleHelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            //Operazione di output di Hello World
            Console.WriteLine("Hello World!");          //Commento dopo istruzione
                                                        //Console.WriteLine("CIAO");

            /*
             * Sono un commento multilinea
             * ciao GIovanni
             */

            //Un commento rapido si abilita con Ctrl + K + C
            //Un commento rapido si disabilita con Ctrl + K + U
            //Console.WriteLine("CIAO");
            //Console.WriteLine("CIAO");
            //Console.ReadLine();
            //Console.WriteLine("CIAO");
            //Console.WriteLine("CIAO");

            Console.Write("CIAO ");
            Console.Write("Giovanni ");
            Console.Write("Pace");

            //Evito la chiusura automatica del programma
            Console.ReadLine();
        }
    }
}
