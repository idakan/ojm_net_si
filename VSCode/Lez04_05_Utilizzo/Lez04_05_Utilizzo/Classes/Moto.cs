﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_05_Utilizzo.Classes
{
    public class Moto : Veicolo
    {
        public int NumRuote { get; set; }

        public Moto(string varTelaio, int varRuote)
        {
            NumeroTelaio = varTelaio;
            NumRuote = varRuote;
        }
        public override string ToString()
        {
            return $"MOTO: {NumeroTelaio} - Ruote: {NumRuote} - {velocitaMax}";
        }
    }
}
