﻿using Lez04_05_Utilizzo.Classes;
using System;

namespace Lez04_05_Utilizzo
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * Creare un sistema di gestione veicoli:
             * 
             * All'inserimento di un nuovo veicolo mi sia permessa la scelta tra:
             * 1. Automobile
             * 2. Moto
             * 
             * Alla fine dell'inserimento delle caratteristiche del veicolo, stampare i suoi dettagli.
             * 
             * Libera scelta degli attributi
             */

            //Automobile ford = new Automobile("123456", 4);
            //ford.accelera(20.0f);
            //ford.setVelocitaMassima(150);
            //Console.WriteLine(ford);

            //Moto ducati = new Moto("987654", 2);
            //ducati.accelera(50.0d);
            //Console.WriteLine(ducati);

            bool inserimentoAbilitato = true;

            do
            {
                Console.WriteLine("CHe tipo di veicolo vuoi inserire (AUTO/MOTO)?\nDigita Q per uscire");
                string tipo = Console.ReadLine();

                string telaio;

                switch (tipo)
                {
                    case "AUTO":
                        Console.Write("Inserisci il numero di telaio: ");
                        telaio = Console.ReadLine();

                        Console.Write("Inserisci il numero di porte: ");
                        string porte = Console.ReadLine();

                        Automobile autoTemp = new Automobile(telaio, Convert.ToInt32(porte));

                        Console.WriteLine($"DETTAGLIO: {autoTemp}");

                        break;
                    case "MOTO":
                        Console.Write("Inserisci il numero di telaio: ");
                        telaio = Console.ReadLine();

                        Console.Write("Inserisci il numero di ruote: ");
                        string ruote = Console.ReadLine();

                        Moto motoTemp = new Moto(telaio, Convert.ToInt32(ruote));

                        Console.WriteLine($"DETTAGLIO: {motoTemp}");
                        break;
                    case "Q":
                        inserimentoAbilitato = !inserimentoAbilitato;
                        break;
                    default:
                        Console.WriteLine("Comando non riconosciuto");
                        break;
                }
            } while (inserimentoAbilitato);
        }
    }
}
