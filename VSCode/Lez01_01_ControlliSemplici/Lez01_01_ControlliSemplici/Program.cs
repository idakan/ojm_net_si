﻿using System;

namespace Lez01_01_ControlliSemplici
{
    class Program
    {
        static void Main(string[] args)
        {

            /**
             * if(condizione){
             *      corpo del codice nel caso la condizione sia verificata e positiva
             * } else {
             *      corpo del codice da avviare altrimenti
             * }
             */

            //int eta = 18;

            //if (eta >= 18)
            //{
            //    Console.WriteLine("Sei maggiorenne");
            //}
            //else
            //{
            //    Console.WriteLine("Sei minorenne");
            //}

            //Console.WriteLine(16 == 16);

            /* Possibili operatori:
             * >
             * <
             * >=
             * <=
             * ==
             * !=
             */

            //if (false) {
            //    Console.WriteLine("Sono nel ramo IF");
            //}
            //else
            //{
            //    Console.WriteLine("Sono nel ramo ELSE");
            //}


            //Validazione NAIV
            //int eta = 19;

            //if(eta < 0)
            //{
            //    Console.WriteLine("Troppo piccolo, sei un embrione... nemmeno");
            //}
            //else
            //{
            //    if (eta >= 18)
            //    {
            //        Console.WriteLine("Sei maggiorenne");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Sei minorenne");
            //    }
            //}


            //if(eta < 120)
            //{
            //    if (eta > 0)
            //    {
            //        if (eta >= 18)
            //        {
            //            Console.WriteLine("Sei maggiorenne");
            //        }
            //        else
            //        {
            //            Console.WriteLine("Sei minorenne");
            //        }
            //    }
            //    else
            //    {
            //        Console.WriteLine("Troppo piccolo, sei un embrione... nemmeno");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("Ehhhhhhhhhhhhhhh... troppo elevata");
            //}

            //------------------ OPERATORI LOGICI
            //int eta = 19;
            //AND
            //  true   &&   true        = true
            //   1     *     1          = 1

            //int eta = 190;
            //AND
            //  true   &&   false        = false
            //   1     *     0          = 0

            //int eta = -20;
            ////AND
            ////  false   &&   true        = false
            ////   0     *     1          = 0
            //if (eta > 0 && eta < 120)
            //{
            //    Console.WriteLine("OK");
            //}
            //else
            //{
            //    Console.WriteLine("ERRORE");
            //}

            //int eta = 190;
            //OR
            //   false  ||   true           = true
            //     0    +     1             = 1

            //int eta = -20;
            ////OR
            ////   true  ||   false           = true
            ////     1    +     0             = 1

            //int eta = 19;
            ////OR
            ////   false  ||   false           = false
            ////     0    +     0             = 0
            //if (eta <= 0 || eta >= 120)
            //{
            //    Console.WriteLine("ERRORE");
            //}
            //else
            //{
            //    Console.WriteLine("OK");
            //}

            //int eta = -20;
            //if (eta > 0 && eta < 120)
            //{
            //    if (eta >= 18)
            //    {
            //        Console.WriteLine("Sei maggiorenne");
            //    }
            //    else
            //    {
            //        Console.WriteLine("Sei minorenne");
            //    }
            //}
            //else
            //{
            //    Console.WriteLine("ERRORE");
            //}

            //------------- NEGAZIONE

            //bool stato = true;

            //stato = !stato;

            //if (stato)
            //{
            //    Console.WriteLine("Sono nel ramo IF");
            //}
            //else
            //{
            //    Console.WriteLine("Sono nel ramo ELSE");
            //}

            //Console.WriteLine(!true);

            

        }
    }
}
