﻿using System;
using System.Collections.Generic;

#nullable disable

namespace EF_Lez01_PrimoCollaudo.Models
{
    public partial class CartaFedeltum
    {
        public int CartaId { get; set; }
        public string CodiceCarta { get; set; }
        public string Negozio { get; set; }
        public int PersonaRif { get; set; }

        public virtual Persona PersonaRifNavigation { get; set; }
    }
}
