﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace EF_Lez01_PrimoCollaudo.Models
{
    public partial class Lez01_02_CartaFedeltaContext : DbContext
    {
        public Lez01_02_CartaFedeltaContext()
        {
        }

        public Lez01_02_CartaFedeltaContext(DbContextOptions<Lez01_02_CartaFedeltaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CartaFedeltum> CartaFedelta { get; set; }
        public virtual DbSet<Persona> Personas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-J6HN12A\\SQLEXPRESS;Database=Lez01_02_CartaFedelta;User Id=sharpuser;Password=cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<CartaFedeltum>(entity =>
            {
                entity.HasKey(e => e.CartaId)
                    .HasName("PK__CartaFed__30780D4D90EC06C9");

                entity.HasIndex(e => e.CodiceCarta, "UQ__CartaFed__7F04167B1F418595")
                    .IsUnique();

                entity.Property(e => e.CartaId).HasColumnName("cartaID");

                entity.Property(e => e.CodiceCarta)
                    .IsRequired()
                    .HasMaxLength(25)
                    .IsUnicode(false)
                    .HasColumnName("codiceCarta");

                entity.Property(e => e.Negozio)
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("negozio");

                entity.Property(e => e.PersonaRif).HasColumnName("personaRIF");

                entity.HasOne(d => d.PersonaRifNavigation)
                    .WithMany(p => p.CartaFedelta)
                    .HasForeignKey(d => d.PersonaRif)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__CartaFede__perso__29572725");
            });

            modelBuilder.Entity<Persona>(entity =>
            {
                entity.ToTable("Persona");

                entity.HasIndex(e => e.Email, "UQ__Persona__AB6E6164D4FEF871")
                    .IsUnique();

                entity.Property(e => e.PersonaId).HasColumnName("personaID");

                entity.Property(e => e.Cognome)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("cognome");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.Indirizzo)
                    .HasMaxLength(750)
                    .IsUnicode(false)
                    .HasColumnName("indirizzo")
                    .HasDefaultValueSql("('N.D.')");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("nome");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
