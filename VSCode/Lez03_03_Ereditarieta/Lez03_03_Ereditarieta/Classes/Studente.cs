﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_03_Ereditarieta.Classes
{
    class Studente : Persona
    {
        public Studente(string nome, string cognome)
        {
            Nome = nome;
            Cognome = cognome;
            Matricola = incrementoMatr();
        }
    }
}
