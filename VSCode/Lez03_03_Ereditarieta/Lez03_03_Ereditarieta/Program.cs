﻿using Lez03_03_Ereditarieta.Classes;
using System;

namespace Lez03_03_Ereditarieta
{
    class Program
    {
        static void Main(string[] args)
        {
            Persona giovanni = new Persona("Giovanni", "Pace");
            giovanni.stampaDettaglio();

            Docente mario = new Docente("Mario", "Rossi", "MRRRSS", "Fisica");
            mario.stampaDettaglio();
        }
    }
}
