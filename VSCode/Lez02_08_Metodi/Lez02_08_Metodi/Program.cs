﻿using System;

namespace Lez02_08_Metodi
{
    class Program
    {
        static void salutaPersona(string nome, string cognome, int eta)
        {
            Console.WriteLine($"Ciao {nome} {cognome} - Anni: {eta}");
        }

        static string componiSaluto(string nome, string cognome, int eta)
        {
            nome = (nome.Length == 0) ? "N.D." : nome;
            cognome = (cognome.Length == 0) ? "N.D." : cognome;

            return $"Salve {nome} {cognome} - con anni: {eta}";
        }

        static void metodoUno()
        {
            Console.WriteLine("Sono il primo Metodo");

            metodoDue();
        }
        static void metodoDue()
        {
            Console.WriteLine("Sono il secondo Metodo");
        }

        static void Main(string[] args)
        {
            //salutaPersona("Giovanni", "Pace", 35);

            //string risultato = componiSaluto("Mario", "Rossi", 59);
            //Console.WriteLine(risultato);

            //Console.WriteLine(componiSaluto("Valeria", "Verdi", 24));

            //Console.WriteLine(componiSaluto("Marco", "", 24));

            metodoUno();
        }
    }
}
