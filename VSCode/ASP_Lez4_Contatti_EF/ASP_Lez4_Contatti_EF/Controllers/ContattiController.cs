﻿using ASP_Lez4_Contatti_EF.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez4_Contatti_EF.Controllers
{
    [Route("api/contatti")]
    [ApiController]
    public class ContattiController : Controller
    {
        [HttpGet("lista")]                                  //https:://.../api/contatti/lista
        public ActionResult<IEnumerable<Contatti>> GetAllContatti()
        {
            var elenco = new List<Contatti>();

            try
            {
                using (var context = new db_rubricaContext())
                {
                    elenco = context.Contattis.ToList();
                }
            } catch (Exception ex)
            {
                return Ok("PROBLEMA");
            } finally
            {
                Console.Write("CIAO");
            }

            return Ok(elenco);
        }

        [HttpGet("{varId}")]
        public ActionResult<Contatti> GetContatto(int varId)
        {
            Contatti temp = null;

            try
            {
                using (var context = new db_rubricaContext())
                {
                    //temp = context.Contattis.Where(o => o.RubricaId == varId).FirstOrDefault();
                    temp = context.Contattis
                        .Where(o => o.RubricaId == varId)
                        .Select(k => new Contatti() { Cognome = k.Cognome, Nome = k.Nome, Telefono = k.Telefono})
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }

            return Ok(temp);
        }

        //TODO: Inserimento
        //TODO: Modifica
        //TODO: Eliminazione
    }
}
