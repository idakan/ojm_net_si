using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ASPCORE_Lez2_HelloWorld_RazorPages.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ASPCORE_Lez2_HelloWorld_RazorPages.Pages
{
    public class IndexModel : PageModel
    {
        public string Sottotitolo { get; set; }

        public Messaggio messaggio;

        public void OnGet()
        {
            Sottotitolo = "Ciao sono un sottotitolo";

            messaggio = new Messaggio()
            {
                Corpo = "Ciao sono Giovanni Pace e ti saluto!"
            };


        }
    }
}
