﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_Palestre.Models
{
    class SingleEntry
    {
        public int singleEntryId { get; set; }
        public string nome { get; set; }
        public string codFis { get; set; }
        public string telefono { get; set; }
        public string ultimaPalestra { get; set; }
        public string ultimoIngresso { get; set; }
        public string indirizzo { get; set; }
        public List<Corso> Corsi { get; set; }
    }
}
