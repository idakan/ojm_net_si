﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DB_Palestre.Models
{
    class Corso
    {
        public int corsoID { get; set; }
        public string nomeCorso { get; set; }
        public override string ToString()
        {
            return $"{corsoID} - {nomeCorso}";
        }
    }
}
