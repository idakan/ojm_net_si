﻿using EF_Lez01_OneToMany.Models;
using System;
using System.Linq;

namespace EF_Lez01_OneToMany
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             *  Scaffold-DbContext "Server=DESKTOP-6IND805\SQLEXPRESS;Database=Lez1_02_CarteFedelta;User Id=sharpuser;Password=cicciopasticcio;Trusted_Connection=True;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models
             */

            using (var context = new Lez1_02_CarteFedeltaContext())
            {
                //Inserimento semplice

                //var perUno = new Persona()
                //{
                //    Nome = "Provetto",
                //    Cognome = "MarProvettizilli",
                //    Indirizzo = "Via le mani dal naso",
                //    Email = "provetto@provetti.com"
                //};

                //var cartUno = new CartaFedeltum()
                //{
                //    CodiceCarta = "CC78965",
                //    Negozio = "Tigre",
                //    PersonaRifNavigation = perUno
                //};

                //var cartDue = new CartaFedeltum()
                //{
                //    CodiceCarta = "CC78966",
                //    Negozio = "Ultimo",
                //    PersonaRifNavigation = perUno
                //};

                //try
                //{
                //    context.Personas.Add(perUno);
                //    context.CartaFedelta.Add(cartUno);
                //    context.CartaFedelta.Add(cartDue);
                //    context.SaveChanges();

                //    Console.WriteLine("Tutto ok");
                //} catch (Exception ex)
                //{
                //    Console.WriteLine(ex.Message);
                //}

                //Selezione

                var elencoPersone = context.Personas.ToList();

                Persona perTemp = context.Personas.Single(item => item.PersonaId == 9);
                
            }
        }
    }
}
