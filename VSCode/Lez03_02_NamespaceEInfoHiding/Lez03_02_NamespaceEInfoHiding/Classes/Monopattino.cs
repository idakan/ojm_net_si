﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_02_NamespaceEInfoHiding.Classes
{
    class Monopattino
    {
        //public string Marca { get; set; }

        //private string marca;

        //public string Marca
        //{
        //    get { return marca; }
        //    set
        //    {
        //        if (value.Length == 0)
        //        {
        //            marca = "N.D.";
        //        }
        //        else
        //        {
        //            marca = value;
        //        }
        //    }
        //}

        private string marca;

        private string telaio;

        public string Telaio
        {
            get { return telaio; }
            private set { 
                if(value.Length == 0)
                {
                    //Genera numero di telaio
                    //telaio = Numero casuale
                }
                else
                {
                    telaio = $"2022-{value}";
                }
            }
        }

        public Monopattino()
        {
            this.Telaio = "";
        }

        public Monopattino(string varTelaio)
        {
            this.Telaio = varTelaio;
        }


    }
}
