﻿using System;

namespace Lez01_03_Stringhe
{
    class Program
    {
        static void Main(string[] args)
        {
            //string nominativo = "Giovanni Pace";
            //Console.WriteLine(nominativo);

            //Console.WriteLine("Giovanni" + "Pace");

            //string nome = "Giovanni";
            //string cognome = "Pace";
            //Console.WriteLine(nome + " " + cognome);

            //Numeri e stringhe

            //int a = 5;
            //int b = 6;
            //string frase = "La somma dei due numeri è: ";

            ////Console.WriteLine(frase + a + b);
            ////Console.WriteLine(a + b + frase);
            //Console.WriteLine(frase + (a + b));

            //Operazioni semplici tra stringhe:
            /*
             * Scrivere un programma che mandi in  output le seguenti due stringhe:
             * 
             * Giovanni Pace è 35 anni vecchio ed ha una temperatura corporea di 36.5°.
             * Mario Rossi è 25 anni vecchio ed ha una temperatura corporea di 37.5°.
             * 
             * Tip: Variabili Nome - Cognome - Eta - Temperatura
             */

            //Soluzione NAIV

            //string nome;
            //string cognome;
            //int eta;
            //float temperatura;

            //nome = "Giovanni";
            //cognome = "Pace";
            //eta = 35;
            //temperatura = 36.5f;

            //Console.WriteLine(nome + " " + cognome + " è " + eta + " anni vecchio ed ha " 
            //    + temperatura + "°.");

            //nome = "Mario";
            //cognome = "Rossi";
            //eta = 25;
            //temperatura = 37.5f;

            //Console.WriteLine(nome + " " + cognome + " è " + eta + " anni vecchio ed ha "
            //    + temperatura + "°.");

            //Soluzioni con interpolazione
            //string nome;
            //string cognome;
            //int eta;
            //float temperatura;

            //nome = "Giovanni";
            //cognome = "Pace";
            //eta = 35;
            //temperatura = 36.5f;

            //string risultato;
            //risultato = $"{nome} {cognome} è {eta} anni vecchio ed ha una temperatura corporea di {temperatura}°.";
            //Console.WriteLine(risultato);

            //nome = "Mario";
            //cognome = "Rossi";
            //eta = 25;
            //temperatura = 37.5f;

            //Console.WriteLine($"{nome} {cognome} è {eta} anni vecchio ed ha una temperatura corporea di {temperatura}°.");

            //string nome = "Giovanni";
            //string cognome = "Pace";

            //string risultato = string.Concat(nome, cognome);
            //Console.WriteLine(risultato);

            //--------------------- Proprietà della stringa
            //string nominativo = "Giovanni Pace";
            //Console.WriteLine($"La lunghezza della stringa è: {nominativo.Length}");

            //--------------------- Stringhe particolari
            //string frase = "Ciao, sei maschio\\femmina\\altro?";
            //Console.WriteLine(frase);

            //string frase = "Mi piace programmare in \"linguaggio semplice\"";
            //Console.WriteLine(frase);

            //--------------------- Index e Sub
            string frase = "Ciao sono Giovanni e mi piace tantissimo programmare";
            //Console.WriteLine(frase.IndexOf("Giovanni"));     //Verifica esistenza

            Console.WriteLine(frase[10]);                       //Accesso al CARATTERE in posizione 10

            Console.WriteLine(frase.Substring(3));
        }
    }
}
