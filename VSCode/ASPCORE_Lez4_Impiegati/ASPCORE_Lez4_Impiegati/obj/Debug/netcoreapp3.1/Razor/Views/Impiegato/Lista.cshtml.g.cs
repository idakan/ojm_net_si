#pragma checksum "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "b2ae7da2a401584c3afc9209e951737845ed1ef4"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Impiegato_Lista), @"mvc.1.0.view", @"/Views/Impiegato/Lista.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b2ae7da2a401584c3afc9209e951737845ed1ef4", @"/Views/Impiegato/Lista.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"b853c2693020103458b52d8246f923ca3fd89014", @"/Views/_ViewImports.cshtml")]
    public class Views_Impiegato_Lista : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<List<ASPCORE_Lez4_Impiegati.Models.Impiegato>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
   
    Layout = "_Layout";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<h1>Lista impiegati</h1>

<table class=""table table-striped"">
    <thead>
        <tr>
            <th>Nome</th>
            <th>Cognome</th>
            <th>Codice</th>
            <th>Città</th>
            <th>Titolo</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
");
#nullable restore
#line 21 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
         foreach (var item in Model)
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <tr>\r\n                <td>");
#nullable restore
#line 24 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
               Write(item.Nome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 25 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
               Write(item.Cognome);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>\r\n                    <a");
            BeginWriteAttribute("href", " href=\"", 579, "\"", 619, 2);
            WriteAttributeValue("", 586, "/Impiegato/Dettaglio/", 586, 21, true);
#nullable restore
#line 27 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
WriteAttributeValue("", 607, item.Codice, 607, 12, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 27 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
                                                           Write(item.Codice);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                </td>\r\n                <td>");
#nullable restore
#line 29 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
               Write(item.Citta);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>");
#nullable restore
#line 30 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
               Write(item.Titolo);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                <td>\r\n                    <button type=\"button\" class=\"btn btn-danger\"");
            BeginWriteAttribute("onclick", " onclick=\"", 825, "\"", 861, 3);
            WriteAttributeValue("", 835, "eliminaImp(\'", 835, 12, true);
#nullable restore
#line 32 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
WriteAttributeValue("", 847, item.Codice, 847, 12, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 859, "\')", 859, 2, true);
            EndWriteAttribute();
            WriteLiteral(">Elim.</button>\r\n                </td>\r\n            </tr>\r\n");
#nullable restore
#line 35 "C:\Users\UTENTE\source\repos\ASPCORE_Lez4_Impiegati\ASPCORE_Lez4_Impiegati\Views\Impiegato\Lista.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n    </tbody>\r\n</table>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<List<ASPCORE_Lez4_Impiegati.Models.Impiegato>> Html { get; private set; }
    }
}
#pragma warning restore 1591
