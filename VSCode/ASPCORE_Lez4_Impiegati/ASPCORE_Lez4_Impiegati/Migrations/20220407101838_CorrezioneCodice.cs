﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASPCORE_Lez4_Impiegati.Migrations
{
    public partial class CorrezioneCodice : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Codice",
                table: "Impiegati",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Codice",
                table: "Impiegati");
        }
    }
}
