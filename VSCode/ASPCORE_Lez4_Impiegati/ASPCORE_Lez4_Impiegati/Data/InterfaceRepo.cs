﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_Lez4_Impiegati.Data
{
    public interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int varId);
        T Insert(T t);
        bool Update(T t);
        bool Delete(int varId);

        T GetFromCodice(string codice);
    }
}
