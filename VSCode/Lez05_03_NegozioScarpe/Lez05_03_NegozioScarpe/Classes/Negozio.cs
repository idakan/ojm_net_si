﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_03_NegozioScarpe.Classes
{
    public class Negozio
    {
        public string Nome { get; set; }
        public string Indirizzo { get; set; }

        private List<Scarpa> magazzino = new List<Scarpa>();    //Devo inizializzare la List prima di poterla usare!

        public List<Scarpa> Magazzino
        {
            get { return magazzino; }
            set { magazzino = value; }
        }

        public void stampaMagazzino()
        {
            Console.WriteLine($"-----{Nome}-----");
            foreach(Scarpa item in Magazzino)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine($"---------------\n");
        }

    }
}
