﻿using Lez05_03_NegozioScarpe.Classes;
using System;

namespace Lez05_03_NegozioScarpe
{
    class Program
    {
        static void Main(string[] args)
        {
            Negozio fl = new Negozio()
            {
                Nome = "StoreFL",
                Indirizzo = "Via le mani dal naso"
            };

            Scarpa nike = new Scarpa()
            {
                Codice = "NK1234",
                Marca = "Nike",
                Materiale = "Plastica",
                Misura = 45.0f
            };
            Scarpa superga = new Scarpa()
            {
                Codice = "SP1234",
                Marca = "Superga",
                Materiale = "Tela",
                Misura = 42.0f
            };
            Scarpa converse = new Scarpa()
            {
                Codice = "CON987",
                Marca = "Converse",
                Materiale = "Tela",
                Misura = 41.0f
            };

            fl.Magazzino.Add(nike);
            fl.Magazzino.Add(superga);
            fl.Magazzino.Add(converse);

            fl.stampaMagazzino();

            //--------------------------------------------

            Negozio sd = new Negozio()
            {
                Nome = "Scarpe Diem",
                Indirizzo = "Via piedi profumati, 5"
            };

            sd.Magazzino.Add(superga);              //ATTENZIONE, sto riutilizzando l'indirizzo ad uno stesso oggetto
            sd.stampaMagazzino();

            superga.Codice = "SUPGIO78945";

            Console.WriteLine("---------------->> DOPO AGGIORNAMENTO");

            fl.stampaMagazzino();
            sd.stampaMagazzino();

            /*
             * Creare un sistema che mantenga traccia degli oggetti contenuti nelle stanze 
             * della vostra casa.
             * Di ogni oggetto voglio conoscere:
             * - Nome
             * - Descrizione
             * - Valore dell'oggetto
             * 
             * Di ogni stanza voglio conoscere il nome
             */


        }
    }
}
