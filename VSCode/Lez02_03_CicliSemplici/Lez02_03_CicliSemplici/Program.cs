﻿using System;

namespace Lez02_03_CicliSemplici
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * 
             * while(condizione){
             *      corpo del while
             * }
             *
             */

            //Loop infinito
            //while (1 == 1)
            //{
            //    Console.WriteLine("CIAO");
            //}

            //Console.WriteLine("HO finito!");

            //Controllo a priori - While
            //int indiceMassimo = 0;
            //int indiceAttuale = 0;

            //while (indiceAttuale < indiceMassimo)
            //{
            //    Console.WriteLine($"L'indice attuale è: {indiceAttuale + 1}");

            //    indiceAttuale++;        // indiceAttuale = indiceAttuale + 1
            //}

            //Console.WriteLine("HO finito!");

            //-------------------------------------------------------
            //Controllo a posteriori - do - while

            /*
             * do{
             *      corpo del do-while
             * } while(condizione)
             * 
             */

            //do
            //{
            //    Console.WriteLine("CIAO");
            //} while (false);

            //int indiceMassimo = 0;
            //int indiceAttuale = 0;

            //do
            //{
            //    Console.WriteLine($"L'indice attuale è: {indiceAttuale}");
            //    indiceAttuale++;
            //} while (indiceAttuale < indiceMassimo);

            //INTERRUZIONI

            //while (true)
            //{
            //    Console.WriteLine("CIAO");
            //    Console.ReadLine();
            //}

            bool scritturaAbilitata = true;

            while (scritturaAbilitata)
            {
                Console.WriteLine("Inserisci il tuo nome (QUIT per uscire)");
                string input = Console.ReadLine();

                if (input.Equals("QUIT"))
                {
                    scritturaAbilitata = !scritturaAbilitata;
                }
                else
                {
                    Console.WriteLine($"Ciao {input}");
                }
            }

            /*
             * STEP 1
             * Scrivere un sistema di gestione elenco invitati.
		     * L'inserimento avviene console che prende in input
		     * (in due tempi diversi) il nome ed il cognome.
		     * 
		     * All'uscita del programma verrà stampato l'elenco delle persone inserite
		     * in precedenza separate da virgola.
		     *
		     * STEP 2
		     * Prevedere una modifica al codice grazie alla quale l'elenco
		     * delle persone inserite viene stampato ad ogni nuovo inserimento avvenuto
		     * con successo.
		     */




        }
    }
}
