﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using VIEW_Lez07_Contatti_Semplice.Controllers;
using VIEW_Lez07_Contatti_Semplice.Models;

namespace VIEW_Lez07_Contatti_Semplice
{
    /// <summary>
    /// Logica di interazione per InserisciContatto.xaml
    /// </summary>
    public partial class InserisciContatto : Window
    {
        public InserisciContatto()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ContattoController gestore = new ContattoController();
            if(gestore.InserisciContatto(inNome.Text, inCognome.Text, inTelefono.Text))
            {
                MessageBox.Show("Inserimento effettuato con successo", "Tutto ok!", MessageBoxButton.OK, MessageBoxImage.Information);

                this.Close();
            }
                
            else
                MessageBox.Show("Non sono riuscito ad effettuare le operaizoni richieste", "Errore!", MessageBoxButton.OK, MessageBoxImage.Error);


        }
    }
}
