﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VIEW_Lez07_Contatti_Semplice.DAL;
using VIEW_Lez07_Contatti_Semplice.Models;

namespace VIEW_Lez07_Contatti_Semplice.Controllers
{
    class ContattoController
    {
        private static IConfiguration configuration;

        public ContattoController()
        {
            if(configuration == null)
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", false, false);

                configuration = builder.Build();
            }
        }

        public bool InserisciContatto(string varNome, string varCognome, string varTelefono)
        {
            Contatto temp = new Contatto()
            {
                Nome = varNome,
                Cognome = varCognome,
                Telefono = varTelefono
            };

            ContattoDAL contattoDAL = new ContattoDAL(configuration);
            return contattoDAL.Insert(temp);
        }

        public List<Contatto> VisualizzaTuttiContatti()
        {
            ContattoDAL contattoDAL = new ContattoDAL(configuration);
            return contattoDAL.ReadAll();
        }

        public bool EliminaContatto(int varId)
        {
            return (new ContattoDAL(configuration)).Delete(varId);
        }

        public bool ModificaContatto(int varId, string varNome, string varCognome, string varTelefono)
        {
            return (new ContattoDAL(configuration)).Update(new Contatto() { Id = varId, Nome = varNome, Cognome = varCognome, Telefono = varTelefono });
        }
    }
}
