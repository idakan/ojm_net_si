﻿using System;

namespace Lez02_04_TaskCicli
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
             * STEP 1
             * Scrivere un sistema di gestione elenco invitati.
		     * L'inserimento avviene console che prende in input
		     * (in due tempi diversi) il nome ed il cognome.
		     * 
		     * All'uscita del programma verrà stampato l'elenco delle persone inserite
		     * in precedenza separate da virgola.
		     *
		     * STEP 2
		     * Prevedere una modifica al codice grazie alla quale l'elenco
		     * delle persone inserite viene stampato ad ogni nuovo inserimento avvenuto
		     * con successo.
		     */

            bool inserimentoAbilitato = true;

            string elenco = "";

            while (inserimentoAbilitato)
            {
                Console.WriteLine("Inserisci il nome (Q per uscire): ");
                string nome = Console.ReadLine();

                if (nome.Equals("Q"))
                    break;

                Console.WriteLine("Inserisci il cognome (Q per uscire): ");
                string cognome = Console.ReadLine();

                if (cognome.Equals("Q"))
                    break;

                //elenco = elenco + $"{nome} {cognome}, ";
                elenco += $"{nome} {cognome}, ";
            }

            Console.WriteLine($"\nElenco: \n{elenco}");
        }
    }
}
