﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_04_Interface.Classes
{
    public class Gatto : IAnimale
    {
        public bool HaIlPelo { get; set; }

        public void tipoMovimento()
        {
            Console.WriteLine("Cammina");
        }

        public void versoEmesso()
        {
            Console.WriteLine("Meow");
        }
    }
}
