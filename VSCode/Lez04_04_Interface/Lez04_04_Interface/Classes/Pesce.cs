﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_04_Interface.Classes
{
    class Pesce : IAnimale
    {
        public void tipoMovimento()
        {
            Console.WriteLine("Nuota");
        }

        public void versoEmesso()
        {
            Console.WriteLine("° ° °");
        }
    }
}
