﻿using Lez04_04_Interface.Classes;
using System;

namespace Lez04_04_Interface
{
    class Program
    {
        static void Main(string[] args)
        {
            Gatto bu = new Gatto();
            Pesce pu = new Pesce();
            bu.tipoMovimento();
            pu.versoEmesso();
        }
    }
}
