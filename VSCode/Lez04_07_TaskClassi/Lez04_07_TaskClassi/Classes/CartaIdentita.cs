﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_07_TaskClassi.Classes
{
    class CartaIdentita
    {
        public string Codice { get; set; }
        public DateTime DataScadenza { get; set; }
        public DateTime DataEmissione { get; set; }

        private string emissione;

        public string Emissione
        {
            get { return emissione; }
            set {
                if (value.Equals("Comune") || value.Equals("Zecca"))
                    emissione = value;
                else
                    Console.WriteLine("[ERR] Errore, parametro non consentito!\n" +
                        "Parametri possibili solo: Comune|Zecca");
            }
        }
        public override string ToString()
        {
            string scadenza = DataScadenza.ToString("yyyy-MM-dd");
            string emissione = DataEmissione.ToString("yyyy-MM-dd");

            return $"CARTA DI IDENTITA: {Codice}\n" +
                $"Data Scad: {scadenza} " + 
                $"Data Emis: {emissione} " +
                $"Emesso da: {Emissione}";
        }

    }
}
