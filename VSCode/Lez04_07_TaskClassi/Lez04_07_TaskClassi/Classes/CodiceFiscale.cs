﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_07_TaskClassi.Classes
{
    public class CodiceFiscale
    {
        public string Codice { get; set; }
        public DateTime DataScadenza { get; set; }

        public override string ToString()
        {
            string scadenza = DataScadenza.ToString("yyyy-MM-dd");

            return $"CODICE FISCALE: {Codice} - Scad: {scadenza}";
        }
    }
}
