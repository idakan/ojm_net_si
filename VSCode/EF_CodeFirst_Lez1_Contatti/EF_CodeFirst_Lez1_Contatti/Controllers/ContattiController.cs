﻿using EF_CodeFirst_Lez1_Contatti.Data;
using EF_CodeFirst_Lez1_Contatti.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Lez1_Contatti.Controllers
{
    [ApiController]
    [Route("api/contatti")]
    public class ContattiController : Controller
    {
        private readonly InterfaceRepo<Contact> _repo;

        public ContattiController(InterfaceRepo<Contact> rep)
        {
            _repo = rep;
        }

        [HttpGet("lista")]
        public ActionResult<IEnumerable<Contact>> RestituisciContatti()
        {
            var elenco = _repo.GetAll();

            return Ok(elenco);
        }

        [HttpGet("{varId}")]
        public ActionResult<Contact> SingoloContatto(int varId)
        {
            var singolo = _repo.GetById(varId);

            return Ok(singolo);
        }
    }
}
