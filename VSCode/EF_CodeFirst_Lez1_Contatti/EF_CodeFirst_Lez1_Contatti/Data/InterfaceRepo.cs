﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Lez1_Contatti.Data
{
    public interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int varId);
    }
}
