﻿using EF_CodeFirst_Lez1_Contatti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Lez1_Contatti.Data
{
    public class ContactsRepo : InterfaceRepo<Contact>
    {
        private readonly ContactsContext _contesto;

        public ContactsRepo(ContactsContext cont)
        {
            _contesto = cont;
        }

        public IEnumerable<Contact> GetAll()
        {
            return _contesto.Contacts.ToList();
        }

        public Contact GetById(int varId)
        {
            return _contesto.Contacts.Where(o => o.Id == varId).FirstOrDefault();

        }
    }
}
