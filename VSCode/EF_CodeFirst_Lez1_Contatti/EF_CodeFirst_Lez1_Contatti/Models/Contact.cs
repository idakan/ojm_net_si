﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EF_CodeFirst_Lez1_Contatti.Models
{
    public class Contact
    {
        [Key]
        public int Id { get; set; }

        [MaxLength(250)]
        [Required]
        public string Nome { get; set; }

        [MaxLength(250)]
        [Required]
        public string Cognome { get; set; }

        [MaxLength(250)]
        [Required]
        public string Telefono { get; set; }

        [MaxLength(50)]
        public string Email { get; set; }

        [MaxLength(150)]
        [Required]
        public string CodiceFiscale { get; set; }
    }
}
