﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EF_CodeFirst_Lez1_Contatti.Migrations
{
    public partial class AggoCF : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CodiceFiscale",
                table: "Contacts",
                type: "nvarchar(150)",
                maxLength: 150,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CodiceFiscale",
                table: "Contacts");
        }
    }
}
