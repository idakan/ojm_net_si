﻿using ASP_Lez1_Contatti.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Data
{
    public class SqlContatti : InterfaceRepo<Contatto>
    {
        private string stringaConnessione = "Server=DESKTOP-6IND805\\SQLEXPRESS;Database=db_rubrica;User Id=sharpuser;Password=cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false";

        public bool Delete(int varId)
        {
            using (SqlConnection c = new SqlConnection(stringaConnessione))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = c;
                cmd.CommandText = "DELETE FROM Contatti " +
                                        "WHERE rubricaID = @varId";

                cmd.Parameters.AddWithValue("@varId", varId);

                c.Open();

                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }

        public IEnumerable<Contatto> GetAll()
        {
            List<Contatto> elenco = new List<Contatto>();

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT rubricaID, nome, cognome, telefono FROM Contatti";

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Contatto temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };

                    elenco.Add(temp);
                }
            }

            return elenco;
        }

        public Contatto GetById(int varId)
        {
            Contatto temp = null;

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT rubricaID, nome, cognome, telefono FROM Contatti WHERE rubricaID = @varId";
                comm.Parameters.AddWithValue("@varId", varId);

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();

                try
                {
                    reader.Read();

                    temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };
                } catch (Exception ex)
                {

                }
                
            }

            return temp;

        }

        public bool Insert(Contatto obj)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Contatti (nome, cognome, telefono) VALUES (@varNome, @varCognome, @varTelefono)";
                comm.Parameters.AddWithValue("@varNome", obj.Nome);
                comm.Parameters.AddWithValue("@varCognome", obj.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", obj.Telefono);

                connessione.Open();
                try
                {
                    int affRows = comm.ExecuteNonQuery();
                    if (affRows > 0)
                        return true;
                } catch (Exception ex)
                {
                    return false;
                }
            }

            return false;
        }

        public bool Update(Contatto obj)
        {
            using (SqlConnection c = new SqlConnection(stringaConnessione))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = c;
                cmd.CommandText = "UPDATE Contatti SET " +
                                        "nome = @varNome, " +
                                        "cognome = @varCognome, " +
                                        "telefono = @varTelefono " +
                                        "WHERE rubricaID = @varId";

                cmd.Parameters.AddWithValue("@varNome", obj.Nome);
                cmd.Parameters.AddWithValue("@varCognome", obj.Cognome);
                cmd.Parameters.AddWithValue("@varTelefono", obj.Telefono);
                cmd.Parameters.AddWithValue("@varId", obj.Id);

                c.Open();

                if (cmd.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }
    }
}
