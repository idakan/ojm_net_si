﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MONGO_Lez2_Libri.DAL;
using MONGO_Lez2_Libri.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_Lez2_Libri.Controllers
{
    [ApiController]
    [Route("api/libri")]
    public class BookController : Controller
    {
        private static BookRepo _repository;

        public BookController(IConfiguration configurazione)
        {
            if (_repository == null)
            {
                bool isLocale = configurazione.GetValue<bool>("IsLocale");

                string stringaConnessione = isLocale == true ?
                    configurazione.GetValue<string>("MongoSettings:DatabaseLocale") :
                    configurazione.GetValue<string>("MongoSettings:DatabaseRemoto");

                string nomeDatabase = configurazione.GetValue<string>("MongoSettings:NomeDatabase");

                _repository = new BookRepo(stringaConnessione, nomeDatabase);
            }  
        }

        [HttpGet]
        public ActionResult Lista()
        {
            return Ok(_repository.GetAll());
        }


        [HttpPost]
        public ActionResult Inserisci(Book libro)
        {
            if (_repository.Insert(libro))
                return Ok(new { Status = "success" });
            else
                return Ok(new { Status = "error" });
        }
    }
}
