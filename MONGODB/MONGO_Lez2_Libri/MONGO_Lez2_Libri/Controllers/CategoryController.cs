﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MONGO_Lez2_Libri.DAL;
using MONGO_Lez2_Libri.Models;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MONGO_Lez2_Libri.Controllers
{
    [ApiController]
    [Route("api/categorie")]
    public class CategoryController : Controller
    {
        private readonly CategoryRepo _repository;

        public CategoryController(IConfiguration configurazione)
        {
            bool isLocale = configurazione.GetValue<bool>("IsLocale");

            string stringaConnessione = isLocale == true ? 
                configurazione.GetValue<string>("MongoSettings:DatabaseLocale") : 
                configurazione.GetValue<string>("MongoSettings:DatabaseRemoto");

            string nomeDatabase = configurazione.GetValue<string>("MongoSettings:NomeDatabase");

            //if (_repository == null)
                _repository = new CategoryRepo(stringaConnessione, nomeDatabase);
        }

        [HttpPost("inserisci")]
        public ActionResult Inserisci(Category cat) {
            //cat.Codice = Guid.NewGuid().ToString().Substring(0, 5);
            cat.Codice = Guid.NewGuid().ToString();

            if (_repository.Insert(cat))
                return Ok(new { Status = "success", Descrizione = "" });

            return Ok(new { Status = "error", Descrizione = "Non sono riuscito ad inserire il documento!" });
        }

        [HttpGet]
        public ActionResult Lista()
        {
            return Ok(_repository.GetAll());
        }



    }
}
