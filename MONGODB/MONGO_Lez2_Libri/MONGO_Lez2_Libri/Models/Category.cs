﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_Lez2_Libri.Models
{
    public class Category
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }

        [Required]
        [StringLength(150)]
        public string Titolo { get; set; }

        [Required]
        [StringLength(150)]
        public string Scaffale { get; set; }

        [Required]
        [StringLength(150)]
        public string Descrizione { get; set; }

        [StringLength(150)]
        public string Codice { get; set; }
    }
}
