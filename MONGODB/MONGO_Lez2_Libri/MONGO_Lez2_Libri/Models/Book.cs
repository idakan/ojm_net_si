﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_Lez2_Libri.Models
{

    //TODO: Definzione di tutti i BSONELEMENT per evitare le maiuscole su DB
    public class Book
    {
        [BsonId]
        public ObjectId DocumentID { get; set; }

        [Required]
        [MaxLength(255)]
        public string Nome { get; set; }

        [Required]
        [MaxLength(255)]
        public string Descrizione { get; set; }

        [Required]
        [MaxLength(255)]
        public string Codice { get; set; }

        [Required]
        public int Quantita { get; set; }

        public ObjectId Category { get; set; }

        [Required]
        [BsonIgnore]
        public string Categoria { get; set; }       //Titolo della categoria da cui estrapolerà l'Object ID

        [BsonIgnore]
        public Category InfoCategoria { get; set; }
    }
}
