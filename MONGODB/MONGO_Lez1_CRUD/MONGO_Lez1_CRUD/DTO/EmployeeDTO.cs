﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MONGO_Lez1_CRUD.DTO
{
    public class EmployeeDTO
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string Cogn { get; set; }
        public string Tito { get; set; }
    }
}
