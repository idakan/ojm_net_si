﻿using System;
using System.Collections.Generic;
using System.Text;
using VIEW_Lez07_Contatti_Semplice.Models;

namespace VIEW_Lez07_Contatti_Semplice.DAL
{
    interface InterfaceDAL<T>
    {
        List<T> ReadAll();

        bool Insert(T t);
        bool Modify(T t);
        bool Delete(int id);
    }
}
