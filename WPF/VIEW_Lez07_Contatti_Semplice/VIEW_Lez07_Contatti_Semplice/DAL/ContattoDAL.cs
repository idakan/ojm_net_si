﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using VIEW_Lez07_Contatti_Semplice.Models;

namespace VIEW_Lez07_Contatti_Semplice.DAL
{
    class ContattoDAL : InterfaceDAL<Contatto>
    {
        public static string stringaConnessione;

        public ContattoDAL(IConfiguration configurazione)
        {
            if(stringaConnessione == null)
                stringaConnessione = configurazione.GetConnectionString("ServerLocale");
        }

        public bool Insert(Contatto t)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "INSERT INTO Contatto (nome, cognome, telefono) VALUES (@varNome, @varCognome, @varTelefono)";
                comm.Parameters.AddWithValue("@varNome", t.Nome);
                comm.Parameters.AddWithValue("@varCognome", t.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", t.Telefono);

                connessione.Open();
                int affRows = comm.ExecuteNonQuery();
                if (affRows > 0)
                    return true;
            }

            return false;
        }

        public List<Contatto> ReadAll()
        {
            List<Contatto> elenco = new List<Contatto>();

            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "SELECT contattoID, nome, cognome, telefono FROM Contatto";

                connessione.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    Contatto temp = new Contatto()
                    {
                        Id = Convert.ToInt32(reader[0]),
                        Nome = reader[1].ToString(),
                        Cognome = reader[2].ToString(),
                        Telefono = reader[3].ToString()
                    };

                    elenco.Add(temp);
                }
            }

            return elenco;
        }
        public bool Modify(Contatto t)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "UPDATE Contatto SET nome = @varNome, cognome = @varCognome, telefono = @varTelefono WHERE contattoID = @varId";
                comm.Parameters.AddWithValue("@varNome", t.Nome);
                comm.Parameters.AddWithValue("@varCognome", t.Cognome);
                comm.Parameters.AddWithValue("@varTelefono", t.Telefono);
                comm.Parameters.AddWithValue("@varId", t.Id);

                connessione.Open();
                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }
        public bool Delete(int varId)
        {
            using (SqlConnection connessione = new SqlConnection(stringaConnessione))
            {
                SqlCommand comm = new SqlCommand();
                comm.Connection = connessione;
                comm.CommandText = "DELETE FROM Contatto WHERE contattoID = @varId";
                comm.Parameters.AddWithValue("@varId", varId);

                connessione.Open();
                if (comm.ExecuteNonQuery() > 0)
                    return true;
            }

            return false;
        }
    }
}
