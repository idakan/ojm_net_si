﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using VIEW_Lez07_Contatti_Semplice.Controllers;
using VIEW_Lez07_Contatti_Semplice.Models;

namespace VIEW_Lez07_Contatti_Semplice
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ElencoContatti : Window
    {
        public ElencoContatti()
        {
            InitializeComponent();

            LeggiValoriDb();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            InserisciContatto inserimento = new InserisciContatto();
            inserimento.ShowDialog();

            LeggiValoriDb();
        }
        private void LeggiValoriDb()
        {
            ContattoController gestore = new ContattoController();
            List<Contatto> elenco = gestore.VisualizzaTuttiContatti();

            lvContatti.ItemsSource = elenco;
        }

        private void lvContatti_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListView lv = sender as ListView;
            Contatto selezionato = (Contatto)lv.SelectedItem;

            ModificaContatto modifica = new ModificaContatto(selezionato);
            modifica.ShowDialog();
            LeggiValoriDb();
        }
    }
}
