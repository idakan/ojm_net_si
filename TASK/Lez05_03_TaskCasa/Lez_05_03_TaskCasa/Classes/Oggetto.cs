﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez_05_03_TaskCasa.Classes
{
    class Oggetto
    {
        public string Nome { get; set; }
        public string Descrizione { get; set; }
        public float Valore { get; set; }
        public Oggetto(string nome, string descrizione, float valore)
        {
            Nome = nome;
            Descrizione = descrizione;
            Valore = valore;
        }
        public override string ToString()
        {
            return $"{Nome}\n{Descrizione}\nValore : {Valore} Euro\n";
        }
    }
}
