﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez_05_03_TaskCasa.Classes
{
    class Stanza
    {
        public string Nome { get; set; }
        public List<Oggetto> Oggetti { get; set; }
        public Stanza(string nome)
        {
            Oggetti = new List<Oggetto>();
            Nome = nome;
        }
        public void stampaOggetti()
        {
            foreach(Oggetto oggetto in Oggetti)
            {
                Console.WriteLine(oggetto);
            }
        }
    }
}
