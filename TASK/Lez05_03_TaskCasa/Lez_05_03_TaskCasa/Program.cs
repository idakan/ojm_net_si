﻿using Lez_05_03_TaskCasa.Classes;
using System;

namespace Lez_05_03_TaskCasa
{
    class Program
    {
        static void Main(string[] args)
        {
            /* 
            * Creare un sistema che mantenga traccia degli oggetti contenuti nelle stanze della vostra casa.
            * Di ogni oggetto voglio conoscere:
            * - Nome
            * - Descrizione
            * - Valore dell'oggetto
            * 
            * Di ogni stanza voglio conoscere il nome
            */
            Stanza[] casa = {
                new Stanza("Cucina"),
                new Stanza("Camera")
            };
            Oggetto frigo = new Oggetto("Frigo", "E' un frigo", 950.00f);
            Oggetto letto = new Oggetto("Letto", "E' un letto", 250.00f);
            Oggetto comodino = new Oggetto("Comodino", "E' un comodino", 50.00f);
            casa[0].Oggetti.Add(frigo);
            casa[1].Oggetti.Add(letto);
            casa[1].Oggetti.Add(comodino);
            casa[0].stampaOggetti();
            Console.WriteLine();
            casa[1].stampaOggetti();
        }
    }
}
