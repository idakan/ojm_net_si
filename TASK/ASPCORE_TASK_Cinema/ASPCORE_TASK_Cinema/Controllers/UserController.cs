﻿using ASPCORE_TASK_Cinema.Data;
using ASPCORE_TASK_Cinema.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Cinema.Controllers
{
    public class UserController : Controller
    {
        private UserRepo _repo;
        public UserController(IConfiguration config)
        {
            if (_repo == null)
            {
                bool isOnline = config.GetValue<bool>("IsOnline");
                string connString = isOnline ?
                    config.GetValue<string>("MongoDbSettings:OnlineDb") :
                    config.GetValue<string>("MongoDbSettings:LocalDb");

                string dbName = config.GetValue<string>("MongoDbSettings:DBName");


                _repo = new UserRepo(dbName, connString);
            }
        }
    }
}
