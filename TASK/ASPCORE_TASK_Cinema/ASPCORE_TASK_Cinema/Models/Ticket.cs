﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Cinema.Models
{
    public class Ticket
    {
        [BsonId]
        public ObjectId Id{ get; set; }

        [BsonElement("seat")]
        [Required]
        public int Seat { get; set; }

        [BsonElement("row")]
        [Required]
        public char Row { get; set; }

        [BsonElement("hall")]
        [Required]
        public int Hall { get; set; }

        [BsonElement("airTime")]
        [BsonDateTimeOptions]
        [Required]
        public DateTime AirTime { get; set; }

        [BsonElement("filmLength")]
        [Required]
        public int FilmLength { get; set; }

        [BsonElement("owner")]
        [Required]
        public ObjectId Owner { get; set; }

        [BsonIgnore]
        public User TicketOwner { get; set; }
    }
}
