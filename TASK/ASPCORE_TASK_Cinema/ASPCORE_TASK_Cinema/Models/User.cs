﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Cinema.Models
{
    public class User
    {
        [BsonId]
        public ObjectId Id{ get; set; }

        [BsonElement("fullname")]
        [Required(ErrorMessage = "The FullName field is required")]
        [MaxLength(50)]
        public string FullName{ get; set; }

        [BsonElement("taxId")]
        [Required(ErrorMessage = "The TaxId field is required")]
        [MaxLength(16),MinLength(16)]
        public string TaxId { get; set; }

        [BsonElement("email")]
        [Required(ErrorMessage = "The Email field is required")]
        //[RegularExpression(@"^[^@\s] +@[^@\s]+\.[^@\s]+$", ErrorMessage = "Your Email must follow the required standard")]
        [MaxLength(40)]
        public string Email { get; set; }

        [BsonElement("password")]
        [Required(ErrorMessage = "The Password field is required")]
        [MaxLength(50),MinLength(5)]
        public string Password { get; set; }

        [BsonIgnore]
        public List<Ticket> TicketList { get; set; }
    }
}
