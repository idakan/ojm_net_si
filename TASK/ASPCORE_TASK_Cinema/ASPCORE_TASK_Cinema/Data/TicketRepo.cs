﻿using ASPCORE_TASK_Cinema.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Cinema.Data
{
    public class TicketRepo : InterfaceRepo<Ticket>
    {
        private IMongoCollection<Ticket> tickets;

        public TicketRepo(string dbName, string connString)
        {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbName);

            if(tickets == null)
                tickets = db.GetCollection<Ticket>("Tickets");
        }

        public bool Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Ticket> GetAll()
        {
            return tickets.Find(FilterDefinition<Ticket>.Empty).ToList();
        }

        public Ticket GetById(ObjectId id)
        {
            return tickets.Find(i => i.Id == id).FirstOrDefault();
        }

        public bool Insert(Ticket t)
        {
            Ticket temp = tickets.Find(i => i.Id == t.Id).FirstOrDefault();

            if(temp == null)
            {
                tickets.InsertOne(t);
                return true;
            }
            return false;
        }

        public bool Update(Ticket t)
        {
            throw new NotImplementedException();
        }
    }
}
