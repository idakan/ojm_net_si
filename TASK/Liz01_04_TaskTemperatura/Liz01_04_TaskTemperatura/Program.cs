﻿using System;

namespace Liz01_04_TaskTemperatura
{
    class Program
    {
        static void Main(string[] args)
        {
            /**
             * Creare un sistema di controllo ingressi al ristorante:
             * In input avremo la temperatura (salvata in una variabile).
             * 
             * Se la temperatura è maggiore o uguale a 37,5° allora vietare l'ingresso al ristorante,
             * se è inferiore permettere l'ingresso!
             * 
             * ATTENZIONE, non ci vuole un medico per dire che sotto i 35 sei morto e sopra i 42 direi
             * che non è un valore accettabile!
             */

            float temp = 37.0f;

            if (temp > 35 && temp < 42)
            {
                if (temp >= 37.5)
                {
                    Console.WriteLine("Non puoi entrare, hai la temperatura troppo alta");
                }
                else
                {
                    Console.WriteLine("Puoi entrare prego");
                }
            }
            else
            {
                Console.WriteLine("Sei morto");
            }
        }
    }
}
