﻿using Lez04_07_TaskClassi.Classes;
using System;

namespace Lez04_07_TaskClassi
{
    class Program
    {
        static void Main(string[] args)
        {
            CodiceFiscale codFis = new CodiceFiscale("MNGLCU98L27H501Y","27/07/2026");
            CartaIdentita id = new CartaIdentita("AX867309","08/12/2016","27/07/2027","Comune");
            Persona luca = new Persona("Luca", "Mangano", codFis, id);
            Console.WriteLine(codFis);
            Console.WriteLine();
            Console.WriteLine(id);
            Console.WriteLine();
            Console.WriteLine(luca);

        }
    }
}
