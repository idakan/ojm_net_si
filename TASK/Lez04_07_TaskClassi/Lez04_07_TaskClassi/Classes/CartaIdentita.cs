﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_07_TaskClassi.Classes
{
    class CartaIdentita : Documento
    {
        public string DataEmissione { get; set; }
        private string enteEmissione;

        public string EnteEmissione
        {
            get { return enteEmissione; }
            set {
                if (value.Equals("Comune") || value.Equals("Zecca"))
                    enteEmissione = value;
                else
                    Console.WriteLine("Errore, parametro non consentito!\n" +
                        "Parametri possibili solo Comune|Zecca");
            }
        }

        public CartaIdentita(string codice, string dataScad, string dataEmissione, string enteEmissione)
        {
            Codice = codice;
            DataScadenza = dataScad;
            DataEmissione = dataEmissione;
            EnteEmissione = enteEmissione;
        }
        public override string ToString()
        {
            return $"Codice : {Codice}\nData di Scadenza : {DataScadenza}\nData di Emissione : {DataEmissione}\nEnte Emissione : {EnteEmissione}";
        }
    }
}
