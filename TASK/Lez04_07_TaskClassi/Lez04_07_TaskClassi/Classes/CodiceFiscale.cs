﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_07_TaskClassi.Classes
{
    class CodiceFiscale : Documento
    {
        public CodiceFiscale(string codice, string data)
        {
            Codice = codice;
            DataScadenza = data;
        }
        public override string ToString()
        {
            return $"Codice : {Codice}\nData di Scadenza : {DataScadenza}";
        }
    }
}
