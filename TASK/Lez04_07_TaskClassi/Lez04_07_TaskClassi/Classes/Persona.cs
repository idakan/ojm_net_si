﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez04_07_TaskClassi.Classes
{
    class Persona
    {
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public CodiceFiscale CodFis { get; set; }
        public CartaIdentita CartaId { get; set; }
        public Persona(string nome, string cognome, CodiceFiscale cod, CartaIdentita id)
        {
            Nome = nome;
            Cognome = cognome;
            CodFis = cod;
            CartaId = id;
        }
        public override string ToString()
        {
            return $"-------Credenziali---------------\nNome : {Nome}\nCognome : {Cognome}\n--------Codice Fiscale-----------\n{CodFis}\n--------Carta di Identità--------\n{CartaId}";
        }
    }
}
