function aggiornaTabelle()
{
    $.ajax(
        {
            url: "http://www.giovannimastromatteo.it/projects/prodotti.json",
            method: "GET",
            success: function(response)
            {
                let contAbb = "";
                let contCal = "";
                let contBor = "";
                let contUom = "";
                let contDon = "";
                let contSco = "";
                let contVen = "";

                for(let item of response)
                {
                    if(item.categoria == "abbigliamento")
                    {
                            contAbb += `
                                <tr>
                                    <td>${item.prodotto}</td>
                                    <td>${item.genere}</td>
                                    <td>${item.prezzo}€</td>
                                    <td>${item.venduti}</td>
                                </tr>
                            `;
                    }else if(item.categoria == "calzature")
                    {
                            contCal += `
                                <tr>
                                    <td>${item.prodotto}</td>
                                    <td>${item.genere}</td>
                                    <td>${item.prezzo}€</td>
                                    <td>${item.venduti}</td>
                                </tr>
                            `;
                    }else if(item.categoria == "borse")
                    {
                            contBor += `
                                <tr>
                                    <td>${item.prodotto}</td>
                                    <td>${item.genere}</td>
                                    <td>${item.prezzo}€</td>
                                    <td>${item.venduti}</td>
                                </tr>
                            `;
                    }
                    if(item.genere == "uomo")
                    {
                        contUom += `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                    }
                    else if(item.genere == "donna")
                    {
                        contDon += `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                    `;
                    }
                    if(item.prezzo < 30)
                    {
                        contSco += `
                        <tr>
                            <td>${item.prodotto}</td>
                            <td>${item.categoria}</td>
                            <td>${item.genere}</td>
                            <td>${item.prezzo}€</td>
                            <td>${item.venduti}</td>
                        </tr>
                        `;
                    }
                    
                }
                $("#tableAbbigliamento").html(contAbb);
                $("#tableCalzature").html(contCal);
                $("#tableBorse").html(contBor);
                $("#tableUomo").html(contUom);
                $("#tableDonna").html(contDon);
                $("#tableSconti").html(contSco);
                $("#tableDonna").html(contDon);

            },
            error:
                function(error){
                    console.log(error);
                }
        }
    );
}

$(document).ready(function(){
    aggiornaTabelle();
});