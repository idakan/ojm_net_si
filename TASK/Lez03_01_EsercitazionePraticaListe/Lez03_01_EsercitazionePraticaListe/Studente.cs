﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez03_01_EsercitazionePraticaListe
{
    class Studente
    {
        private string nome;
        private string matricola;
        private string provenienza;
        private int totaleCFU;
        private int annoCorr;
        public Studente(string nome, string matricola, string provenienza, int cfu)
        {
            Matricola = nome;
            Nome = matricola;
            Provenienza = provenienza;
            TotaleCFU = cfu;
        }
        public string Matricola
        {
            get { return matricola; }
            set { matricola = value; }
        }
        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        public string Provenienza
        {
            get { return provenienza; }
            set { provenienza = value; }
        }
        public int TotaleCFU
        {
            get { return totaleCFU; }
            set {
                if (value < 0)
                    totaleCFU = 0;
                else
                    totaleCFU = value; }
        }

        public override string ToString()
        {
            string asString = $"Nome : {Nome}\nMatricola: {Matricola}\nProvenienza: {Provenienza}";
            return asString;
        }
        public int CalcoloAnnoCorr()
        {
            if (TotaleCFU > 0 && TotaleCFU < 60)
            {
                annoCorr = 1;
            }
            else if (TotaleCFU > 60 && TotaleCFU < 120)
            {
                annoCorr = 2;
            }
            else if (TotaleCFU >= 120)
            {
                annoCorr = 3;
            }
            return annoCorr;
        }
    }
}
