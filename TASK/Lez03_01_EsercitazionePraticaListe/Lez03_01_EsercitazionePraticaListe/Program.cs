﻿using System;
using System.Collections.Generic;

namespace Lez03_01_EsercitazionePraticaListe
{
    class Program
    {
        private static List<Studente> listaStud = new List<Studente>();
        private static int StudExist(string matricola)
        {
            for (int i = 0; i < listaStud.Count; i++)
            {
                if (listaStud[i].Matricola.Equals(matricola))
                {
                    return i;
                }
            }
            return -1;
        }
        static void Main(string[] args)
        {
            /*
            * Realizzare un sistema di gestione dell'anagrafica studenti.
            * I valori da inserire, e consultare, sono i seguenti: Matricola(Univoca), Nome, Città di provenienza, totale CFU conseguiti
            * tra 0 e 180 (0<cfu<60 - primo anno, 60< = cfu < 120 secondo anno, cfu > = 120 terzo anno )
            * Avviando l'applicazione, l'utente si troverà di fronte ad un menù a scelta, con le seguenti options:
            * 1. inserimento nuovo studente
            * 2. aggiornamento dati studente
            * 3. eliminazione studente
            * 4. visualizzazione dettaglio scheda studente
            * 5. visualizzazione lista studenti
            * 6. verifica risultati studente
            * 7. Uscita dall'applicazione
            */
            //Variabili utilizzate
            int scelta;
            int studCorrente = -1;  //Variabile per la selezione dello studente durante le operazioni, l'intero indica la posizione dello studente nella lista: -1 --> Non trovato
            string matrCorrente;
            do
            {
                Console.WriteLine("1.inserimento nuovo studente\n2.aggiornamento dati studente\n3.eliminazione studente\n4.visualizzazione dettaglio scheda studente\n5.visualizzazione lista studente\n6.verifica risultati studente\n7.Uscita dall'applicazione");
                scelta = Convert.ToInt32(Console.ReadLine());
                switch(scelta)
                {
                    //Inserimento nuovo studente
                    case 1:
                        Console.WriteLine("Inserire nome e cognome dello studente: ");
                        string nome = Console.ReadLine();
                        Console.WriteLine("Inserire numero di Matricola dello studente: ");
                        string matricola = Console.ReadLine();
                        Console.WriteLine("Inserire città di provenienza dello studente: ");
                        string provenienza = Console.ReadLine();
                        Console.WriteLine("Inserire la quantità di CFU acquisiti: ");
                        int cfu = Convert.ToInt32(Console.ReadLine());
                        if (StudExist(matricola) > -1)
                        {
                            Console.WriteLine("Matricola già presente in archivio.");
                        }
                        else
                        {
                            Studente stud = new Studente(nome, matricola, provenienza, cfu);
                            listaStud.Add(stud);
                            Console.WriteLine("Studente aggiunto con successo!");
                        }
                        break;
                    //Aggiornamento studente corrente
                    case 2:
                        Console.WriteLine("Inserire la matricola dello studente da modificare: ");
                        matrCorrente = Console.ReadLine();
                        studCorrente = StudExist(matrCorrente);
                        //Se è stato trovato uno studente con la matricola inserita si entra nell'if
                        if (studCorrente > -1)
                        {
                            int scelta2;
                            do
                            {
                                Console.WriteLine("Quale campo dello studente si vuole modificare?\n1.Nome\n2.Matricola\n3.Città di provenienza\n4.CFU ottenuti\n5.Annulla operazione");
                                scelta2 = Convert.ToInt32(Console.ReadLine());
                                switch (scelta2)
                                {
                                    case 1:
                                        Console.WriteLine("Inserire il nuovo nome: ");
                                        listaStud[studCorrente].Nome = Console.ReadLine();
                                        Console.WriteLine("Operazione avvenuta con successo.");
                                        break;
                                    case 2:
                                        Console.WriteLine("Inserire la nuova matricola: ");
                                        listaStud[studCorrente].Matricola = Console.ReadLine();
                                        Console.WriteLine("Operazione avvenuta con successo.");
                                        break;
                                    case 3:
                                        Console.WriteLine("Inserire la nuova città di provenienza: ");
                                        listaStud[studCorrente].Provenienza = Console.ReadLine();
                                        Console.WriteLine("Operazione avvenuta con successo.");
                                        break;
                                    case 4:
                                        Console.WriteLine("Inserire la nuova quantità di CFU: ");
                                        listaStud[studCorrente].TotaleCFU = Convert.ToInt32(Console.ReadLine());
                                        Console.WriteLine("Operazione avvenuta con successo.");
                                        break;
                                    case 5:
                                        Console.WriteLine("Operazione annullata");
                                        break;
                                    default:
                                        Console.WriteLine("Campo non valido, inserire un numero compreso tra 1 e 4");
                                        break;
                                }
                            } while (scelta2 != 5);
                        }
                        else
                        {
                            Console.WriteLine("Studente non presente in archivio.");
                        }
                        break;
                    //Eliminazione studente
                    case 3:
                        Console.WriteLine("Inserire la matricola dello studente da eliminare dall'Anagrafica");
                        matrCorrente = Console.ReadLine();
                        studCorrente = StudExist(matrCorrente);
                        if (studCorrente > -1)
                        {
                            Console.WriteLine($"Lo studente con la matricola \"{matrCorrente}\" è il seguente :\n {listaStud[studCorrente].ToString()}\n\nConfermare l'eliminazione dello studente dall'anagrafica?(S/N)");
                            string risposta = Console.ReadLine();
                            if (risposta.Equals("S"))
                            {
                                listaStud.RemoveAt(studCorrente);
                            }
                        }
                        else
                        {
                            Console.WriteLine($"Non è stato trovato alcuno studente con la matricola : {matrCorrente}");
                        }
                        break;
                    case 4:
                        Console.WriteLine("Inserire la matricola dello studente di cui mostrare l'anagrafica");
                        matrCorrente = Console.ReadLine();
                        studCorrente = StudExist(matrCorrente);
                        if (studCorrente > -1)
                        {
                            Console.WriteLine($"Lo studente con la matricola \"{matrCorrente}\" è il seguente :\n {listaStud[studCorrente].ToString()}");
                        }
                        else
                        {
                            Console.WriteLine($"Non è stato trovato alcuno studente con la matricola : {matrCorrente}");
                        }
                        break;
                    case 5:
                        for (int i = 0; i < listaStud.Count; i++)
                        {
                            Console.WriteLine(value: $"---------Studente nr.{i + 1}---------\n{listaStud[i].ToString()}");
                        }
                        break;
                    case 6:
                        Console.WriteLine("Inserire la matricola dello studente di cui mostrare la quantità di CFU ottenuti:");
                        matrCorrente = Console.ReadLine();
                        studCorrente = StudExist(matrCorrente);
                        if (studCorrente > -1)
                        {
                            Console.WriteLine($"Lo studente con la matricola \"{matrCorrente}\" ha ottenuto :\n {listaStud[studCorrente].TotaleCFU} CFU classificandosi al {listaStud[studCorrente].CalcoloAnnoCorr()}° Anno");
                        }
                        else
                        {
                            Console.WriteLine($"Non è stato trovato alcuno studente con la matricola : {matrCorrente}");
                        }
                        break;
                    case 7:
                        Console.WriteLine("Uscita in corso...");
                        break;
                    default:
                        Console.WriteLine("Numero inserito non valido");
                        break;
                }
            } while (scelta != 7);
        }
    }
}
