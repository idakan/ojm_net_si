-- Prodotto(codiceProdotto, nomeProdotto, categoriaProdotto, descrizioneProdotto)
-- Produttore(idProduttore, nomeProduttore, citta, RecapitoTelefonico)
-- Fornitura(idProduttore, codiceProdotto, idNegozio, prezzoUnitarioProdotto, quantita)
-- Negozio(idNegozio, nomeNegozio, recapitoTelefonico , citta)

-- Trova i nomi di tutti i negozi che si trovano nella citt� di "ROMA".
SELECT nomeNegozio
    FROM Negozio
    WHERE citta = 'Roma';

-- Trova il nome del produttore che fornisce la quantit� maggiore di un qualsiasi prodotto.

SELECT nomeProduttore
    FROM Produttore
    JOIN Fornitura ON Fornitura.idProduttore = Produttore.idProduttore
    WHERE quantita >= ALL(SELECT quantita FROM fornitura);

-- Trova i nomi e le citt� di tutti i produttori che forniscono qualsiasi prodotto di
-- pi� di 500 unit� il cui prezzo unitario all'ingrosso � maggiore di 100,00�.

SELECT citta
    FROM Produttore
    JOIN Fornitura ON Fornitura.idProduttore = Produttore.idProduttore
    WHERE quantita > 500 
		AND prezzoUnitarioProdotto > 100.00;

-- Trova i nomi delle coppie negozio-produttore in cui il negozio e il produttore di ciascuna
-- coppia si trovano nella stessa citt� e c'� un record di vendita maggiore di 10000,00�.

SELECT nomeNegozio AS Negozio, nomeProduttore AS Produttore
    FROM Negozio
    JOIN Fornitura ON Fornitura.idNegozio = Negozio.idNegozio
    JOIN Produttore ON Produttore.idProduttore = Fornitura.idProduttore
    WHERE negozio.citta = produttore.citta
        AND (prezzoUnitarioProdotto * quantita) > 10000.00;

-- Trova il nome del negozio, la citt� e il nome del prodotto di tutti i prodotti
-- il cui prezzo unitario all'ingrosso � inferiore a 100 e la citt� non � "Frosinone".

SELECT Negozio.nomeNegozio, Negozio.citta, Prodotto.nomeProdotto
	FROM Negozio
    JOIN Fornitura ON Fornitura.idNegozio = Negozio.idNegozio
    JOIN Prodotto ON Prodotto.codiceProdotto = Fornitura.codiceProdotto
    WHERE prezzoUnitarioProdotto < 100 
		AND Negozio.citta != 'Frosinone';