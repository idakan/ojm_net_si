﻿using API_Test_Task.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API_Test_Task.Controllers
{
    [Route("api/contatto")]
    [ApiController]
    public class ContattiController : Controller
    {
        [HttpGet("lista")]
        public ActionResult<IEnumerable<ContattiController>> GetAllContatti()
        {
            var elenco = new List<Contatto>();

            try
            {
                using (var context = new bl_rubricaContext())
                {
                    elenco = context.Contattos.ToList();
                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }
            finally
            {
                Console.Write("CIAO");
            }

            return Ok(elenco);
        }

        [HttpGet("{varId}")]
        public ActionResult<Contatto> GetContatto(int varId)
        {
            Contatto temp = null;

            try
            {
                using (var context = new bl_rubricaContext())
                {
                    //temp = context.Contattis.Where(o => o.RubricaId == varId).FirstOrDefault();
                    temp = context.Contattos
                        .Where(o => o.ContattoId == varId)
                        .Select(k => new Contatto() { Cognome = k.Cognome, Nome = k.Nome, Telefono = k.Telefono })
                        .FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }

            return Ok(temp);
        }

        //TODO: Inserimento
        [HttpPost("insert")]
        public ActionResult InsertContatto(Contatto objContatto)
        {
            try
            {
                using (var context = new bl_rubricaContext())
                {
                    context.Contattos.Add(objContatto);
                    context.SaveChanges();
                    return Ok("Success!");
                }
            }
            catch (Exception e)
            {
                return Ok("Errore");
            }
        }
        //TODO: Modifica
        [HttpPut("{id}")]
        public ActionResult UpdateContatto(Contatto objContatto, int id)
        {
            try
            {
                using (var context = new bl_rubricaContext())
                {
                    var result = context.Contattos.Find(id);
                    if (result != null)
                    {
                        result.Nome = objContatto.Nome;
                        result.Cognome = objContatto.Cognome;
                        result.Telefono = objContatto.Telefono;
                        context.SaveChanges();
                    }
                    else
                    {
                        return Ok("Contatto non trovato");
                    }

                    return Ok("Success!");
                }
            }
            catch (Exception e)
            {
                return Ok("Errore");
            }
        }

        //TODO: Eliminazione
        [HttpDelete("{id}")]
        public ActionResult DeleteContatto(int id)
        {
            try
            {
                using (var context = new bl_rubricaContext())
                {
                    var result = context.Contattos.Find(id);
                    if(result != null)
                    {
                        context.Contattos.Remove(result);
                        context.SaveChanges();
                    }
                    else
                    {
                        return Ok("Contatto non esistente");
                    }
                    return Ok("Success!");
                }
            }
            catch (Exception e)
            {
                return Ok("Errore");
            }
        }
    }
}
