﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace API_Test_Task.Models
{
    public partial class bl_rubricaContext : DbContext
    {
        public bl_rubricaContext()
        {
        }

        public bl_rubricaContext(DbContextOptions<bl_rubricaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Contatto> Contattos { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=DESKTOP-J6HN12A\\SQLEXPRESS;Database=bl_rubrica;User Id=sharpuser;Password=cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Contatto>(entity =>
            {
                entity.ToTable("Contatto");

                entity.HasIndex(e => e.Telefono, "UQ__Contatto__2A16D945221FB53C")
                    .IsUnique();

                entity.Property(e => e.ContattoId).HasColumnName("contattoID");

                entity.Property(e => e.Cognome)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("cognome");

                entity.Property(e => e.Nome)
                    .IsRequired()
                    .HasMaxLength(250)
                    .IsUnicode(false)
                    .HasColumnName("nome");

                entity.Property(e => e.Telefono)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasColumnName("telefono");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
