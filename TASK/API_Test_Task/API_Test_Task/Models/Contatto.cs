﻿using System;
using System.Collections.Generic;

#nullable disable

namespace API_Test_Task.Models
{
    public partial class Contatto
    {
        public int ContattoId { get; set; }
        public string Nome { get; set; }
        public string Cognome { get; set; }
        public string Telefono { get; set; }
    }
}
