CREATE DATABASE bl_biblioteca

USE bl_biblioteca

CREATE TABLE Libri (
	libroID INT PRIMARY KEY IDENTITY(1,1),
	titolo varchar(255) NOT NULL,
	descrizione text NOT NULL,
	autore varchar(255) NOT NULL,
	isbn varchar(255) UNIQUE NOT NULL,
	quantita int NOT NULL DEFAULT 0,
);

INSERT INTO Libri(titolo,descrizione,autore,isbn,quantita) VALUES
	('Uno','asdfgh','UnoA','abc123',2),
	('Due','sdfgh','DueA','abc124',5),
	('Tre','dfghj','TreA','abc125',8),
	('Quattro','fghjk','QuattroA','abc126',1),
	('Cinque','ghjkl','CinqueA','abc127',0);