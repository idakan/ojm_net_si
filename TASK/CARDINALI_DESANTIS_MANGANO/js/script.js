function salvaFilm() {
    let input_film = $("#elenco_film").val();

    let elencoFilm = {
        nomeFilm: input_film

    }

    $.ajax(
        {
            url: "https://localhost:44324/api/film",
            method: "GET",
            data: JSON.stringify(elencoFilm),
            dataType: "json",
            contentType: "applicazione/json",
            success: function(response){
                switch(response.result){
                    case "success":
                        stampaFilm();

                        $("title").val("");
                        $("image").val("");
                        $("description").val("");
                        $("director").val("");
                        $("producer").val("");
                        $("release_date").val("");
                        $("running_time").val("");
                        $("profits").val("");
                        break;
                    case "error":
                        alert("Error: ;(" + response.description);
                        break;

                }
            },
            error: function(errore){
                console.log(errore)
                alert("Error ;(")
            }
        }
    );
}
                        

function stampaFilm(){
    $.ajax(
        {
            url: "https://localhost:44324/api/film",
            method: "GET",
            success: function(response){


                let contenuto = "";
                for(let item of response){
                    contenuto += 
                    //Da Rivedere Lasciato di appoggio
                    `
                    <hr/>
                    <div class="container bg-custom-purewhite text-center p-5">
            
                        <div class="row">
                            <div class="col-md mr-5">
                                <img src="${item.image}" alt="Immagine mancante">
                            </div>
                            <div class="col-md">
                                <div class="row">
                                    <div class="col-md text-center">
                                        <h1>${item.title}</h1>
                                        <h3>${item.producer}</h3>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md text-center">
                                        Data di uscita: ${item.releaseDate}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md text-center">
                                        Diretto da: ${item.director}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md text-center">
                                        Durata: ${item.runningTime}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md text-justify">
                                        ${item.description}
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-md p-3">
                                ${item.profits}€
                            </div>
                        </div>
            

                        <div class="row">
                            <div class="col-md">
                                <button id="btn_ins" type="button" class="btn btn-block btn-outline-dark">
                                    <i class="fa-solid fa-plus"></i>
                                    AGGIORNA DATI DEL FILM
                                </button>
                            </div>
                            <div class="col-md">
                                <button id="btn_ins" type="button" class="btn btn-block btn-outline-danger">
                                    <i class="fa-solid fa-plus"></i>
                                    ELIMINA FILM
                                </button>
                            </div>
                        </div>
            
                    </div>
                    `;   
                }

                $("#elenco_film").html(contenuto);

            },
            error: function(errore){
                console.log(errore)
                alert("Errore")
            }
        }
    );
}


$(document).ready(
    function(){
        $("#btn_ins").click(salvaFilm());
        stampaFilm();
       
    }
)