﻿using boxOffice_EF.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace boxOffice_EF.Controllers
{
    [Route("api/film")]
    [ApiController]
    public class FilmController : Controller
    {
        [HttpGet]
        public ActionResult<IEnumerable<Film>> GetAllContatti()
        {
            var elenco = new List<Film>();

            try
            {
                using (var context = new boxofficeContext())
                {
                    elenco = context.Films.OrderByDescending(x => x.Profits).ToList();
                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }
            finally
            {
                Console.Write("CIAO");
            }
            return Ok(elenco);
        }

        [HttpPost("insert")]
        public ActionResult InsertContatti(Film objFilm)
        {
            try
            {
                using (var context = new boxofficeContext())
                {
                    context.Films.Add(objFilm);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }
            return Ok("Successo");
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteContatti(int id)
        {
            try
            {
                using (var context = new boxofficeContext())
                {
                    var entity = context.Films.Find(id);
                    if(entity != null)
                    {
                        context.Films.Remove(entity);
                        context.SaveChanges();
                    }
                    else
                    {
                        return Ok("Contatto non trovato");
                    }
                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }
            return Ok("Successo");
        }

        [HttpPut("{id}")]
        public ActionResult UpdateContatto(Film objFilm, int id)
        {
            try
            {
                using (var context = new boxofficeContext())
                {
                    var entity = context.Films.Find(id);
                    if (entity != null)
                    {
                        entity.Title = objFilm.Title;
                        entity.Image = objFilm.Image;
                        entity.Description = objFilm.Description;
                        entity.Director = objFilm.Director;
                        entity.Producer = objFilm.Producer;
                        entity.ReleaseDate = objFilm.ReleaseDate;
                        entity.RunningTime = objFilm.RunningTime;
                        entity.Profits = objFilm.Profits;
                        context.SaveChanges();
                    }
                    else
                    {
                        return Ok("Contatto non trovato");
                    }
                }
            }
            catch (Exception ex)
            {
                return Ok("PROBLEMA");
            }
            return Ok("Successo");
        }

    }
}
