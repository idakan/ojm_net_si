﻿using ASPCORE_TASK_FitnessClub.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_FitnessClub.Data
{
    public class CourseRepo : InterfaceRepo<Course>
    {
        private IMongoCollection<Course> courses;

        public CourseRepo(string dbName, string connString)
        {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbName);

            if (courses == null)
                courses = db.GetCollection<Course>("Courses");
        }

        public bool Delete(ObjectId id)
        {
            var op = courses.DeleteOne<Course>(c => c.Id == id);

            if (op.IsAcknowledged && op.DeletedCount > 0)
                return true;
            return false;
        }

        public IEnumerable<Course> GetAll()
        {
            return courses.Find(FilterDefinition<Course>.Empty).ToList();
        }

        public Course GetById(ObjectId id)
        {
            return courses.Find(c => c.Id == id).FirstOrDefault();
        }

        public bool Insert(Course t)
        {
            throw new NotImplementedException();
        }

        public bool Update(Course t)
        {
            throw new NotImplementedException();
        }
    }
}
