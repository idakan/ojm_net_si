﻿using ASPCORE_TASK_FitnessClub.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_FitnessClub.Data
{
    public class UserRepo : InterfaceRepo<User>
    {
        private IMongoCollection<User> users;

        public UserRepo(string dbName, string connString)
        {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbName);

            if (users == null)
                users = db.GetCollection<User>("Users");
        }

        public bool Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll()
        {
            return users.Find(FilterDefinition<User>.Empty).ToList();
        }

        public User GetById(ObjectId id)
        {
            return users.Find(u => u.Id == id).FirstOrDefault();
        }

        public bool Insert(User t)
        {
            User temp = users.Find(u => u.Id == t.Id).FirstOrDefault();
            User emailCheck = users.Find(u => u.Email == t.Email).FirstOrDefault();
            User checkCert = users.Find(u => u.Certificate == t.Certificate).FirstOrDefault();
            if(temp == null && emailCheck == null && checkCert == null)
            {
                users.InsertOne(t);
                return true;
            }
            return false;
        }

        public bool Update(User t)
        {
            throw new NotImplementedException();
        }
    }
}
