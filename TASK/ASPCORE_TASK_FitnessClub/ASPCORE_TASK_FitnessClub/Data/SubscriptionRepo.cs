﻿using ASPCORE_TASK_FitnessClub.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_FitnessClub.Data
{
    public class SubscriptionRepo : InterfaceRepo<Subscription>
    {
        private IMongoCollection<Subscription> subscriptions;

        public SubscriptionRepo(string dbName, string connString)
        {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbName);

            if (subscriptions == null)
                subscriptions = db.GetCollection<Subscription>("Subscriptions");
        }

        public bool Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Subscription> GetAll()
        {
            return subscriptions.Find(FilterDefinition<Subscription>.Empty).ToList();
        }

        public Subscription GetById(ObjectId id)
        {
            return subscriptions.Find(s => s.Id == id).FirstOrDefault();
        }

        public bool Insert(Subscription t)
        {
            Subscription courseCheck = subscriptions.Find(s => s.CourseREF == t.CourseREF).FirstOrDefault();
            Subscription userCheck = subscriptions.Find(s => s.UserREF == t.UserREF).FirstOrDefault();

            if(courseCheck == null && userCheck == null)
            {
                subscriptions.InsertOne(t);
                return true;
            }
            return false;
        }

        public bool Update(Subscription t)
        {
            throw new NotImplementedException();
        }
    }
}
