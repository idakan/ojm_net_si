﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_FitnessClub.Models
{
    public class User
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("firstName")]
        [Required(ErrorMessage = "The First Name field is required")]
        public string FirstName { get; set; }

        [BsonElement("lastName")]
        [Required(ErrorMessage = "The Last Name field is required")]
        public string LastName { get; set; }

        [BsonElement("email")]
        [Required(ErrorMessage = "The Email field is required")]
        public string Email { get; set; }

        [BsonElement("password")]
        [Required(ErrorMessage = "The Password field is required")]
        public string Password { get; set; }

        [BsonElement("mobileNumber")]
        [Required(ErrorMessage = "The Mobile Number field is required")]
        public string MobileNumber { get; set; }

        [BsonElement("certificate")]
        [Required(ErrorMessage = "The Certificate field is required")]
        public string Certificate { get; set; }

        [BsonElement("certificateDate")]
        [Required(ErrorMessage = "The Certificate Date field is required")]
        [BsonDateTimeOptions(DateOnly = true)]
        public DateTime CertificateDate { get; set; }

        [BsonElement("subscriptions")]
        public List<ObjectId> Subscriptions { get; set; }

        [BsonIgnore]
        public List<Subscription> SubscriptionsList { get; set; }
    }
}
