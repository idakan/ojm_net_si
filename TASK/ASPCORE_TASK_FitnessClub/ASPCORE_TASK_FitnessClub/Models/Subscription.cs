﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;

namespace ASPCORE_TASK_FitnessClub.Models
{
    public class Subscription
    {
        [BsonId]
        public ObjectId Id { get; set; }

        [BsonElement("plan")]
        public int Plan { get; set; }

        [BsonElement("startDate")]
        [BsonDateTimeOptions]
        public DateTime StartDate { get; set; }

        [BsonElement("cost")]
        public float Cost { get; set; }

        [BsonElement("courseRif")]
        public ObjectId CourseREF { get; set; }

        [BsonElement("userRif")]
        public ObjectId UserREF { get; set; }
    }
}