let elenco = []

function send(){
    let varSong = document.getElementById("inSong").value;
    let varSinger = document.getElementById("inSinger").value;
    let varYear = document.getElementById("inYear").value;

    let element = {
        id: getId(),
        song: varSong,
        singer: varSinger,
        year: varYear,
    }

    elenco.push(element);
    printTable();

}

function getId(){
    if(elenco.length == 0)
        return 1;
    return elenco[elenco.length - 1].id + 1;
}

function printTable(){
    let content = "";

    for(let item of elenco)
    {
        content += `
        <tr>
            <td>${item.song}</td>
            <td>${item.singer}</td>
            <td>${item.year}</td>
            <td><button type="button" class="btn btn-danger" onclick="deleteItem('${item.id}')"><i class="fa-solid fa-trash-can"></i></button></td>
            <td><button type="button" class="btn btn-primary" onclick="openModal('${item.id}')"><i class="fa-solid fa-pencil"></i></button></td>
        </tr>
        `;
    }

    document.getElementById("tableBody").innerHTML = content;
}

function update(){
    let varId = document.getElementById("updateId").value;
    let varSong = document.getElementById("updateSong").value;
    let varSinger = document.getElementById("updateSinger").value;
    let varYear = document.getElementById("updateYear").value;

    let element = {
        id: varId,
        song: varSong,
        singer: varSinger,
        year: varYear,
    }

    for(let item of elenco)
    {
        if(item.id == element.id)
        {
            item.song = element.song;
            item.singer = element.singer;
            item.year = element.year;
            $("#modalUpdate").modal('hide');
            printTable();
            return;
        }
    }


}
 
function deleteItem(id){
    for(let [index, item] of elenco.entries())
    {
        if(item.id == id)
        {
            elenco.splice(index, 1);
        }
    }
    printTable();
}

function openModal(id)
{
    for(let item of elenco){
        if(item.id == id){
            document.getElementById("updateId").value = item.id;
            document.getElementById("updateSong").value = item.song;
            document.getElementById("updateSinger").value = item.singer;
            document.getElementById("updateYear").value = item.year;
        }
    }
    $("#modalUpdate").modal('show');
}

function toggle() {
    $("#firstTab").toggleClass("hide");
    $("#secondTab").toggleClass("hide");
}