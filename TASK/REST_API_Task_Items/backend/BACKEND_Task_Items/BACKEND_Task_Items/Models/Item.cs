﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace BACKEND_Task_Items.Models
{
    [Index(nameof(Serial), IsUnique = true)]
    public class Item
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(250)]
        [Required]
        public string Name { get; set; }
        [Column(TypeName = "TEXT")]
        public string Description { get; set; }
        [MaxLength(250)]
        [Required]
        public string Serial { get; set; }
        [Required]
        public int Quantity { get; set; }
    }
}
