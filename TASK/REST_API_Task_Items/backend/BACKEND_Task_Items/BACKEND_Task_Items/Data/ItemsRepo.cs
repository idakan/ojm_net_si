﻿using BACKEND_Task_Items.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACKEND_Task_Items.Data
{
    public class ItemsRepo : InterfaceRepo<Item>
    {
        private readonly ItemsContext _context;
        public ItemsRepo(ItemsContext context)
        {
            _context = context;
        }
        public bool DeleteById(int varId)
        {
            var result = _context.Items.Find(varId);
            if(result != null)
            {
                _context.Remove(result);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public IEnumerable<Item> GetAll()
        {
            return _context.Items.ToList();
        }

        public Item GetById(int varId)
        {
            return _context.Items.Find(varId);
        }

        public Item GetBySerial(string serial)
        {
            var result = _context.Items.Where(o => o.Serial.Equals(serial)).FirstOrDefault();
            return result;
        }

        public bool Insert(Item objItem)
        {
            _context.Items.Add(objItem);
            if (_context.SaveChanges() > 0)
                return true;
            else
                return false;
        }

        public bool UpdateById(Item objItem, int varId)
        {
            var result = _context.Items.Find(varId);
            if(result != null)
            {
                result.Name = objItem.Name;
                result.Description = objItem.Description;
                result.Serial = objItem.Serial;
                result.Quantity += objItem.Quantity;
                _context.SaveChanges();
                return true;
            }
            return false;
        }
    }
}
