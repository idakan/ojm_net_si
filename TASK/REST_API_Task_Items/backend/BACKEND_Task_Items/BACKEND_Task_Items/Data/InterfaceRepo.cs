﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACKEND_Task_Items.Data
{
    public interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(int varId);
        bool DeleteById(int varId);
        bool UpdateById(T objItem, int varId);
        bool Insert(T objTime);
        T GetBySerial(string Serial);

    }
}
