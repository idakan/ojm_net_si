﻿using BACKEND_Task_Items.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACKEND_Task_Items.Data
{
    public class ItemsContext : DbContext
    {
        public ItemsContext(DbContextOptions<ItemsContext> opt) : base(opt)
        {

        }

        public DbSet<Item> Items { get; set; }
    }
}
