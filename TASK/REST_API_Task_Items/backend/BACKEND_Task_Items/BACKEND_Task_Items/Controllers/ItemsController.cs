﻿using BACKEND_Task_Items.Data;
using BACKEND_Task_Items.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BACKEND_Task_Items.Controllers
{
    [ApiController]
    [Route("api/items")]
    public class ItemsController : Controller
    {
        private readonly InterfaceRepo<Item> _repo;

        public ItemsController(InterfaceRepo<Item> rep)
        {
            _repo = rep;
        }
        [HttpGet("list")]
        public ActionResult<IEnumerable<Item>> GetAll()
        {
            var list = _repo.GetAll();
            return Ok(list);
        }
        [HttpGet("{varId}")]
        public ActionResult<Item> GetById(int varId)
        {
            var item = _repo.GetById(varId);
            return Ok(item);
        }
        [HttpPut("{varId}")] //POTENZIALE UPDATE
        public ActionResult UpdateById(Item objItem, int varId)
        {
            var result = _repo.UpdateById(objItem, varId);
            return Ok(result);
        }
        [HttpDelete("{varId}")]
        public ActionResult DeleteById(int varId)
        {
            var result = _repo.DeleteById(varId);
            if (result)
                return Ok("Success");
            else
                return Ok("Error");
        }
        
        [HttpPost("insert")]
        public ActionResult Insert(Item objItem)
        {
            bool result = false;
            var item = _repo.GetBySerial(objItem.Serial);
            if (item == null)
            {
                result = _repo.Insert(objItem);
            }
            else
            {
                result = _repo.UpdateById(objItem, item.Id);
            }
            if (result)
                return Ok("Success");
            else
                return Ok("Error");
        }
    }
}
