function printItems()
{
    $.ajax(
        {
            url: "https://localhost:44389/api/items/list",
            method: "GET",
            success: function(response){
                let content = "";
                for(let item of response)
                {
                    content += `
                    <tr>
                        <td>${item.name}</td>
                        <td>${item.description}</td>
                        <td>${item.serial}</td>
                        <td>${item.quantity}</td>
                        <td>
                            <button type="button" class="btn btn-danger" onclick="deleteItem(${item.id})"><i class="fa-solid fa-trash-can"></i></button>
                        </td>
                    </tr>
                    `;
                }
                $("#tableBody").html(content);
            },
            error: function(error){
                alert(error);
            }

        }
    )
}

function insertItem(){
    let varName = $("#inputName").val();
    let varDesc = $("#inputDesc").val();
    let varSeri = $("#inputSeri").val();
    let varQnty = $("#inputQnty").val();

    let item = {
        Name: varName,
        Description: varDesc,
        Serial: varSeri,
        Quantity: Number(varQnty),
    }
    $.ajax(
        {
            url: "https://localhost:44389/api/items/insert",
            method: "POST",
            data: JSON.stringify(item),
            contentType: "application/json",
            success: function(response){
                switch(response){
                    case "Success":
                        printItems();
                        $("#inputName").val("");
                        $("#inputDesc").val("");
                        $("#inputSeri").val("");
                        $("#inputQnty").val("");
                        break;
                    case "Error":
                        alert("Insert Error");
                }
            },
            error: function(error){
                console.log(error);
                alert(error);
            }
        }
    )

}
function deleteItem(varId){
    $.ajax(
        {
            url: "https://localhost:44389/api/items/" + varId,
            method: "DELETE",
            success: function(risposta){
                switch(risposta){
                    case "Success":
                        printItems();
                        break;
                    case "Error":
                        alert("Errore");
                        break;
                }
            },
            error: function(error){
                console.log(error);
                alert("Error");
            }
        }
    );
}

$(document).ready(function(){
    printItems();
});