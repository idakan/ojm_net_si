﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace TASK_CHATEF2.Models
{
    public partial class archivio_messaggiContext : DbContext
    {
        public archivio_messaggiContext()
        {
        }

        public archivio_messaggiContext(DbContextOptions<archivio_messaggiContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Messaggi> Messaggis { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("Server=localhost\\SQLEXPRESS;Database=archivio_messaggi;User Id=sharpuser;Password=cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Messaggi>(entity =>
            {
                entity.HasKey(e => e.IdMessaggio)
                    .HasName("PK__Messaggi__F4E481E5B905EB0C");

                entity.ToTable("Messaggi");

                entity.Property(e => e.IdMessaggio).HasColumnName("idMessaggio");

                entity.Property(e => e.Messaggio)
                    .IsRequired()
                    .HasColumnType("text")
                    .HasColumnName("messaggio")
                    .HasDefaultValueSql("('')");

                entity.Property(e => e.NomeUtente)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("nomeUtente");

                entity.Property(e => e.Orario)
                    .HasColumnName("orario")
                    .HasDefaultValueSql("(getdate())");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
