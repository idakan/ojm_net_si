﻿using System;
using System.Collections.Generic;

#nullable disable

namespace TASK_CHATEF2.Models
{
    public partial class Messaggi
    {
        public int IdMessaggio { get; set; }
        public string NomeUtente { get; set; }
        public TimeSpan Orario { get; set; }
        public string Messaggio { get; set; }
    }
}
