if(localStorage.getItem("chat_nickname") == null)
    localStorage.setItem("chat_nickname", JSON.stringify([]));

let nickname = JSON.parse(localStorage.getItem("chat_nickname"));
stampaChat();

function login(){
    var nickname = $("#nicknameField").val();
    localStorage.setItem("chat_nickname", JSON.stringify(nickname));
    $(window.location).attr('href', 'chat.html');
    // $("#headerTitle").text("Hello world!");
    // document.getElementById("headerTitle").innerText = nickname;
}

function logout(){
    localStorage.clear();
    $(window.location).attr('href', 'index.html');
}

function stampaChat(){
    $.ajax(
        {
            url: "https://localhost:44378/api/Messaggi",
            method: "GET",
            success: function(response){
                let contenuto = "";
                for(let item of response)
                {
                    let orario = item.orario.hours+":"+item.orario.minutes+":"+item.orario.seconds;
                    contenuto += `
                        <li style="list-style-type: none">
                            <p class="">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <strong>${item.nomeUtente}</strong>
                                <small>${orario}</small>
                            </p> 
                            <p>${item.messaggio}</p>
                        </li>
                        <hr />
                    `;
                }
                $("#itemContent").html(contenuto);
            }
        }
    )
}

// function zeroFill(varTempo){
//     let zeros = "00";
//     let stringaFull = zeros+varTempo;
//     return stringaFull.slice(-2);
// }

function inviaMessaggio(){
    let username = nickname;
    // let date = new Date();
    // let ore = zeroFill(date.getHours());
    // let minuti = zeroFill(date.getMinutes());
    // let secondi = zeroFill(date.getSeconds());
    // let time = ore + ":" + minuti + ":" + secondi;
    // console.log(time);
    // console.log(nickname);
    let testo = $("#messaggio").val();

    let messaggio = {
        nomeUtente: username,
        // orario: time,
        messaggio: testo,
    }

    $.ajax(
        {
            url: "https://localhost:44378/api/Messaggi/insert",
            method: "POST",
            data: JSON.stringify(messaggio),
            dataType: "json",
            contentType: "application/json",
            success: function(risposta){
                switch(risposta.result){
                    case "Success":
                        $("#messaggio").val("");
                        break;
                    case "Error":
                        alert("Errore: " + risposta.description);
                        break;
                }
            },
            errore: function(errore){
                console.log(errore);
                alert("Errore");
            }
        }
    );
}

$(document).ready(
    function(){
        $("#welcome").html(`<h1>Benvenuto ${nickname}</h1>`);
        let autoUpdate = setInterval(function(){
            stampaChat();
        },100);

        console.log(localStorage.getItem("chat_nickname"));
        setTimeout(function(){
            if(localStorage.getItem("chat_nickname") == "[]" && window.location.href == "http://127.0.0.1:5500/chat.html")
            {
                 $(window.location).attr('href', 'index.html');
            }
        },1000);
    }   
)