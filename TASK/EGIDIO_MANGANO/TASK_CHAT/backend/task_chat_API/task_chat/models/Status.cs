﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Lez1_Contatti.Models
{
    public class Status
    {
        /// <summary>
        /// Result resituisce "succes" | "error"
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// Descrizione estesa in caso di errore
        /// </summary>
        public string Description { get; set; }
    }
}
