﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TASK_CHAT.models
{
    public class Messaggio
    {

        public int IdMessaggio { get; set; }
        public string NomeUtente { get; set; }
        public string Orario { get; set; }
        public string Contenuto { get; set; }
    }
}
