﻿using ASP_Lez1_Contatti.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TASK_CHAT.Data;
using TASK_CHAT.models;

namespace TASK_CHAT.Controllers
{
    [Route("api/Messaggi")]
    [ApiController]

    public class ControllerMessaggi : Controller
    {

        private readonly SqlMessaggi _repo = new SqlMessaggi();

        [HttpPost, HttpGet]

        public ActionResult<IEnumerable<Messaggio>> GetAllMessaggi()
        {
            var elenco = _repo.GetAll();
            return Ok(elenco);
        }


        [HttpPost("insert")]

        public ActionResult<Status> InsertMessaggio(Messaggio objMessaggio)
        {
            if (_repo.insert(objMessaggio))
            {
                return Ok(new Status() { Result = "Success", Description = "" });
            }
            else
            {
                return Ok(new Status() { Result = "Error", Description = "Errore di inserimento" });

            }

        }
    }
}
