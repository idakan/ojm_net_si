﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using TASK_CHAT.models;

namespace TASK_CHAT.Data
{
    public class SqlMessaggi : IRepoData<Messaggio>
    {
        private string stringaConnessione = "Server=DESKTOP-J6HN12A\\SQLEXPRESS;Database=archivio_messaggi;User Id=sharpuser; Password=cicciopasticcio;MultipleActiveResultSets=true;Encrypt=false;TrustServerCertificate=false";
        public IEnumerable<Messaggio> GetAll()
        {
            List<Messaggio> elenco = new List<Messaggio>();

            using(SqlConnection con = new SqlConnection(stringaConnessione))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = con;
                cmd.CommandText="Select nomeUtente,orario,messaggio,idMessaggio FROM Messaggi";


                con.Open();

                
                    SqlDataReader read = cmd.ExecuteReader();
                    while (read.Read())
                    {
                        Messaggio temp = new Messaggio()
                        {
                            NomeUtente = read[0].ToString(),
                            Orario = read[1].ToString(),
                            Contenuto = read[2].ToString(),
                            IdMessaggio = Convert.ToInt32(read[3])
                        };
                        elenco.Add(temp);
                    }
                    return elenco;
                
            }
        }

            public bool insert(Messaggio obj)
        {
           

            using (SqlConnection conn = new SqlConnection(stringaConnessione))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandText = "INSERT INTO Messaggi(nomeUtente,orario,messaggio) VALUES(@varNome,@varOra,@varMess)";
                cmd.Parameters.AddWithValue("@varNome", obj.NomeUtente);
                cmd.Parameters.AddWithValue("@varOra", obj.Orario);
                cmd.Parameters.AddWithValue("@varMess", obj.Contenuto);

                conn.Open();

                try
                {
                    int AffRows = cmd.ExecuteNonQuery();
                    if (AffRows > 0)
                    {
                        return true;
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
                return false;
            }
        }


    }
}
