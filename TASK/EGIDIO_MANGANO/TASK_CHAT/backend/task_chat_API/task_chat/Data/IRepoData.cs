﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TASK_CHAT.models
{
    interface IRepoData<T>
    {

        IEnumerable<T> GetAll();

        bool insert(T obj);

    }
}
