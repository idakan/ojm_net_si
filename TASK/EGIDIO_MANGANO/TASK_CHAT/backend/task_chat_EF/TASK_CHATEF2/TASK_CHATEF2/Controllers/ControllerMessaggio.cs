﻿using ASP_Lez1_Contatti.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TASK_CHATEF2.Models;

namespace TASK_CHATEF2.Controllers
{
    [Route("api/Messaggi")]
    [ApiController]
    public class ControllerMessaggio : Controller
    {

            [HttpGet]
            public ActionResult<IEnumerable<Messaggi>> GetAllContatti()
            {
                var elenco = new List<Messaggi>();

                try
                {
                    using (var context = new archivio_messaggiContext())
                    {
                        elenco = context.Messaggis.ToList();
                    }
                }
                catch (Exception ex)
                {
                    return Ok("Success");
                }

                return Ok(elenco);
            }


            [HttpPost("insert")]
            public ActionResult inserMessaggio(Messaggi objMess)
            {
                try
                {
                    using (var context = new archivio_messaggiContext())
                    {
                        context.Messaggis.Add(objMess);
                        context.SaveChanges();
                        return Ok(new Status() { Result = "success", Description = "" });
                    }
                }
                catch (Exception e)
                {
                    return Ok(new Status() { Result = "error", Description = "Errore di eliminazione, contatto non trovato" });
                }
            }
        }
    }

