CREATE DATABASE archivio_messaggi;
USE archivio_messaggi;

CREATE TABLE Messaggi(
    idMessaggio INT PRIMARY KEY IDENTITY(1,1),
    nomeUtente VARCHAR(255) NOT NULL,
    orario TIME  DEFAULT CURRENT_TIMESTAMP not null,
    messaggio TEXT NOT NULL DEFAULT '',
);