﻿using ASPCORE_TASK_Palestra.Data;
using ASPCORE_TASK_Palestra.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Palestra.Controllers
{
    public class UtenteController : Controller
    {
        private UtenteRepo _repoUtente;
        private CorsoRepo _repoCorso;
        private IscrizioneRepo _repoIscrizione;

        public UtenteController(IConfiguration config)
        {
            if(_repoUtente == null)
            {
                bool isOnline = config.GetValue<bool>("IsOnline");

                string connString = isOnline ?
                    config.GetValue<string>("MongoDbSettings:OnlineDb") :
                    config.GetValue<string>("MongoDbSettings:LocalDb");

                string dbString = config.GetValue<string>("MongoDbSettings:NomeDb");

                _repoUtente = new UtenteRepo(connString, dbString);
                _repoCorso = new CorsoRepo(connString, dbString);
                _repoIscrizione = new IscrizioneRepo(connString, dbString);
            }
        }
        public RedirectResult Logout()
        {
            HttpContext.Session.Clear();
            return Redirect("/Utente/Login");
        }
        public IActionResult Login()
        {
            if (HttpContext.Session.GetString("isLogged") == null)
                return View();
            return Redirect("/Utente/IMieiCorsi");
        }

        public RedirectResult LoginCheck(Utente objUtente)
        {
            Utente loginAttempt = _repoUtente.GetByEmail(objUtente.Email);
            if(loginAttempt != null && loginAttempt.Password == objUtente.Password)
            {
                HttpContext.Session.SetString("isLogged", loginAttempt.Email);
                return Redirect("/Utente/MyCourses");
            }
            else
            {
                return Redirect("/Utente/Login");
            }
        }

        public IActionResult SignUp()
        {
            return View();
        }

        public IActionResult SignedIn()
        {
            return View();
        }

        [HttpPost]
        public IActionResult SignUp(Utente objUtente)
        {
            if (ModelState.IsValid)
            {
                _repoUtente.Insert(objUtente);
                return Redirect("/Utente/SignedIn");
            }
            return View(objUtente);
            
        }
        public IActionResult Courses()
        {
            return View(_repoCorso.GetAll().ToList());
        }

        public IActionResult IscrizioneCorso(string codice)
        {
            ObjectId idCorso = _repoCorso.GetByCodice(codice).Id;
            string currentUser = HttpContext.Session.GetString("isLogged");
            if (currentUser != null)
            {
                //Prendo i dati dell'utente al momento loggato
                Utente temp = _repoUtente.GetByEmail(currentUser);

                //Se l'utente non è iscitto ad alcun corso allora lo iscrivo direttamente
                if(_repoIscrizione.GetAll().Where(i => i.UtenteId == temp.Id) == null)
                {
                    //Inizializzo una nuova iscrizione al corso selezionato
                    Iscrizione newSub = new Iscrizione()
                    {
                        CorsoId = idCorso,
                        UtenteId = temp.Id,
                        IscrizioneId = new ObjectId(),
                        DataIscrizione = DateTime.Now,
                        CorsoRIF = _repoCorso.GetById(idCorso),
                        UtenteRIF = temp
                    };
                    temp.ElencoIscrizioni.Add(newSub);
                    _repoIscrizione.Insert(newSub);
                    return Redirect("/Utente/MyCourses");
                }
                else
                {
                    var findCorso = _repoIscrizione.GetAll()
                        .Where(i => i.CorsoId == idCorso)
                        .Where(i => i.UtenteId == temp.Id)
                        .FirstOrDefault();
                    //L'utente non è già iscritto al corso, ce lo iscrivo
                    if (findCorso == null)
                    {
                        //Inizializzo una nuova iscrizione al corso selezionato
                        Iscrizione newSub = new Iscrizione()
                        {
                            CorsoId = idCorso,
                            UtenteId = temp.Id,
                            IscrizioneId = new ObjectId(),
                            DataIscrizione = DateTime.Now,
                            CorsoRIF = _repoCorso.GetById(idCorso),
                            UtenteRIF = temp
                        };
                        temp.ElencoIscrizioni.Add(newSub);
                        _repoIscrizione.Insert(newSub);
                        return Redirect("/Utente/MyCourses");
                    }
                    //L'utente è già iscritto al corso
                    else
                        return Redirect("/Utente/MyCourses");
                }
            }
            return Redirect("/Utente/Login");
        }

        public IActionResult MyCourses()
        {
            string currentUser = HttpContext.Session.GetString("isLogged");
            if (currentUser != null)
            {
                Utente temp = _repoUtente.GetByEmail(currentUser);
                List<Corso> elencoCorsi = _repoIscrizione.GetAll()
                    .Where(i => i.UtenteId == temp.Id)
                    .Select(i => _repoCorso.GetById(i.CorsoId)).ToList();


                return View(elencoCorsi);
            }
            return Redirect("/Utente/Login");
        }

        public RedirectResult Disiscriviti(string codice)
        {
            ObjectId idCorso = _repoCorso.GetByCodice(codice).Id;
            string currentUser = HttpContext.Session.GetString("isLogged");
            if (currentUser != null)
            {
                Utente temp = _repoUtente.GetByEmail(currentUser);
                Iscrizione queryIscrizione = _repoIscrizione.GetAll()
                    .Where(i => i.CorsoId == idCorso)
                    .Where(i => i.UtenteId == temp.Id)
                    .FirstOrDefault();
                if (_repoIscrizione.Delete(queryIscrizione.IscrizioneId))
                {
                    temp.ElencoIscrizioni.Remove(queryIscrizione);
                    return Redirect("/Utente/MyCourses");
                }
            }
            return Redirect("/Utente/Courses");
        }

        public IActionResult AreaPersonale()
        {
            string currentUser = HttpContext.Session.GetString("isLogged");
            if(currentUser != null)
            {
                ViewBag.TempClass = "disabled";
                Utente temp = _repoUtente.GetByEmail(currentUser);
                return View(temp);
            }
            return Redirect("/Utente/Login");
        }

        public IActionResult ModificaDati()
        {
            string currentUser = HttpContext.Session.GetString("isLogged");
            if (currentUser != null)
            {
                ViewBag.TempClass = "disabled";
                Utente temp = _repoUtente.GetByEmail(currentUser);
                return View(temp);
            }
            return Redirect("/Utente/Login");
        }

        [HttpPost]
        public IActionResult ModificaDati(Utente objUtente)
        {
            if(ModelState.IsValid)
            {
                if(_repoUtente.Update(objUtente))
                    return Redirect("/Utente/AreaPersonale");
            }
            return View(objUtente);
        }
    }
}
