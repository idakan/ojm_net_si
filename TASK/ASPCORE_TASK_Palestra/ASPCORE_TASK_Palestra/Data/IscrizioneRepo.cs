﻿using ASPCORE_TASK_Palestra.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Palestra.Data
{
    public class IscrizioneRepo : InterfaceRepo<Iscrizione>
    {
        private IMongoCollection<Iscrizione> iscrizioni;
        private string connString;
        private string dbString;

        public IscrizioneRepo(string connString, string dbString)
        {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbString);

            if (iscrizioni == null)
                iscrizioni = db.GetCollection<Iscrizione>("Iscriziones");

            this.connString = connString;
            this.dbString = dbString;
        }
        public bool Delete(ObjectId id)
        {
            Iscrizione temp = iscrizioni.Find(i => i.IscrizioneId == id).FirstOrDefault();
            if(temp != null)
            {
                var op = iscrizioni.DeleteOne<Iscrizione>(i => i.IscrizioneId == id);
                if (op.IsAcknowledged && op.DeletedCount > 0)
                    return true;
            }
            return false;
        }

        public IEnumerable<Iscrizione> GetAll()
        {
            return iscrizioni.Find(FilterDefinition<Iscrizione>.Empty).ToList();
        }

        public Iscrizione GetById(ObjectId id)
        {
            return iscrizioni.Find(i => i.IscrizioneId == id).FirstOrDefault();
        }

        public bool Insert(Iscrizione t)
        {
            Iscrizione temp = iscrizioni.Find(i => i.IscrizioneId == t.IscrizioneId).FirstOrDefault();
            if (temp == null)
            { 
                iscrizioni.InsertOne(t);
                return true;
            }

            return false;
        }

        public bool Update(Iscrizione t)
        {
            throw new NotImplementedException();
        }
    }
}
