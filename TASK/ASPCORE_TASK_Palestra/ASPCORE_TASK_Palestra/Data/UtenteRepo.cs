﻿using ASPCORE_TASK_Palestra.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Palestra.Data
{
    public class UtenteRepo : InterfaceRepo<Utente>
    {
        private IMongoCollection<Utente> utenti;
        private string connString;
        private string dbString;

        public UtenteRepo(string connString, string dbString)
        {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbString);

            if (utenti == null)
                utenti = db.GetCollection<Utente>("Utentes");

            this.connString = connString;
            this.dbString = dbString;
        }

        public bool Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Utente> GetAll()
        {
            throw new NotImplementedException();
        }

        public Utente GetByEmail(string email)
        {
            return utenti.Find(i => i.Email == email).FirstOrDefault();
        }

        public Utente GetById(ObjectId id)
        {
            return utenti.Find(i => i.Id == id).FirstOrDefault();
        }

        public bool Insert(Utente t)
        {
            Utente temp = utenti.Find(i => i.Id == t.Id).FirstOrDefault();
            Utente emailCheck = GetByEmail(t.Email);
            if (temp == null && emailCheck == null)
            {
                utenti.InsertOne(t);
                return true;
            }
            return false;
        }

        public bool Update(Utente t)
        {
            Utente temp = utenti.Find(i => i.Email == t.Email).FirstOrDefault();
            if(temp != null)
            {
                temp.Nome = t.Nome;
                temp.Cognome = t.Cognome;
                temp.Email = t.Email;
                temp.Password = t.Password;
                temp.DataNascita = t.DataNascita;
                temp.Indirizzo = t.Indirizzo;

                var filter = Builders<Utente>.Filter.Eq(i => i.Email, t.Email);
                var risultato = utenti.ReplaceOne(filter, temp);
                if (risultato.IsAcknowledged && risultato.ModifiedCount > 0)
                    return true;
                else
                    return false;
            }

            return false;
        }
    }
}
