﻿using ASPCORE_TASK_Palestra.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Palestra.Data
{
    public class CorsoRepo : InterfaceRepo<Corso>
    {
        private IMongoCollection<Corso> corsi;
        private string connString;
        private string dbString;

        public CorsoRepo(string connString, string dbString)
        {
            var client = new MongoClient(connString);
            var db = client.GetDatabase(dbString);

            if (corsi == null)
                corsi = db.GetCollection<Corso>("Corsos");

            this.connString = connString;
            this.dbString = dbString;
        }
        public bool Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Corso> GetAll()
        {
            return corsi.Find(FilterDefinition<Corso>.Empty).ToList();
        }

        public Corso GetByCodice(string codice)
        {
            return corsi.Find(i => i.Codice == codice).FirstOrDefault();
        }

        public Corso GetById(ObjectId id)
        {
            return corsi.Find(i => i.Id == id).FirstOrDefault();
        }

        public bool Insert(Corso t)
        {
            throw new NotImplementedException();
        }

        public bool Update(Corso t)
        {
            throw new NotImplementedException();
        }
    }
}
