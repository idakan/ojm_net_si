﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Palestra.Models
{
    public class Corso
    {
        [BsonId]
        [BsonElement("corsoId")]
        public ObjectId Id { get; set; }
        [BsonElement("titolo")]
        [Required]
        public string Titolo { get; set; }
        [Required]
        [BsonElement("descrizione")]
        [MaxLength(300)]
        public string Descrizione { get; set; }
        [BsonElement("codice")]
        [Required]
        public string Codice { get; set; }
        [BsonElement("dataora")]
        [Required]
        public DateTime DataOra { get; set; }
        [BsonIgnore]
        public List<Iscrizione> ElencoIscrizioni { get; set; }
    }
}
