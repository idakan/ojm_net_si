﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Palestra.Models
{
    public class Utente
    {
        [BsonElement("utenteId")]
        public ObjectId Id { get; set; }

        [BsonElement("nome")]
        [Required(ErrorMessage = "Il campo Nome è obbligatorio")]
        [MaxLength(100)]
        public string Nome { get; set; }
        
        [BsonElement("cognome")]
        [Required(ErrorMessage = "Il campo Cognome è obbligatorio")]
        [MaxLength(100)]
        public string Cognome { get; set; }
        
        [BsonElement("indirizzo")]
        [MaxLength(100)]
        public string Indirizzo { get; set; }

        [BsonElement("dataNascita")]
        [Required(ErrorMessage = "Il campo Data di Nascita è obbligatorio")]
        [BsonDateTimeOptions(DateOnly = true)]
        [MaxLength(100)]
        private DateTime dataNascita;

        [BsonIgnore]
        public DateTime DataNascita
        {
            get { return dataNascita; }
            set { dataNascita = (DateTime)value; }
        }

        [BsonElement("email")]
        [Required(ErrorMessage = "Il campo Email è obbligatorio")]
        [MaxLength(100)]
        public string Email { get; set; }
        
        [BsonElement("password")]
        [Required(ErrorMessage = "La password deve essere lunga minimo 6 caratteri")]
        [MaxLength(100),MinLength(6, ErrorMessage = "La password deve essere lunga minimo 6 caratteri")]
        public string Password { get; set; }

        [BsonIgnore]
        private List<Iscrizione> elencoIscrizioni = new List<Iscrizione>();
        [BsonIgnore]
        public List<Iscrizione> ElencoIscrizioni
        {
            get { return elencoIscrizioni; }
            set { elencoIscrizioni = value; }
        }

}
}
