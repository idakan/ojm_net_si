﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASPCORE_TASK_Palestra.Models
{
    public class Iscrizione
    {
        [BsonElement("iscrizioneId")]
        [Required]
        [BsonId]
        public ObjectId IscrizioneId { get; set; }

        [BsonElement("dataIscrizione")]
        [Required]
        public DateTime DataIscrizione { get; set; }

        [BsonElement("corsoId")]
        [Required]
        public ObjectId CorsoId { get; set; }

        [BsonElement("utenteId")]
        [Required]
        public ObjectId UtenteId { get; set; }

        [BsonIgnore]
        public Corso CorsoRIF { get; set; }

        [BsonIgnore]
        public Utente UtenteRIF { get; set; }
    }
}
