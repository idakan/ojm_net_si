﻿using Mongo_TASK_Prodotti.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_TASK_Prodotti.DAL
{
    public class CategoryRepo : InterfaceRepo<Category>
    {
        private IMongoCollection<Category> categories;

        private string connString;
        private string dataString;

        public CategoryRepo(string strConn, string strData)
        {
            var client = new MongoClient(strConn);
            var db = client.GetDatabase(strData);

            if (categories == null)
                categories = db.GetCollection<Category>("Category");

            connString = strConn;
            dataString = strData;
        }


        public bool Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Category> GetAll()
        {
            throw new NotImplementedException();
        }

        public Category GetById(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Category t)
        {
            throw new NotImplementedException();
        }

        public bool Update(Category t)
        {
            throw new NotImplementedException();
        }
    }
}
