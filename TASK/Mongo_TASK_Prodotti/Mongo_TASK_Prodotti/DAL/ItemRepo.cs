﻿using Mongo_TASK_Prodotti.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_TASK_Prodotti.DAL
{
    public class ItemRepo : InterfaceRepo<Item>
    {
        private IMongoCollection<Item> items;

        private string connString;
        private string dataString;

        public ItemRepo(string strConn, string strData)
        {
            var client = new MongoClient(strConn);
            var db = client.GetDatabase(strData);

            if (items == null)
                items = db.GetCollection<Item>("Items");

            connString = strConn;
            dataString = strData;
        }

        public bool Delete(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Item> GetAll()
        {
            List<Item> list = items.Find(FilterDefinition<Item>.Empty).ToList();

            ItemRepo tempRepo = new ItemRepo(connString, dataString);
        }

        public Item GetById(ObjectId id)
        {
            throw new NotImplementedException();
        }

        public bool Insert(Item t)
        {
            throw new NotImplementedException();
        }

        public bool Update(Item t)
        {
            throw new NotImplementedException();
        }
    }
}
