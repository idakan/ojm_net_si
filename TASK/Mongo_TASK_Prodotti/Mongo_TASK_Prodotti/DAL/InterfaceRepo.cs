﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_TASK_Prodotti.DAL
{
    interface InterfaceRepo<T>
    {
        IEnumerable<T> GetAll();
        T GetById(ObjectId id);
        bool Insert(T t);
        bool Delete(ObjectId id);
        bool Update(T t);
    }
}
