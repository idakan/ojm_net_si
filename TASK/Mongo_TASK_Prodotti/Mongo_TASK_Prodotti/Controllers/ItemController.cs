﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Mongo_TASK_Prodotti.DAL;
using Mongo_TASK_Prodotti.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_TASK_Prodotti.Controllers
{
    public class ItemController : Controller
    {
        private readonly ItemRepo _repository;

        public ItemController(IConfiguration config)
        {
            bool isOnline = config.GetValue<bool>("isOnline");

            string connString = isOnline == true ?
                config.GetValue<string>("MongoDbSettings:OnlineDB") :
                config.GetValue<string>("MongoDbSettings:LocalDB");

            string dbName = config.GetValue<string>("DBName");

            _repository = new ItemRepo(connString, dbName);
        }

        [HttpGet]
        public ActionResult<IEnumerable<Item>> GetAll()
        {
            return Ok(_repository.GetAll());
        }

        [HttpGet]
        public ActionResult<Item> GetById(ObjectId id)
        {
            return Ok(_repository.GetById(id));
        }

        [HttpPost]
        public ActionResult Insert(Item objItem)
        {
            if (_repository.Insert(objItem))
                return Ok(new { Status = "success" });
            else
                return Ok(new { Status = "error", Description = "Insert Error" });
        }

        [HttpDelete]
        public ActionResult Delete()
        {
            return Ok();
        }

        [HttpPut]
        public ActionResult Update()
        {
            return Ok();
        }
    }
}
