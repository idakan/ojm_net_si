﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Mongo_TASK_Prodotti.DAL;
using Mongo_TASK_Prodotti.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_TASK_Prodotti.Controllers
{
    [ApiController]
    [Route("api/categories")]
    public class CategoryController : Controller
    {
        private readonly CategoryRepo _repository;
        public CategoryController(IConfiguration config)
        {
            bool isOnline = config.GetValue<bool>("isOnline");

            string connString = isOnline == true ?
                config.GetValue<string>("MongoDbSettings:OnlineDB") :
                config.GetValue<string>("MongoDbSettings:LocalDB");

            string dbName = config.GetValue<string>("DBName");

            _repository = new CategoryRepo(connString, dbName);
        }

        [HttpGet]
        public ActionResult<IEnumerable<Category>> GetAll()
        {
            return Ok();
        }

        [HttpGet]
        public ActionResult<Category> GetById()
        {
            return Ok();
        }

        [HttpPost]
        public ActionResult Insert()
        {
            return Ok();
        }

        [HttpDelete]
        public ActionResult Delete()
        {
            return Ok();
        }

        [HttpPut]
        public ActionResult Update()
        {
            return Ok();
        }
    }
}
