﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_TASK_Prodotti.Models
{
    public class Category
    {
        [Required]
        [MaxLength(255)]
        [BsonElement("name")]
        public string Name { get; set; }
        [Required]
        [MaxLength(500)]
        [BsonElement("description")]
        public string Description { get; set; }
        [Required]
        [BsonElement("serialCode")]
        public string SerialCode { get; set; }
        [Required]
        [MaxLength(5)]
        [BsonElement("position")]
        public string Position { get; set; }
    }
}
