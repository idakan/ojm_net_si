﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mongo_TASK_Prodotti.Models
{
    public class Item
    {
        [Required]
        [MaxLength(255)]
        [BsonElement("name")]
        public string Name { get; set; }
        [Required]
        [MaxLength(500)]
        [BsonElement("description")]
        public string Description { get; set; }
        [Required]
        [BsonElement("price")]
        public double Price { get; set; }
        [Required]
        [BsonElement("quantity")]
        public int Quantity { get; set; }
        [Required]
        [BsonElement("sku")]
        public string SKU { get; set; }
        [Required]
        [BsonElement("category")]
        public ObjectId Category { get; set; }

    }
}
