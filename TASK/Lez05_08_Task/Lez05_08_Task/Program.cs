﻿using Lez05_08_Task.Classes;
using System;

namespace Lez05_08_Task
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Creare un software per la gestione del menu 
             * di un ristorante
             * questo avrà due categorie di articolo :
             * Bevande
             * Piatti
             * 
             * Creare i metodi in grado di:
             * - Inserire una bevanda o un piatto
             * - Stampare tutte le bevande o piatti (a scelta)
             * - Contare tutte le bevande o piatti (a scelta)
             * 
             * Ogni articolo avrà almeno:
             * - Codice
             * - Nome
             * - Prezzo
             */
            string scelta;
            string scelta2 = "";
            string codice = "";
            string nome = "";
            float prezzo = 0.0f;
            bool success = false;     //Variabile per controllare il successo di certe operazioni
            int conta;
            GestoreMenu gm = new GestoreMenu();
            do
            {
                Console.WriteLine("Benvento nel sistema di gestione del menù.\n" +
                                  "Le operazioni disponibili sono :\n" +
                                  "1. Inserimento Articolo.\n" +
                                  "2. Stampa articoli.\n" +
                                  "3. Conta articoli.\n" +
                                  "4. Esci.\n");
                scelta = Console.ReadLine();
                switch(scelta)
                {
                    case "1":
                        Console.WriteLine("Che categoria di articolo vuoi aggiungere? bevanda/piatto");
                        do
                        { 
                            try
                            {
                                scelta2 = Console.ReadLine();
                                Console.WriteLine("Inserisci il codice dell'articolo da aggiungere:");
                                codice = Console.ReadLine();
                                Console.WriteLine("Inserisci il nome dell'articolo da aggiungere:");
                                nome = Console.ReadLine();
                                Console.WriteLine("Inserisci il prezzo dell'articolo da aggiungere:");
                                prezzo = Convert.ToSingle(Convert.ToDouble(Console.ReadLine()));
                            } catch(Exception e)
                            {
                                Console.WriteLine("Errore di inserimento\n");
                            }


                            success = gm.InserimentoArticolo(scelta2, codice, nome, prezzo);
                            if (success)
                            {
                                Console.WriteLine("Articolo aggiunto con successo!\n");
                            }
                            else
                            {
                                Console.WriteLine("Errore nell'inserimento dell'articolo.\n");
                            }
                        } while (!(scelta2.Equals("bevanda") || scelta2.Equals("piatto")));
                        break;
                    case "2":
                        do
                        {
                            Console.WriteLine("Che categoria di articolo vuoi elencare? bevanda/piatto");
                            try
                            {
                                scelta2 = Console.ReadLine();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Errore di inserimento\n");
                            }
                            gm.StampaArticoli(scelta2);
                        } while (!(scelta2.Equals("bevanda") || scelta2.Equals("piatto")));
                        break;
                    case "3":
                        do
                        {
                            Console.WriteLine("Che categoria di articolo vuoi contare? bevanda/piatto");
                            try
                            {
                                scelta2 = Console.ReadLine();
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine("Errore di inserimento\n");
                            }
                            conta = gm.ContaArticoli(scelta2);
                            if(conta >= 0)
                            {
                                Console.WriteLine($"Sono presenti {conta} articoli di tipo {scelta2}\n");
                            }
                            else
                            {
                                Console.WriteLine("Errore nella conta degli articoli\n");
                            }
                        } while (!(scelta2.Equals("bevanda") || scelta2.Equals("piatto")));
                        break;
                    case "4":
                        Console.WriteLine("Arrivederci!\n");
                        break;
                    default:
                        Console.WriteLine("Scelta non valida\n");
                        break;
                }
            } while (!scelta.Equals("4"));
        }
    }
}
