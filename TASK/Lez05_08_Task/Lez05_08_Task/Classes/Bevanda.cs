﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_08_Task.Classes
{
    public class Bevanda : Articolo
    {
        public Bevanda(string codice, string nome, float prezzo)
        {
            Codice = codice;
            Nome = nome;
            Prezzo = prezzo;
        }
    }
}
