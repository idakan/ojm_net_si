﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_08_Task.Classes
{
    public abstract class Articolo
    {
        public string Codice { get; set; }
        public string Nome { get; set; }
        public float Prezzo { get; set; }
        public override string ToString()
        {
            return $"Codice: {Codice}\nNome: {Nome}\nPrezzo: {Prezzo} Euro\n";
        }
    }
}
