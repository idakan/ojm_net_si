﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_08_Task.Classes
{
    public class Piatto : Articolo
    {
        public Piatto(string codice, string nome, float prezzo)
        {
            Codice = codice;
            Nome = nome;
            Prezzo = prezzo;
        }
    }
}
