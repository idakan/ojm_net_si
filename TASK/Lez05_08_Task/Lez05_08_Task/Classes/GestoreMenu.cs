﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lez05_08_Task.Classes
{
    public class GestoreMenu
    {
        public List<Articolo> Menu { get; set; }
        public GestoreMenu()
        {
            Piatto pasta = new Piatto("12345", "Pasta", 9.50f);
            Piatto tagliatelle = new Piatto("12347", "Tagliatelle", 8.0f);
            Bevanda acqua = new Bevanda("12346", "Acqua", 1.50f);
            Menu = new List<Articolo>();
            Menu.Add(pasta);
            Menu.Add(acqua);
            Menu.Add(tagliatelle);
        }
        public bool InserimentoArticolo(string categoria, string codice, string nome, float prezzo)
        {
            try
            {
                categoria = categoria.ToLower().Trim();
                if (categoria.Equals("bevanda"))
                {
                    Menu.Add(new Bevanda(codice, nome, prezzo));
                    return true;
                }
                else if (categoria.Equals("piatto"))
                {
                    Menu.Add(new Piatto(codice, nome, prezzo));
                    return true;
                }
            }catch (Exception e) { }
            return false;
        }
        public void StampaArticoli(string categoria)
        {
            categoria = categoria.ToLower().Trim();     //Tolgo gli spazi dalla stringa e la rendo tutta lowerCase
            if (categoria.Equals("piatto") || categoria.Equals("bevanda"))
            {
                foreach (Articolo articolo in Menu)
                {
                    if (categoria.Equals("bevanda") && articolo.GetType().Name.Equals("Bevanda"))
                    {
                        Console.WriteLine(articolo);
                    }
                    else if (categoria.Equals("piatto") && articolo.GetType().Name.Equals("Piatto"))
                    {
                        Console.WriteLine(articolo);
                    }
                }
            }
            else
            {
                Console.WriteLine("Categoria non valida");
            }
        }
        public int ContaArticoli(string categoria)
        {
            int i = -1;                 //Quantità articoli contati
            if (categoria.Equals("piatto") || categoria.Equals("bevanda"))
            {
                i++;
                foreach (Articolo articolo in Menu)
                {
                    if (categoria.Equals("bevanda") && articolo.GetType().Name.Equals("Bevanda"))
                    {
                        i++;
                    }
                    else if (categoria.Equals("piatto") && articolo.GetType().Name.Equals("Piatto"))
                    {
                        i++;
                    }
                }
            }
            return i;
        }
    }
}
