const http = require("http");

const host = "127.0.0.1";
const port = 4001;

const server = http.createServer((req, res) => {

    let responso = {
        urlRef: req.url,
        methodReq: req.method
    }

    res.statusCode = 200;
    res.setHeader("Content-Type", "text/json");
    res.end(JSON.stringify(responso));

    console.log("Richiesta")
})

server.listen(port, host, () => {
    console.log(`Sono in ascolto sull'indirizzo ${host}:${port}`);
})