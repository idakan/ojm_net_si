const mongoose = require("mongoose");
const BlogPost = require("./models/BlogPost");

const dbName = "SitoNotizie";

const db = mongoose.connect(`mongodb+srv://mongodbUser:cicciopasticcio@cluster0.rptpp.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono Connesso!");
});

let idRicerca = "62541bb61b10f60983e47caa";

// FindAll -> SELECT * FROM BlogPosts
// BlogPost.find({}, (error, elenco) => {
//     if(!error)
//         console.log(elenco);
//     else
//         console.log(error);
// })

// SELECT * FROM BlogPosts WHERE Titolo = "Prova di primo articolo"
// BlogPost.find({title: "Prova di primo articolo"}, (error, elenco) => {
//     if(!error)
//         console.log(elenco);
//     else
//         console.log(error);
// })

// SELECT * FROM BlogPosts WHERE Titolo = "Prova%"
// BlogPost.find({title: /^Prova/}, (error, elenco) => {
//     if(!error)
//         console.log(elenco);
//     else
//         console.log(error);
// })

// SELECT * FROM BlogPosts WHERE Titolo = "%articolo"
// BlogPost.find({title: /articolo$/}, (error, elenco) => {
//     if(!error)
//         console.log(elenco);
//     else
//         console.log(error);
// })

// SELECT * FROM BlogPosts WHERE Titolo = "%im%"
BlogPost.find({title: /im/ }, (error, elenco) => {
    if(!error)
        console.log(elenco);
    else
        console.log(error);
})