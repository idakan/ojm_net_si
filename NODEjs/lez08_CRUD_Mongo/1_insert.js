const mongoose = require("mongoose");
const BlogPost = require("./models/BlogPost");

const dbName = "SitoNotizie";

const db = mongoose.connect(`mongodb+srv://mongodbUser:cicciopasticcio@cluster0.rptpp.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono Connesso!");
});

let articolo = {
    title: "Prova di terzo articolo",
    body: "Prova di descrizione senza troppe pretese con un testo lunghissimo",
}

BlogPost.create(articolo, (error, item) => {
    if(!error)
        console.log(item);
    else
        console.log("Errore di inserimento");
});