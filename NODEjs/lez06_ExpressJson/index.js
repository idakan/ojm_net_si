const express = require("express");
const app = express();

const port = 4001;

app.listen(port, () => {
    console.log(`Sono in ascolto all'indirizzo 127.0.0.1:${port}`);
});

//ROTTE
app.get("/", (req,res) => {
    res.end("<h1>Sono la Home</h1>");
});

app.get("/contatti", (req,res) => {
    res.end("<h1>Sono il Contattami</h1>");

});

app.get("/servizi", (req,res) => {
    res.end("<h1>Sono i Servizi</h1>");
});

app.get("/giovanni", (req,res) => {
    let giovanni = {
        nome: "Giovanni",
        cognome: "Pace",
        eta: 35
    };

    res.status(404).json(giovanni);
});

app.get("/lista", (req,res) => {
    let elenco = [
        {
        nome: "Giovanni",
        cognome: "Pace",
        eta: 35
        },
        {
        nome: "Valeria",
        cognome: "Verdi",
        eta: 24
        },
        {
        nome: "Mario",
        cognome: "Rossi",
        eta: 52
        },
    ];

    res.json(elenco);
});