const http = require("http");

const host = "127.0.0.1";
const port = 4001;

const server = http.createServer((req, res) => {
    switch(req.url)
    {
        case "/":
            res.end("Sono la pagina HOME");
            break;
        case "/contatti":
            res.end("Sono la pagina CONTATTI");
            break;
        case "/servizi":
            res.end("Sono la pagina SERVIZI");
            break;
        default:
            res.statusCode = 404;
            res.end("Ooooops... pagina non trovata!");    
            break;
    }
})

server.listen(port, host, () => {
    console.log(`Sono in ascolto sull'indirizzo ${host}:${port}`);
})