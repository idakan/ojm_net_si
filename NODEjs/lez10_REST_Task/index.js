//MODULES AND SERVER INITIALIZATION

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const Category = require("./models/Category");
const Product = require("./models/Product");
const bodyParser = require("body-parser");
const Student = require("../lez09_CRUD_Mongo_REST/models/Student");

const app = express();

const port = 4001;
const dbName = "Menu";

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

const db = mongoose.connect(`mongodb+srv://mongodbUser:cicciopasticcio@cluster0.rptpp.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Server up!");
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});

//##################################################################

//CRUD ON PRODUCTS :start

//GETALL -> Products
app.get("api/products", (req, res) => {
    Student.find({}, (error, list) => {
        if(!error)
            res.json(list);
        else
            res.status(500).end();
    });
});


//INSERT -> Product
app.post("/api/products/insert", (req, res) => {
    console.log(req.body);

    Product.create(req.body, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    });
});

//GET BY ID -> Product
app.get("/api/products/:productId", (req, res) => {
    console.log(req.params.productId);

    Product.findById(req.params.productId, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    });
});


//DELETE -> Product
app.delete("/api/products/:productId", (req, res) => {
    console.log(req.params.productId);

    Product.findByIdAndDelete(req.params.productId, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"});
        else
            res.status(500).end();
    });
});

//UPDATE -> Product
app.put("/api/products/:productId", (req, res) => {
    console.log(req.params.productId);

    Product.findByIdAndUpdate(req.params.productId, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    })
})

//CRUD ON PRODUCTS :end
 
//##################################################################

//CRUD ON CATEGORIES :start

//GETALL -> Categories
app.get("api/categories", (req, res) => {
    Category.find({}, (error, list) => {
        if(!error)
            res.json(list);
        else
            res.status(500).end();
    });
});


//INSERT -> Category
app.post("/api/categories/insert", (req, res) => {
    console.log(req.body);

    Category.create(req.body, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    });
});

//GET BY ID -> Category
app.get("/api/categories/:categoryId", (req, res) => {
    console.log(req.params.categoryId);

    Category.findById(req.params.categoryId, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    });
});


//DELETE -> Category
app.delete("/api/categories/:categoryId", (req, res) => {
    console.log(req.params.categoryId);

    Category.findByIdAndDelete(req.params.categoryId, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"});
        else
            res.status(500).end();
    });
});

//UPDATE -> Category
app.put("/api/categories/:categoryId", (req, res) => {
    console.log(req.params.categoryId);

    Category.findByIdAndUpdate(req.params.categoryId, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    })
})

//CRUD ON CATEGORIES :end

//##################################################################