const mongoose = require("mongoose");
const Category = require("./Category");
const Schema = mongoose.Schema;

const ProductSchema = new Schema({
    name: String,
    serial: String,
    description: String,
    categories: {
        type: Schema.Types.ObjectId, 
        ref: "Category"
    }
})

const Product = mongoose.model("Product", ProductSchema);

module.exports = Product;