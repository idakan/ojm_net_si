const http = require("http");

const host = "127.0.0.1";
const port = 4001;

const server = http.createServer((req, res) => {
    res.statusCode = 200;
    res.end("CIAO GIOVANNI");

    console.log("Richiesta")
})

server.listen(port, host, () => {
    console.log(`Sono in ascolto sull'indirizzo ${host}:${port}`);
})