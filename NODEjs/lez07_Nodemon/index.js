const express = require("express");
const app = express();

const port = 4001;

app.listen(port, () =>{
    console.log(`Sono in ascolto sulla porta ${port}`);
})

app.get("/", (req, res) => {
    res.end("Ciao");
})