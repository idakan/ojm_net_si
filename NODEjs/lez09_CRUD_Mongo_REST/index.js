const express = require("express");
const mongoose = require("mongoose");
const Student = require("./models/Student");
const bodyParser = require("body-parser");
const app = express();

const port = 4001;
const dbName = "Universita";

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))

const db = mongoose.connect(`mongodb+srv://mongodbUser:cicciopasticcio@cluster0.rptpp.mongodb.net/${dbName}?retryWrites=true&w=majority`, () => {
    console.log("Sono Connesso!");
});

 app.listen(port, () => {
     console.log(`Sono in ascolto sulla porta ${port}`)
 });

 app.get("/api/studenti", (req, res) => {
    Student.find({}, (error, list) => {
        if(!error)
            res.json(list);
        else
            res.status(500);
    })
 });

 app.post("/api/studenti/insert", (req, res) => {
     console.log(req.body);

    Student.create(req.body, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    })
 });

 app.get("/api/studenti/:idstudente", (req, res) => {
    console.log(req.params.idstudente)

    Student.findById(req.params.idstudente, (error, item) => {
        if(!error)
            res.status(200).json(item);
        else
            res.status(500).end();
    })
});

app.delete("/api/studenti/:idstudente", (req, res) => {
    console.log(req.params.idstudente)

    Student.findByIdAndDelete(req.params.idstudente, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"});
        else
            res.status(500).end();
    })
});

app.put("/api/studenti/:idstudente", (req, res) => {
    console.log(req.params.idstudente)

    Student.findByIdAndUpdate(req.params.idstudente, req.body, (error, item) => {
        if(!error)
            res.status(200).json({"status": "success"});
        else
            res.status(500).end();
    })
});