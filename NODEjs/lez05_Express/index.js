const express = require("express");
const app = express();

const port = 4001;

app.listen(port, () => {
    console.log(`Sono in ascolto all'indirizzo 127.0.0.1:${port}`);
});

//ROTTE
app.get("/", (req,res) => {
    res.end("<h1>Sono la Home</h1>");
});

app.get("/contatti", (req,res) => {
    res.end("<h1>Sono il Contattami</h1>");

});

app.get("/servizi", (req,res) => {
    res.end("<h1>Sono i Servizi</h1>");
});